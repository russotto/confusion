#ifndef MDL_INTERP_H_
#define MDL_INTERP_H_
// Sequential access to intepreter args
#define IARGSETUP() mdl_value_t *iargs_cursor = mdl_interp_stack->args;
#define IARGRESET(stack) iargs_cursor = (stack)->args;
#define IGETNEXTARG(a) mdl_value_t* a = NULL; do { if (iargs_cursor) { (a) = iargs_cursor->v.p.car; iargs_cursor = iargs_cursor->v.p.cdr; } } while(0)
#define ISETNEXTARG(a) do { if (iargs_cursor) {iargs_cursor->v.p.car = a; iargs_cursor = iargs_cursor->v.p.cdr; } } while(0)
#define IGETRETVAL(a) mdl_value_t* a = mdl_interp_stack->retval;
#define ISETRETVAL(a) (mdl_interp_stack->retval = (a))

enum mdl_interp_return_t {
    RETURN_RETURN = 0,
    RETURN_COPYDOWN,    // Copy to frame already started
    RETURN_COPYFORWARD, // Copy to next frame, not yet started
    RETURN_COPYNEXT,    // Copy to next frame, unconditional
    RETURN_DISCARD,     // ignore value
};

enum mdl_interp_method_t {
    INTERP_EVAL = 0,
    INTERP_STD_EVAL,
    INTERP_EVAL_LIST,
    INTERP_EVAL_VECTOR,
    INTERP_EVAL_UVECTOR,
    INTERP_EVAL_APPLY_EXPR,
    INTERP_APPLY,
    INTERP_APPLY_FROM_ISTACK,
    INTERP_APPLY_BUILTIN_PT2,
    INTERP_APPLY_BUILTIN_PT3,
    INTERP_APPLY_FIX_PT2,
    INTERP_BIND_ARG_STATE_MACHINE,
    INTERP_SET_ENVIRONMENT,
    INTERP_POP_FRAME,
    INTERP_LISTEN_ERROR_PT2,
    INTERP_EVAL_BODY,
    INTERP_EVAL_BODY_STATE_MACHINE,
    INTERP_REP_STATE_MACHINE,
    INTERP_COND_PT2,
    INTERP_ILIST_PT2,
    INTERP_I_U_VECTOR_PT2,
    INTERP_IBYTES_PT2,  // NOT IMPLEMENTED
    INTERP_ISTRING_PT2,
    INTERP_AND_PT2,
    INTERP_OR_PT2,
    INTERP_EVAL_FROM_ISTACK,
    INTERP_MAPF_LOOP,
    INTERP_MAPR_LOOP,
    INTERP_MAPFR_FINAL,
    INTERP_DEF_PT2,
    INTERP_LONGJMP_TO,
};

struct mdl_interp_frame_t {
    mdl_interp_frame_t *prev_frame;
    mdl_value_t *args;
    mdl_interp_method_t method;
    mdl_interp_return_t return_to;
    bool started;
    int jumpval;
    mdl_value_t *retval;
};

#define EVAL_IN_STRUCT 1
#define EVAL_EXPAND 2

extern struct mdl_interp_frame_t *mdl_interp_stack;
mdl_value_t *mdl_interp_from_stack();
void mdl_push_interp_eval(mdl_interp_return_t return_to, mdl_value_t *l,
                          int eval_flags = 0 , mdl_value_t *environment= NULL);
void mdl_push_interp_eval_list(mdl_interp_return_t return_to, mdl_value_t *l);
void mdl_push_interp_eval_vector(mdl_interp_return_t return_to, mdl_value_t *l);
void mdl_push_interp_eval_uvector(mdl_interp_return_t return_to, mdl_value_t *l);
void mdl_push_interp_eval_apply_expr(mdl_interp_return_t return_to, mdl_value_t *apply_expr);
void mdl_push_interp_apply_from_istack(mdl_interp_return_t return_to, mdl_value_t *l, bool is_expand);
void mdl_push_interp_apply(mdl_interp_return_t return_to, mdl_value_t *applier, mdl_value_t *l, bool called_from_apply_subr = false, bool is_expand = false);
void mdl_push_interp_std_eval(mdl_interp_return_t return_to, mdl_value_t *l,
                              int eval_flags = 0, int eval_as_type = MDL_TYPE_NOTATYPE);
void mdl_push_interp_apply_builtin_pt2(mdl_interp_return_t return_to, mdl_value_t *applier);
void mdl_push_interp_apply_builtin_pt3(mdl_interp_return_t return_to, mdl_value_t *applier);
void mdl_push_interp_apply_fix_pt2(mdl_interp_return_t return_to, mdl_value_t *index);
void mdl_push_interp_set_environment(mdl_interp_return_t return_to, mdl_value_t *environment);
void mdl_push_interp_pop_frame(mdl_interp_return_t return_to, mdl_value_t *frame);
void mdl_push_interp_listen_error_pt2(mdl_interp_return_t return_to, bool is_error);
void mdl_push_interp_bind_arg_state_machine(
    mdl_interp_return_t return_to, mdl_value_t *fargp, mdl_value_t *argptr,
    mdl_value_t *argstate, mdl_value_t *argcount,
    mdl_value_t *apply_to, mdl_value_t *frame,
    mdl_value_t *called_from_apply);
void mdl_push_interp_eval_body(mdl_interp_return_t return_to, mdl_value_t *fbodyp, mdl_value_t *frame);
void mdl_push_interp_eval_body_state_machine(mdl_interp_return_t return_to, mdl_value_t *fargp, mdl_value_t *frame);
void mdl_push_interp_cond_pt2(mdl_interp_return_t return_to, mdl_value_t *cur_clause_arg);
void mdl_push_interp_ilist_pt2(mdl_interp_return_t return_to, mdl_value_t *list_head);
void mdl_push_interp_i_u_vector_pt2(mdl_interp_return_t return_to, mdl_value_t *empty_u_vector);
void mdl_push_interp_istring_pt2(mdl_interp_return_t return_to, mdl_value_t *result);
void mdl_push_interp_and_pt2(mdl_interp_return_t return_to, mdl_value_t *rest);
void mdl_push_interp_or_pt2(mdl_interp_return_t return_to, mdl_value_t *rest);
void mdl_push_interp_eval_from_istack(mdl_interp_return_t return_to);
void mdl_push_interp_def_pt2(mdl_interp_return_t return_to, mdl_value_t *def_list);
void mdl_push_interp_longjmp_to(mdl_frame_t *jump_frame, int jumpval);

mdl_value_t *mdl_set_environment();
mdl_value_t *mdl_eval_body();
mdl_value_t *mdl_eval_body_state_machine();
mdl_value_t *mdl_listen_error_pt2();
mdl_value_t *mdl_rep_state_machine();
mdl_value_t *mdl_eval_apply_expr();
mdl_value_t *mdl_cond_pt2();
mdl_value_t *mdl_ilist_pt2();
mdl_value_t *mdl_i_u_vector_pt2();
mdl_value_t *mdl_istring_pt2();
mdl_value_t *mdl_ibytes_pt2();    // NOT IMPLEMENTED
mdl_value_t *mdl_and_pt2();
mdl_value_t *mdl_or_pt2();
mdl_value_t *mdl_mapfr_loop();
mdl_value_t *mdl_mapfr_final();
mdl_value_t *mdl_def_pt2();
#endif // MDL_INTERP_H_
