/*****************************************************************************/
/*    'Confusion', a MDL intepreter                                         */
/*    Copyright 2009 Matthew T. Russotto                                    */
/*                                                                          */
/*    This program is free software: you can redistribute it and/or modify  */
/*    it under the terms of the GNU General Public License as published by  */
/*    the Free Software Foundation, either version 3 of the License, or (at */
/*    your option) any later version.                                       */
/*                                                                          */
/*    This program is distributed in the hope that it will be useful,       */
/*    but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*    GNU General Public License for more details.                          */
/*                                                                          */
/*    You should have received a copy of the GNU General Public License     */
/*    along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*****************************************************************************/
#include <assert.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>
#include "macros.hpp"
#include "mdl_internal_defs.h"
#include "mdl_builtin_types.h"
#include "mdl_builtins.h"
#include "mdl_assoc.hpp"
#include "mdl_strbuf.h"
#include "mdl_interp.h"

#pragma GCC diagnostic ignored "-Wuninitialized"
#define DECODE_TENEX_FILESPECS
mdl_built_in_table_t built_in_table;
mdl_type_table_t mdl_type_table;
mdl_symbol_table_t global_syms;

mdl_frame_t *cur_frame = NULL;
mdl_frame_t *initial_frame = NULL;
mdl_frame_t *toplevel_frame = NULL;
int cur_process_bindid;
bool suppress_listen_message;

mdl_assoc_table_t *mdl_assoc_table;
GC_word last_assoc_clean;

struct mdl_interp_frame_t *mdl_interp_stack = NULL;
// misc atoms

atom_t *atom_oblist;
mdl_value_t *mdl_value_oblist;
mdl_value_t *mdl_value_initial_oblist;
mdl_value_t *mdl_value_root_oblist;
mdl_value_t *mdl_value_atom_redefine;
mdl_value_t *mdl_value_atom_lastprog;
mdl_value_t *mdl_value_atom_lastmap;
mdl_value_t *mdl_value_atom_default;
mdl_value_t *mdl_value_T;
mdl_value_t mdl_value_false = { PRIMTYPE_LIST, MDL_TYPE_FALSE};
mdl_value_t mdl_value_unassigned = { PRIMTYPE_WORD, MDL_TYPE_UNBOUND};
mdl_value_t *mdl_static_block_stack = NULL;

#define MDL_OBLIST_HASHBUCKET_DEFAULT 17
#define MDL_ROOT_OBLIST_HASHBUCKET_DEFAULT 103

#define NEW_BINDID(X) ((++(X))?(X):(++(X))) // increment bindid, but disallow 0

mdl_type_table_entry_t *mdl_type_table_entry(int typenum)
{
    if (typenum >= (int)mdl_type_table.size()) return NULL;
    return &mdl_type_table[typenum];
}

atom_t *mdl_type_atom(int typenum)
{
    const mdl_type_table_entry_t *tte = mdl_type_table_entry(typenum);
    if (tte) return tte->a;
    return NULL;
}

int mdl_get_typenum(mdl_value_t *val)
{
    if (val->type != MDL_TYPE_ATOM)
        mdl_error("Only atoms can be types");
    return val->v.a->typenum;
}

atom_t *mdl_get_type_name(int typenum)
{
    const mdl_type_table_entry_t *tte = mdl_type_table_entry(typenum);
    
    if (tte)
        return tte->a;
    else
        mdl_error("Get_type_name passed invalid type");
}

primtype_t mdl_type_primtype(int typenum)
{
    const mdl_type_table_entry_t *tte = mdl_type_table_entry(typenum);
    if (tte) return tte->pt;
    return (primtype_t)MDL_TYPE_NOTATYPE;
}

mdl_value_t *mdl_internal_newtype(mdl_value_t *a, int oldtype)
{
    mdl_type_table_entry_t newt;
    memset(&newt, 0, sizeof(newt));
    if (a->type != MDL_TYPE_ATOM)
        mdl_error("Only atoms can be (new) types");
    newt.pt = mdl_type_primtype(oldtype);
    newt.a = a->v.a;
    newt.a->typenum = mdl_type_table.size();
    mdl_type_table.push_back(newt);
    return a;
}

mdl_value_t *mdl_get_printtype(int typenum)
{
    const mdl_type_table_entry_t *tte = mdl_type_table_entry(typenum);
    if (tte) return tte->printtype;
    return NULL;
}

mdl_value_t *mdl_get_evaltype(int typenum)
{
    const mdl_type_table_entry_t *tte = mdl_type_table_entry(typenum);
    if (tte) return tte->evaltype;
    return NULL;
}

mdl_value_t *mdl_get_applytype(int typenum)
{
    const mdl_type_table_entry_t *tte = mdl_type_table_entry(typenum);
    if (tte) return tte->applytype;
    return NULL;
}

mdl_value_t *mdl_set_printtype(int typenum, mdl_value_t *how)
{
    mdl_type_table_entry_t *tte = mdl_type_table_entry(typenum);
    if (tte) 
    {
        tte->printtype = how;
        return how;
    }
    return NULL;
}

mdl_value_t *mdl_set_evaltype(int typenum, mdl_value_t *how)
{
    mdl_type_table_entry_t *tte = mdl_type_table_entry(typenum);
    if (tte) 
    {
        tte->evaltype = how;
        return how;
    }
    return NULL;
}

mdl_value_t *mdl_set_applytype(int typenum, mdl_value_t *how)
{
    mdl_type_table_entry_t *tte = mdl_type_table_entry(typenum);
    if (tte) 
    {
        tte->applytype = how;
        return how;
    }
    return NULL;
}

// this returns a copy of the internal type vector, not the real thing as in
// real MDL
mdl_value_t *mdl_typevector()
{
    mdl_value_t * result = mdl_new_empty_vector(mdl_type_table.size(), MDL_TYPE_VECTOR);
    mdl_type_table_t::iterator iter;
    mdl_value_t *iptr;

    iptr = VREST(result, 0);
    for (iter = mdl_type_table.begin(); iter != mdl_type_table.end(); iter++)
    {
        iptr->type = MDL_TYPE_ATOM;
        iptr->pt = PRIMTYPE_ATOM;
        iptr->v.a = iter->a;
        iptr++;
    }
    return result;
}

bool mdl_atom_equal(const atom_t *a, const atom_t *b)
{
    return a == b;
}

bool mdl_string_equal_cstr(const counted_string_t *s, const char *cs)
{
    int len  = strlen(cs);
    return (len == s->l) && !memcmp(s->p, cs, len);
}

bool mdl_value_equal_atom(const mdl_value_t *a, const atom_t *b)
{
    if (a == NULL && b == NULL) return true; // ?? maybe wrong
    if (a == NULL || b == NULL) return false;
    if (a->pt != PRIMTYPE_ATOM) return false;
    if (a->type != MDL_TYPE_ATOM) return false;
    return mdl_atom_equal(a->v.a, b);
}

size_t mdl_hash_value(const mdl_value_t *a)
{
    size_t result;
    switch (a->pt)
    {
    case PRIMTYPE_ATOM:
    {
        result = (size_t)a->v.a;
        break;
    }
    case PRIMTYPE_WORD:
    {
        result = (size_t)a->v.w;
        break;
    }
    case PRIMTYPE_STRING:
    {
        result = (size_t)a->v.s.p; // pointer is sufficient, all strings
                                   // with the same pointer
                                   // must have the same length
        break;
    }
    case PRIMTYPE_LIST:
        result = (size_t)a->v.p.cdr; // CAR of list head should always be empty
        break;
    case PRIMTYPE_VECTOR:
        result = (size_t)(a->v.v.p + a->v.v.offset);
        break;
    case PRIMTYPE_UVECTOR:
        result = (size_t)(a->v.uv.p + a->v.uv.offset);
        break;
    case PRIMTYPE_TUPLE:
        result = (size_t)(a->v.tp.p + a->v.tp.offset);
        break;
    case PRIMTYPE_FRAME:
        result = (size_t)a->v.f;
        break;
    default:
        mdl_error("Can't hash that!");
        break;
    }
    result = result ^ ((a->type<<20)*7); // silly, yes.
    return result;
}

bool mdl_value_double_equal(const mdl_value_t *a, const mdl_value_t *b)
{
    if (a == b) return true;
    if (!a || !b) return false;
    if (a->pt != b->pt) return false;
    if (a->type != b->type) return false;
    switch (a->pt)
    {
    case PRIMTYPE_ATOM:
        return mdl_atom_equal(a->v.a, b->v.a);
    case PRIMTYPE_WORD:
        return a->v.w == b->v.w;
    case PRIMTYPE_STRING:
        return (a->v.s.l == b->v.s.l) && (a->v.s.p == b->v.s.p);
        // structured type tests may not be restrictive enough
        // but I think they are; see section 8.2.2 in the MDL Programming Language
    case PRIMTYPE_LIST:
        return a->v.p.cdr == b->v.p.cdr; // CAR of list head should always be empty
    case PRIMTYPE_VECTOR:
        return ((a->v.v.p == b->v.v.p) && (a->v.v.offset == b->v.v.offset));
    case PRIMTYPE_UVECTOR:
        return ((a->v.uv.p == b->v.uv.p) && (a->v.uv.offset == b->v.uv.offset));
    case PRIMTYPE_TUPLE:
        return ((a->v.tp.p == b->v.tp.p) && (a->v.tp.offset == b->v.tp.offset));
    case PRIMTYPE_FRAME:
        return a->v.f == b->v.f;
    }
    return false;
}

bool mdl_value_equal(const mdl_value_t *a, const mdl_value_t *b)
{
    if (a == b) return true;
    if (!a || !b) return false;
    if (a->pt != b->pt) return false;
    if (a->type != b->type) return false;
    switch (a->pt)
    {
    case PRIMTYPE_ATOM:
        return mdl_atom_equal(a->v.a, b->v.a);
    case PRIMTYPE_WORD:
        return a->v.w == b->v.w;
    case PRIMTYPE_STRING:
        return (a->v.s.l == b->v.s.l) && !memcmp(a->v.s.p, b->v.s.p, a->v.s.l);
    case PRIMTYPE_LIST:
        return mdl_value_equal(a->v.p.car, b->v.p.car) && mdl_value_equal(a->v.p.cdr, b->v.p.cdr);
    case PRIMTYPE_VECTOR:
    {
        int len = VLENGTH(a);
        if (len != VLENGTH(b)) return false;
        mdl_value_t *elema = VREST(a, 0);
        mdl_value_t *elemb = VREST(b, 0);
        while (len--)
        {
            if (!mdl_value_equal(elema, elemb)) return false;
            elema++;
            elemb++;
        }
        return true;
    }
    case PRIMTYPE_TUPLE:
    {
        int len = TPLENGTH(a);
        if (len != TPLENGTH(b)) return false;
        mdl_value_t *elema = TPREST(a, 0);
        mdl_value_t *elemb = TPREST(b, 0);
        while (len--)
        {
            if (!mdl_value_equal(elema, elemb)) return false;
            elema++;
            elemb++;
        }
        return true;
    }
    case PRIMTYPE_UVECTOR:
    {
        int len = UVLENGTH(a);
        if (len != UVLENGTH(b)) return false;
        if (UVTYPE(a) != UVTYPE(b)) return false;
        uvector_element_t *elema = UVREST(a, 0);
        uvector_element_t *elemb = UVREST(b, 0);
        while (len--)
        {
            mdl_value_t *vala = mdl_uvector_element_to_value(a, elema, NULL);
            mdl_value_t *valb = mdl_uvector_element_to_value(b, elemb, NULL);
            if (!mdl_value_equal(vala, valb)) return false;
            elema++;
            elemb++;
        }
        return true;
    }
    case PRIMTYPE_FRAME:
        return a->v.f == b->v.f;
    }
    return false;
}

mdl_value_t *mdl_new_mdl_value()
{
    return (mdl_value_t *)GC_MALLOC(sizeof(mdl_value_t));
}


MDL_INT mdl_hash_pname(const char *pname)
{
    // sdbm
    // for each character
    // hash = hash * 65599 + str[i];
    MDL_INT hash = 0;
    int c;
    while ((c = *pname++))
        hash = c + (hash << 6) + (hash << 16) - hash;
    if (hash < 0) hash += MDL_INT_MAX;
    return hash;
}

mdl_value_t *mdl_get_atom_from_oblist(const char *pname, mdl_value_t *oblist)
{
    if (oblist->type != MDL_TYPE_OBLIST)
        mdl_error("Oblist not of oblist type in atom lookup");
    int buckets = UVLENGTH(oblist);
    MDL_INT bucket_num = mdl_hash_pname(pname) % buckets;

    uvector_element_t *bucket = mdl_internal_uvector_rest(oblist, bucket_num);

    mdl_value_t *cursor = bucket->l;
    while (cursor)
    {
        mdl_value_t *av = cursor->v.p.car;
        if (av->type != MDL_TYPE_ATOM)
            mdl_error("Something not an atom in the oblist");
        if (!strcmp(pname, av->v.a->pname))
        {
            return av;
        }
        cursor = cursor->v.p.cdr;
    }
    return NULL;
}

mdl_value_t *mdl_remove_atom_from_oblist(const char *pname, mdl_value_t *oblist)
{
    if (oblist->type != MDL_TYPE_OBLIST)
        mdl_error("Oblist not of oblist type in atom remove");
    int buckets = UVLENGTH(oblist);
    MDL_INT bucket_num = mdl_hash_pname(pname) % buckets;

    uvector_element_t *bucket = mdl_internal_uvector_rest(oblist, bucket_num);

    mdl_value_t *cursor = bucket->l;
    mdl_value_t *lastcursor = NULL;
    while (cursor)
    {
        mdl_value_t *av = cursor->v.p.car;
        if (av->type != MDL_TYPE_ATOM)
            mdl_error("Something not an atom in the oblist");
        if (!strcmp(pname, av->v.a->pname))
        {
            if (cursor == bucket->l) bucket->l = cursor->v.p.cdr;
            else lastcursor->v.p.cdr = cursor->v.p.cdr;
            av->v.a->oblist = NULL;
            return av;
        }
        lastcursor = cursor;
        cursor = cursor->v.p.cdr;
    }
    return NULL;
}

// note that it is assumed the atom isn't already there
void mdl_put_atom_in_oblist(const char *pname, mdl_value_t *oblist, mdl_value_t *new_atom)
{
    if (oblist->type != MDL_TYPE_OBLIST)
        mdl_error("Oblist not of oblist type in atom lookup");
    int buckets = UVLENGTH(oblist);
    MDL_INT bucket_num = mdl_hash_pname(pname) % buckets;

    uvector_element_t *bucket = mdl_internal_uvector_rest(oblist, bucket_num);
    mdl_value_t *n = mdl_newlist();
    n->v.p.car = new_atom;
    n->v.p.cdr = bucket->l;
    bucket->l = n;
}

// create an atom not on an oblist
mdl_value_t *mdl_create_atom(const char *pname)
{
//    atom_t *a = (atom_t *)GC_MALLOC(sizeof(atom_t) + strlen(pname)); // the -1 and +1 cancel
//    strcpy(a->pname, pname);
    atom_t *a = (atom_t *)GC_MALLOC(sizeof(atom_t));
    int len = strlen(pname);
    a->typenum = MDL_TYPE_NOTATYPE;
    a->pname = mdl_new_raw_string(len, true);
    strcpy(a->pname, pname);
    mdl_value_t *atomval = mdl_newatomval(a);
    return atomval;
}

mdl_value_t *mdl_create_atom_on_oblist(const char *pname, mdl_value_t *oblist)
{
    if (oblist->type != MDL_TYPE_OBLIST)
        mdl_error("Oblist not of oblist type in ATOM create");

    if (mdl_get_atom_from_oblist(pname, oblist))
        return NULL; // no dupes allowed

    mdl_value_t *atomval = mdl_create_atom(pname);
    atomval->v.a->oblist = oblist;
    mdl_put_atom_in_oblist(pname, oblist, atomval);
    return atomval;
}

mdl_value_t *mdl_get_or_create_atom_on_oblist(const char *pname, mdl_value_t *oblist)
{
    mdl_value_t *atomval;

    if (oblist->type != MDL_TYPE_OBLIST)
        mdl_error("Oblist not of oblist type in ATOM create");

    if ((atomval = mdl_get_atom_from_oblist(pname, oblist)))
        return atomval;

    atomval = mdl_create_atom(pname);
    atomval->v.a->oblist = oblist;
    mdl_put_atom_in_oblist(pname, oblist, atomval);
    return atomval;
}

mdl_value_t *mdl_create_oblist(mdl_value_t *oblname, int buckets)
{
    mdl_value_t *result;

    if (oblname->type != MDL_TYPE_ATOM)
        mdl_error("OBLIST name must be atom");

    result = mdl_internal_eval_getprop(oblname, mdl_value_oblist);
    if (!result)
    {
        result = mdl_new_empty_uvector(buckets, MDL_TYPE_OBLIST);
        UVTYPE(result) = MDL_TYPE_LIST;
        mdl_internal_eval_putprop(oblname, mdl_value_oblist, result);
        mdl_internal_eval_putprop(result, mdl_value_oblist, oblname);
    }
    return result;
}

atom_t *mdl_get_oblist_name(mdl_value_t *oblist)
{
    mdl_value_t *oname = mdl_internal_eval_getprop(oblist, mdl_value_oblist);
    if (!oname) return NULL;
    if (oname->type != MDL_TYPE_ATOM) 
        mdl_error("Name of an oblist must be an atom"); // probably not actually true in real MDL
    return oname->v.a;
}

mdl_value_t *mdl_get_current_oblists()
{
    mdl_value_t *oblists;

    if (cur_frame == NULL) 
        oblists = mdl_local_symbol_lookup(atom_oblist, cur_process_initial_frame);
    else
        oblists = mdl_local_symbol_lookup(atom_oblist, cur_frame);
    return oblists;
}

mdl_value_t *mdl_get_atom_default_oblist(const char *pname, bool insert_allowed, mdl_value_t *oblists)
{
    if (!oblists)
        oblists = mdl_get_current_oblists();
        
    if (oblists == NULL)
    {
        if (insert_allowed)
            mdl_error(".OBLIST is not set");
        return NULL;
    }
    mdl_value_t *a = NULL;
    if (oblists->type == MDL_TYPE_OBLIST)
    {
        a = mdl_get_atom_from_oblist(pname, oblists);
        if (!a && insert_allowed)
            a = mdl_create_atom_on_oblist(pname, oblists);
    }
    else if (oblists->type == MDL_TYPE_LIST)
    {
        mdl_value_t *cursor = oblists->v.p.cdr;
        mdl_value_t *default_marker = oblists;
        while (cursor && !a)
        {
            mdl_value_t *oblist = cursor->v.p.car;
            if (oblist->type == MDL_TYPE_OBLIST)
            {
                a = mdl_get_atom_from_oblist(pname, oblist);
            }
            else if (insert_allowed && mdl_value_equal(oblist, mdl_value_atom_default))
            {
                default_marker = cursor;
            }
            cursor = cursor->v.p.cdr;
        }
        if (!a && insert_allowed)
        {
            default_marker = default_marker->v.p.cdr;
            if (!default_marker)
                mdl_error("Default oblist for insert missing");
            default_marker = default_marker->v.p.car;
            if (!default_marker)
                mdl_error("Default oblist for insert NULL");
            if (default_marker->type != MDL_TYPE_OBLIST)
                mdl_error("Default oblist for insert not OBLIST");
            a = mdl_create_atom_on_oblist(pname, default_marker);
        }
    }
    return a;
}

mdl_value_t *mdl_push_oblist_lval(mdl_value_t *new_lval)
{
    mdl_value_t *old_lval = mdl_get_current_oblists();
    mdl_static_block_stack = mdl_cons_internal(old_lval, mdl_static_block_stack);
    mdl_set_lval(atom_oblist, new_lval, cur_frame);
    return new_lval;
}

mdl_value_t *mdl_pop_oblist_lval()
{
    if (mdl_static_block_stack == NULL)
        mdl_error("Tried to pop static block stack when it was empty");
    mdl_value_t *old_lval = mdl_static_block_stack->v.p.car;
    mdl_static_block_stack = mdl_static_block_stack->v.p.cdr;
    mdl_set_lval(atom_oblist, old_lval, cur_frame);
    return old_lval;
}

bool mdl_oblists_are_reasonable(mdl_value_t *oblists)
{
    if (!oblists) return false;
    if (oblists->type == MDL_TYPE_OBLIST) return true;
    if (oblists->type != MDL_TYPE_LIST) return false;
    oblists = oblists->v.p.cdr;
    while (oblists)
    {
        if (oblists->v.p.car->type != MDL_TYPE_OBLIST &&
            (oblists->v.p.car->type != MDL_TYPE_ATOM ||
             !mdl_value_double_equal(oblists->v.p.car, mdl_value_atom_default))
            ) return false;
        oblists = oblists->v.p.cdr;
    }
    return true;
}

// mdl_get_atom and mdl_get_and_create_atom
// do what READ does with atoms
mdl_value_t *mdl_get_atom(const char *pname, bool insert_allowed, mdl_value_t *default_oblists)
{

    const char *trailer = strstr(pname, "!-");
    if (trailer == NULL)
    {
        return mdl_get_atom_default_oblist(pname, insert_allowed, default_oblists);
    }
    mdl_value_t *a = NULL;
    int ulen = trailer - pname;
    char *uname = (char *)alloca(ulen + 1);
    memcpy(uname, pname, ulen);
    uname[ulen] = 0;
    if (trailer[2] == '\0')
    {
        a = mdl_get_atom_from_oblist(uname, mdl_value_root_oblist);
        if (!a && insert_allowed)
            a = mdl_create_atom_on_oblist(uname, mdl_value_root_oblist);
    }
    else
    {
        mdl_value_t *oblist_name = mdl_get_atom(trailer + 2, insert_allowed, default_oblists);
        mdl_value_t *oblist = NULL;
        if (oblist_name)
        {
            oblist = mdl_internal_eval_getprop(oblist_name, mdl_value_oblist);
        }
        if (insert_allowed && !oblist)
        {
            oblist = mdl_create_oblist(oblist_name, MDL_OBLIST_HASHBUCKET_DEFAULT);
            a = mdl_create_atom_on_oblist(uname, oblist);
        }
        else if (oblist)
        {
            a = mdl_get_atom_from_oblist(uname, oblist);
            if (!a && insert_allowed)
                a = mdl_create_atom_on_oblist(uname, oblist);
        }
    }
    return a;
}

mdl_value_t *mdl_create_or_get_atom(const char *pname)
{
    mdl_value_t *a = mdl_get_atom(pname, true, NULL);
    return a;
}

// mdl_new_raw_string leaves space for a null, and puts original length on the end
// strings are made immutable by putting the one's complement of the
// original length on the end instead

// the beginning of a string is always v.s.p + v.s.l - len,
// since rest increments p and decrements l.  Assuming GC_MALLOC_ATOMIC
// returns aligned storage, the len of a string object can always be found with
// *(MDL_INT_*)ALIGN_MDL_INT(v.s.p + v.s.l + 1)
char *mdl_new_raw_string(int len, bool immutable)
{
    size_t alignlen = ALIGN_MDL_INT(len + 1);
    
    char *result = (char *)GC_MALLOC_ATOMIC(alignlen + sizeof(MDL_INT));
    memset(result, 0, alignlen);
    *((MDL_INT *)(result + alignlen)) = immutable?(~(MDL_INT)len):len;
    return result;
}

MDL_INT mdl_string_length(mdl_value_t *v)
{
    MDL_INT result = *(MDL_INT *)ALIGN_MDL_INT(v->v.s.p + v->v.s.l + 1);
    if (result < 0) result = ~result;
    return result;
}

bool mdl_string_immutable(mdl_value_t *v)
{
    MDL_INT result = *(MDL_INT *)ALIGN_MDL_INT(v->v.s.p + v->v.s.l + 1);
    return result < 0;
}

// return an empty string with length LEN
mdl_value_t *mdl_new_string(int len)
{
    mdl_value_t *result = mdl_new_mdl_value();
    result->pt = PRIMTYPE_STRING;
    result->type = MDL_TYPE_STRING;
    result->v.s.l = len;
    result->v.s.p = mdl_new_raw_string(len, false);
    return result;
}

mdl_value_t *mdl_new_string(int len, const char *s)
{
    mdl_value_t *result = mdl_new_string(len);
    strncpy(result->v.s.p, s, len);
    return result;
}

mdl_value_t *mdl_new_string(const char *s)
{
    return mdl_new_string(strlen(s),s);
}

mdl_value_t *mdl_new_word(MDL_INT fix, int type)
{
    mdl_value_t *result = mdl_new_mdl_value();
    result->pt = PRIMTYPE_WORD;
    result->type = type;
    result->v.w = fix;
    return result;
}

mdl_value_t *mdl_new_float(MDL_FLOAT flt)
{
    mdl_value_t *result = mdl_new_mdl_value();
    result->pt = PRIMTYPE_WORD;
    result->type = MDL_TYPE_FLOAT;
    result->v.fl = flt;
    return result;
}

mdl_value_t *mdl_newatomval(atom_t *a)
{
    mdl_value_t *result = mdl_new_mdl_value();
    result->pt = PRIMTYPE_ATOM;
    result->type = MDL_TYPE_ATOM;
    result->v.a = a; 
    return result;
}

// mdl_newlist returns a new list structure
mdl_value_t *mdl_newlist()
{
    mdl_value_t *r;

    r = (mdl_value_t *)GC_MALLOC(sizeof(mdl_value_t));
    r->pt = PRIMTYPE_LIST;
    r->type = MDL_TYPE_INTERNAL_LIST; // it's not a true list without the first element
    r->v.p.car = r->v.p.cdr = NULL;
    return r;
}
// mdl_make_list returns a MDL list -- the input list with an extra element at the
// beginning containing its type.  This is necessary to handle the MDL
// REST and ARGS facilities properly

mdl_value_t *mdl_make_list(mdl_value_t *l, int type)
{
    mdl_value_t *r;
    r = mdl_newlist();
    r->v.p.cdr = l;
    r->type = type;
    return r;
}

mdl_value_t *mdl_make_string(int len, char *s)
{
    // like new_string, but doesn't copy s
    mdl_value_t *result = mdl_new_mdl_value();
    result->pt = PRIMTYPE_STRING;
    result->type = MDL_TYPE_STRING;
    result->v.s.l = len;
    result->v.s.p = s;
    return result;
}

mdl_value_t *mdl_make_string(char *s)
{
    return mdl_make_string(strlen(s), s);
}

mdl_value_t *mdl_new_empty_vector(int size, int type)
{ 
    // Elements will have type and primtype 0
    // caller must add any t=LOSE/pt=WORDs required
    mdl_value_t *result = mdl_new_mdl_value();
    mdl_vector_block_t *vec = (mdl_vector_block_t *)GC_MALLOC(sizeof(mdl_vector_block_t));
    mdl_value_t *elems = (mdl_value_t *)GC_MALLOC_IGNORE_OFF_PAGE(size * sizeof(mdl_value_t));

    result->type = type;
    result->pt = PRIMTYPE_VECTOR;
    result->v.v.p = vec;
    result->v.v.offset = 0;
    vec->elements = elems;
    vec->size = size;
    vec->startoffset = 0;
    return result;
}

// TUPLE doesn't really do what TUPLE is supposed to do, but... meh
mdl_value_t *mdl_new_empty_tuple(int size, int type)
{ 
    mdl_value_t *result = mdl_new_mdl_value();
    mdl_tuple_block_t *vec = (mdl_tuple_block_t *)GC_MALLOC(sizeof(mdl_tuple_block_t) + (size - 1) * sizeof(mdl_value_t));

    result->type = type;
    result->pt = PRIMTYPE_TUPLE;
    result->v.tp.p = vec;
    result->v.tp.offset = 0;
    vec->size = size;
    return result;
}

mdl_value_t *mdl_new_empty_uvector(int size, int type)
{ 
    mdl_value_t *result = mdl_new_mdl_value();
    mdl_uvector_block_t *vec = (mdl_uvector_block_t *)GC_MALLOC(sizeof(mdl_uvector_block_t));
    uvector_element_t *elems = (uvector_element_t *)GC_MALLOC_IGNORE_OFF_PAGE(size * sizeof(uvector_element_t));

    result->type = type;
    result->pt = PRIMTYPE_UVECTOR;
    result->v.uv.p = vec;
    result->v.uv.offset = 0;
    result->v.uv.p->type = MDL_TYPE_LOSE;
    vec->elements = elems;
    vec->size = size;
    vec->startoffset = 0;
    return result;
}

// mdl_make_vector makes a vector from an internal list (with no head)
// and optionally destroys the original list;
mdl_value_t *mdl_make_vector(mdl_value_t *l, int type, bool destroy)
{
    int length = 0;
    mdl_value_t *cursor = l;
    mdl_value_t *dest;

    while (cursor) 
    {
        length++;
        cursor = cursor->v.p.cdr;
    }
    dest = mdl_new_empty_vector(length, type);
    mdl_value_t *elems = VREST(dest, 0);
    cursor = l;
    while (cursor) 
    {
        
        mdl_value_t *oldcursor = cursor;
        *elems++ = *(cursor->v.p.car);
        cursor = cursor->v.p.cdr;
        if (destroy) GC_FREE(oldcursor);
    }
    return dest;
}

// mdl_make_tuple makes a tuple from an internal list (with no head)
// and optionally destroys the original list;
mdl_value_t *mdl_make_tuple(mdl_value_t *l, int type, bool destroy)
{
    int length = 0;
    mdl_value_t *cursor = l;
    mdl_value_t *dest;

    while (cursor) 
    {
        length++;
        cursor = cursor->v.p.cdr;
    }
    dest = mdl_new_empty_tuple(length, type);
    mdl_value_t *elems = TPREST(dest, 0);
    cursor = l;
    while (cursor) 
    {
        mdl_value_t *oldcursor = cursor;
        *elems++ = *(cursor->v.p.car);
        cursor = cursor->v.p.cdr;
        if (destroy) GC_FREE(oldcursor);
    }
    return dest;
}

// mdl_make_uvector makes a uvector from an internal list (with no head)
// and optionally destroys the original list;
mdl_value_t *mdl_make_uvector(mdl_value_t *l, int type, bool destroy)
{
    int length = 0;
    mdl_value_t *cursor = l;
    mdl_value_t *dest;

    while (cursor) 
    {
        length++;
        cursor = cursor->v.p.cdr;
    }
    dest = mdl_new_empty_uvector(length, type);
    if (length)
    {
        if (!mdl_valid_uvector_primtype(l->v.p.car->pt))
        {
            mdl_error("Invalid type for UVECTOR");
        }
        UVTYPE(dest) = l->v.p.car->type;
    }
    uvector_element_t *elems = UVREST(dest, 0);
    cursor = l;
    while (cursor) 
    {
        
        mdl_value_t *oldcursor = cursor;
        if (UVTYPE(dest) != cursor->v.p.car->type)
        {
            return mdl_call_error("TYPES-DIFFER-IN-UNIFORM-VECTOR", NULL);
        }
        mdl_uvector_value_to_element(cursor->v.p.car, elems++);
        cursor = cursor->v.p.cdr;
        if (destroy) GC_FREE(oldcursor);
    }
    return dest;
}

// mdl_additem adds b as the last member of a
// a must be primtype LIST or NULL
// a is modified if it is not null
mdl_value_t *mdl_additem(mdl_value_t *a, mdl_value_t *b, mdl_value_t **lastitem)
{
    if (!b)
    {
        printf("Additem NULL\n");
        if (lastitem) *lastitem = a; // not right, but I don't want to iterate for this special case
        return a;
    }
    
    if (a == NULL)
    {
        a = mdl_newlist();
        a->v.p.car = b;
        if (lastitem) *lastitem = a;
    }
    else if (a->pt != PRIMTYPE_LIST )
    {
        mdl_error("Can't add an item to a non-list");
        mdl_print_value(stderr, a);
        printf("\n");
        return NULL;
    }
    else
    {
        mdl_value_t *c = a;
        /* this ain't LISP, lists are always null terminated, if they terminate */
        while (c->v.p.cdr != NULL)
        {
            c = c->v.p.cdr;
        }
        
        mdl_value_t *n = mdl_newlist();
        n->v.p.car = b;
        c->v.p.cdr = n;
        if (lastitem) *lastitem = n;
    }
    return a;
}

// mdl_additem adds b as the last member of a
mdl_value_t *mdl_additem_a(mdl_value_t *a, atom_t *b)
{
    return mdl_additem(a, mdl_newatomval(b));
}

// mdl_cons_internal adds item a to the beginning of internal (no header pointer) list B and returns the resulting list
mdl_value_t *mdl_cons_internal(mdl_value_t *a, mdl_value_t *b)
{
    mdl_value_t *nl = mdl_newlist();
    nl->v.p.car = a;
    nl->v.p.cdr = b;
    return nl;
}

bool mdl_primtype_nonstructured(int pt)
{
    switch (pt)
    {
    case PRIMTYPE_LIST:
    case PRIMTYPE_VECTOR:
    case PRIMTYPE_UVECTOR:
    case PRIMTYPE_TUPLE:
    case PRIMTYPE_BYTES:
    case PRIMTYPE_STRING:
        return false;
    }
    return true;
    // unimplemented primtypes may not be accurate!
}

// INPUT/OUTPUT support

// channel number to file mapping (ick)
typedef std::vector<FILE *> chanfilemap_t;
chanfilemap_t chanfilemap;

int mdl_new_chan_num(FILE *f)
{
    chanfilemap_t::reverse_iterator iter;
    int i;

    for (i = chanfilemap.size(), iter = chanfilemap.rbegin(); iter != chanfilemap.rend(); iter++, i--)
    {
        if (*iter == NULL) 
        {
            *iter = f;
            return i;
        }
    }
    chanfilemap.push_back(f);
    return chanfilemap.size();
}

FILE *mdl_get_channum_file(int chnum)
{
    return chanfilemap[chnum - 1];
}

int mdl_get_chan_channum(mdl_value_t *chan)
{
    return VITEM(chan,CHANNEL_SLOT_CHNUM)->v.w;
}

void mdl_set_chan_mode(mdl_value_t *chan, const char *mode)
{
    *VITEM(chan, CHANNEL_SLOT_MODE) = *mdl_new_string(mode);
}

mdl_value_t *mdl_get_chan_mode(mdl_value_t *chan)
{
    return VITEM(chan, CHANNEL_SLOT_MODE);
}

bool mdl_chan_mode_is_input(mdl_value_t *chan)
{
    mdl_value_t *mode = mdl_get_chan_mode(chan);
    if (mode->v.s.l < 4) return false;
    return !strncmp("READ", mode->v.s.p, 4);
}

bool mdl_chan_mode_is_output(mdl_value_t *chan)
{
    mdl_value_t *mode = mdl_get_chan_mode(chan);
    if (mode->v.s.l < 5) return false;
    return !strncmp("PRINT", mode->v.s.p, 5);
}

bool mdl_chan_mode_is_read_binary(mdl_value_t *chan)
{
    mdl_value_t *mode = mdl_get_chan_mode(chan);
    if (mode->v.s.l != 5) return false;
    return !memcmp("READB", mode->v.s.p, 5);
}

bool mdl_chan_mode_is_print_binary(mdl_value_t *chan)
{
    mdl_value_t *mode = mdl_get_chan_mode(chan);
    if (mode->v.s.l != 6) return false;
    return !memcmp("PRINTB", mode->v.s.p, 6) || !memcmp("PRINTO", mode->v.s.p, 6);
}

void mdl_set_chan_file(int chnum, FILE *f)
{
    chanfilemap[chnum - 1] = f;
}

void mdl_free_chan_file(int chnum)
{
    chanfilemap[chnum - 1] = NULL;
}

// Create and return a new and unopened channel
mdl_value_t *mdl_internal_create_channel()
{
    int i;
    mdl_value_t *cvec = mdl_new_empty_vector(CHANNEL_NSLOTS, MDL_TYPE_CHANNEL);
    mdl_value_t *zerofix = mdl_new_fix(0);
    mdl_value_t *nullstring = mdl_new_string(0);

    cvec = mdl_internal_eval_rest_i(cvec, CHANNEL_SLOT_OFFSET);
    cvec->type = MDL_TYPE_CHANNEL;
    *VITEM(cvec, CHANNEL_SLOT_TRANSCRIPT) = *mdl_make_list(NULL);
    *VITEM(cvec, CHANNEL_SLOT_DEVDEP) = *zerofix;
    *VITEM(cvec, CHANNEL_SLOT_CHNUM) = *zerofix;
    for (i = CHANNEL_SLOT_MODE; i <= CHANNEL_SLOT_DIRN; i++)
    {
        *VITEM(cvec, i) = *nullstring;
    }
    for (i = CHANNEL_SLOT_STATUS; i <= CHANNEL_SLOT_SINK; i++)
    {
        *VITEM(cvec, i) = *zerofix;
    }
    VITEM(cvec, CHANNEL_SLOT_RADIX)->v.w = 10;
    return cvec;
}

const char *mdl_get_chan_os_mode(mdl_value_t *chan)
{
    counted_string_t *chanmode = &VITEM(chan,CHANNEL_SLOT_MODE)->v.s;
    if (mdl_string_equal_cstr(chanmode, "READ")) return "r";
    if (mdl_string_equal_cstr(chanmode, "READB")) return "rb";
    if (mdl_string_equal_cstr(chanmode, "PRINT")) return "w";
    if (mdl_string_equal_cstr(chanmode, "PRINTB")) return "wb";
    if (mdl_string_equal_cstr(chanmode, "PRINTO")) return "rb+";
    return NULL;
}

char *mdl_getcwd()
{
    int bsize = 256;
    char *cwdbuf ;
    char *cwdp;
    do
    {
        cwdbuf = (char *)GC_MALLOC_ATOMIC(bsize);
        cwdp = getcwd(cwdbuf, bsize);
        bsize = bsize << 2;
    }
    while (cwdbuf != NULL && cwdp == NULL && errno == ERANGE);
    return cwdp;
}

void *mdl_memrchr(const void *s, int c, size_t n)
{
    const unsigned char *p = (const unsigned char *)s + n;
    while (p-- != s) if (*p == c) return (void *)p;
    return NULL;
}

void mdl_decode_file_args(mdl_value_t **name1p, mdl_value_t **name2p, mdl_value_t **devicep, mdl_value_t **dirp)
{
    mdl_value_t *name1 = *name1p;
    mdl_value_t *name2 = *name2p;
    mdl_value_t *device = *devicep;
    mdl_value_t *dir = *dirp;
    if (name1 && !name2)
    {
        // a filespec
        char *slashp, *dotp;
#ifdef DECODE_TENEX_FILESPECS
        char *ltp;
        char *gtp;

        // handle the TENEX <DIR>FILEN1.FN2 and DEV:<DIR>FILEN1.NF2 cases
        if (name1->v.s.l > 1 && 
            (ltp = (char *)memchr(name1->v.s.p, '<', name1->v.s.l)) &&
            (gtp = (char *)memchr(name1->v.s.p, '>', name1->v.s.l)) &&
            ltp < gtp &&
            (ltp == name1->v.s.p || ltp[-1] == ':')
            )
        {
            int name1len = name1->v.s.l;
            char *name1p = name1->v.s.p;
            if (ltp != name1p)
            {
                device = mdl_new_string(ltp - name1p, name1p);
                name1len -= ltp - name1p;
                name1p = ltp;
            }
            name1len -= (gtp - name1p) + 1;

            dir = mdl_new_string(gtp - name1p - 1);
            memcpy(dir->v.s.p, name1p + 1, gtp - name1p - 1);
            dotp = (char *)mdl_memrchr(gtp, '.', name1len + 1);
            if (dotp)
            {
                name2 = mdl_new_string(name1->v.s.p + name1->v.s.l - dotp - 1, dotp+1);
                name1len = dotp - gtp - 1;
            }
            else  // do not add an extension to filespecs
            {
                name2 = mdl_new_string(0);
            }
//            fprintf(stderr, "TENEX %s ", name1->v.s.p);
            name1 = mdl_new_string(name1len, gtp+1);
//            fprintf(stderr, "= %s %s %s %s\n", device?device->v.s.p:"default", dir->v.s.p, name1->v.s.p, name2->v.s.p);
        }
#endif
        slashp = (char *)mdl_memrchr(name1->v.s.p, '/', name1->v.s.l);
        dotp = (char *)mdl_memrchr(name1->v.s.p, '.', name1->v.s.l);

        if (slashp || dotp)
        {
            char * name1start = name1->v.s.p;
            int name1len = name1->v.s.l;
            if (slashp)
            {
                dir = mdl_new_string(slashp - name1->v.s.p + 1, name1->v.s.p);
                name1start = slashp + 1;
                name1len = name1->v.s.p + name1->v.s.l - name1start;
            }
            if (!slashp || (dotp && dotp > slashp))
            {
                name2 = mdl_new_string(name1->v.s.p + name1->v.s.l - dotp - 1, dotp+1);

                name1len -= name1->v.s.p + name1->v.s.l - dotp;
            }
            else  // do not add an extension to filespecs
            {
                name2 = mdl_new_string(0);
            }
            name1 = mdl_new_string(name1len, name1start);
        }
        // no slash and no dot means interpret this as a name, not a spec
    }
    if (!name1)
    {
        name1 = mdl_both_symbol_lookup_pname("NM1", cur_frame);
        if (!name1) name1 = mdl_new_string("INPUT");
    }
    if (!name2)
    {
        name2 = mdl_both_symbol_lookup_pname("NM2", cur_frame);
        if (!name2) name2 = mdl_new_string("MUD");
    }
    if (!device)
    {
        device = mdl_both_symbol_lookup_pname("DEV", cur_frame);
        if (!device) device = mdl_new_string("DSK");
    }
    // special case for null device
    if (mdl_string_equal_cstr(&device->v.s, "NUL"))
    {
        name1 = mdl_new_string(4,"null");
        name2 = mdl_new_string(0);
        dir = mdl_new_string(5, "/dev/");
    }
    if (!dir)
    {
        dir = mdl_both_symbol_lookup_pname("SNM", cur_frame);
        if (!dir) 
        {
            char *cwdp = mdl_getcwd();
            if (!cwdp)
                mdl_error("Unable to determine a working directory");
            dir = mdl_new_string(cwdp);
        }
    }
    *name1p = name1;
    *name2p = name2;
    *devicep = device;
    *dirp = dir;
}

char *mdl_build_pathname(mdl_value_t *name1v, mdl_value_t *name2v, mdl_value_t *devv, mdl_value_t *dirv)
{
    mdl_strbuf_t *pname = mdl_new_strbuf(256);
    
    char *name1 = name1v->v.s.p;
    char *name2 = name2v->v.s.p;
    char *dir = dirv->v.s.p;
//    char *dev = devv->v.s.p;
    int name1len = name1v->v.s.l;
    int name2len = name2v->v.s.l;
    int dirlen = dirv->v.s.l;
//    int devlen = devv->v.s.l;

    if (dir)
    {
        pname = mdl_strbuf_append_cstr(pname, dir);
        if (dir[dirlen - 1] != '/') pname = mdl_strbuf_append_cstr(pname, "/");
    }
    if (name1len) pname = mdl_strbuf_append_cstr(pname, name1);
    if (name2len)
    {
        pname = mdl_strbuf_append_cstr(pname, ".");
        pname = mdl_strbuf_append_cstr(pname, name2);
    }
    return mdl_strbuf_to_new_cstr(pname);
}

char *mdl_build_chan_pathname(mdl_value_t *chan)
{
    return mdl_build_pathname(VITEM(chan, CHANNEL_SLOT_FNARG1), VITEM(chan, CHANNEL_SLOT_FNARG2), VITEM(chan, CHANNEL_SLOT_DEVNARG),VITEM(chan, CHANNEL_SLOT_DIRNARG));
}

mdl_value_t *mdl_internal_open_channel(mdl_value_t *chan)
{
    char *pathname;
    const char *osmode;
    int chnum;
    FILE *f;
    counted_string_t *dirstr;

    osmode = mdl_get_chan_os_mode(chan);
    if (osmode == NULL) mdl_error("Bad channel mode");
    pathname = mdl_build_chan_pathname(chan);

    f = fopen(pathname, osmode);
    if (f == NULL)
    {
        mdl_value_t *errfalse = NULL;
        errfalse = mdl_cons_internal(mdl_new_fix(errno), errfalse);
        errfalse = mdl_cons_internal(mdl_new_string(pathname), errfalse);
        errfalse = mdl_cons_internal(mdl_new_string(strerror(errno)), errfalse);
        return mdl_make_list(errfalse, MDL_TYPE_FALSE);
    }
    chnum = mdl_new_chan_num(f);
    VITEM(chan,CHANNEL_SLOT_STATUS)->v.w = 0;
    *VITEM(chan,CHANNEL_SLOT_CHNUM) = *mdl_new_fix(chnum);
    *VITEM(chan,CHANNEL_SLOT_FN1) = *VITEM(chan,CHANNEL_SLOT_FNARG1);
    *VITEM(chan,CHANNEL_SLOT_FN2) = *VITEM(chan,CHANNEL_SLOT_FNARG2);
    dirstr = &VITEM(chan,CHANNEL_SLOT_DIRNARG)->v.s;
    *VITEM(chan,CHANNEL_SLOT_DIRN) = *VITEM(chan,CHANNEL_SLOT_DIRNARG);
    if ((dirstr->l == 0) || (dirstr->p[0] != '/'))
    {
        char *wd = mdl_getcwd();
        if (wd)
        {
            mdl_strbuf_t *absdir = mdl_new_strbuf(256);
            absdir = mdl_strbuf_append_cstr(absdir, wd);
            if (wd[strlen(wd)-1] != '/') absdir = mdl_strbuf_append_cstr(absdir, "/");
            absdir = mdl_strbuf_append_cstr(absdir, VITEM(chan,CHANNEL_SLOT_DIRNARG)->v.s.p);
            *VITEM(chan,CHANNEL_SLOT_DIRN) = *mdl_new_string(mdl_strbuf_to_const_cstr(absdir));
        }
    }
    if (!isatty(fileno(f)))
        *VITEM(chan,CHANNEL_SLOT_DEVN) = *VITEM(chan,CHANNEL_SLOT_DEVNARG);
    else
        *VITEM(chan,CHANNEL_SLOT_DEVN) = *(mdl_new_string(3, "TTY"));
    return chan;
}

mdl_value_t *mdl_internal_reset_channel(mdl_value_t *chan)
{
    char *pathname;
    const char *osmode;
    int chnum;
    FILE *f;
    FILE *oldf;

    if ((chnum = VITEM(chan,CHANNEL_SLOT_CHNUM)->v.w) == 0)
        return mdl_internal_open_channel(chan);
    
    osmode = mdl_get_chan_os_mode(chan);
    if (osmode == NULL) mdl_error("Bad channel mode");
    pathname = mdl_build_pathname(VITEM(chan, CHANNEL_SLOT_FN1), VITEM(chan, CHANNEL_SLOT_FN2), VITEM(chan, CHANNEL_SLOT_DEVN),VITEM(chan, CHANNEL_SLOT_DIRN));
    oldf = mdl_get_channum_file(chnum);
    if (oldf == stdin || oldf == stdout)
    {
        f = oldf;
    }
    else
    {
        if (oldf) fclose(oldf);
        
        f = fopen(pathname, osmode);
    }
    if (f == NULL)
    {
        mdl_value_t *errfalse = NULL;
        mdl_free_chan_file(chnum);

        VITEM(chan,CHANNEL_SLOT_CHNUM)->v.w = 0;
        errfalse = mdl_cons_internal(mdl_new_fix(errno), errfalse);
        errfalse = mdl_cons_internal(mdl_new_string(pathname), errfalse);
        errfalse = mdl_cons_internal(mdl_new_string(strerror(errno)), errfalse);
        return mdl_make_list(errfalse, MDL_TYPE_FALSE);
    }
    mdl_set_chan_file(chnum, f);
    VITEM(chan,CHANNEL_SLOT_STATUS)->v.w = 0;
    if (!isatty(fileno(f)))
        *VITEM(chan,CHANNEL_SLOT_DEVN) = *VITEM(chan,CHANNEL_SLOT_DEVNARG);
    else
        *VITEM(chan,CHANNEL_SLOT_DEVN) = *(mdl_new_string(3, "TTY"));
    return chan;
}

// internal_reopen_channel re-opens a channel after a restore
mdl_value_t *mdl_internal_reopen_channel(mdl_value_t *chan)
{
    char *pathname;
    const char *osmode;
    int chnum;
    FILE *f;
    off_t seekpos;
    bool seekend;

    osmode = mdl_get_chan_os_mode(chan);
    if (osmode == NULL) mdl_error("Bad channel mode");
    seekend = osmode[0] == 'w';
    if (!strcmp(osmode, "w")) osmode = "r+";
    if (!strcmp(osmode, "wb")) osmode = "rb+";

    pathname = mdl_build_pathname(VITEM(chan, CHANNEL_SLOT_FN1), VITEM(chan, CHANNEL_SLOT_FN2), VITEM(chan, CHANNEL_SLOT_DEVN),VITEM(chan, CHANNEL_SLOT_DIRN));
    f = fopen(pathname, osmode);
//    fprintf(stderr, "RE-opened %s = %p\n", pathname, f);
    if (f == NULL)
    {
        mdl_value_t *errfalse = NULL;

        VITEM(chan,CHANNEL_SLOT_CHNUM)->v.w = 0;
        errfalse = mdl_cons_internal(mdl_new_fix(errno), errfalse);
        errfalse = mdl_cons_internal(mdl_new_string(pathname), errfalse);
        errfalse = mdl_cons_internal(mdl_new_string(strerror(errno)), errfalse);
        return mdl_make_list(errfalse, MDL_TYPE_FALSE);
    }
    chnum = mdl_new_chan_num(f);
    VITEM(chan,CHANNEL_SLOT_CHNUM)->v.w = chnum;
    VITEM(chan,CHANNEL_SLOT_STATUS)->v.w = 0;
    if (!isatty(fileno(f)))
    {
        if (seekend)
        {
            fseek(f, 0, SEEK_END);
        }
        {
            seekpos = (off_t)VITEM(chan,CHANNEL_SLOT_PTR)->v.w;
            fseek(f, seekpos, SEEK_SET);
        }
        *VITEM(chan,CHANNEL_SLOT_DEVN) = *VITEM(chan,CHANNEL_SLOT_DEVNARG);
    }
    else
        *VITEM(chan,CHANNEL_SLOT_DEVN) = *(mdl_new_string(3, "TTY"));
    return chan;
}

mdl_value_t *mdl_internal_close_channel(mdl_value_t *chan)
{
    int chnum = VITEM(chan,CHANNEL_SLOT_CHNUM)->v.w;
    mdl_value_t *nullstring = mdl_new_string(0);
    FILE *f;
    int err;
    
    if (!chnum) return chan; // already closed
    f = mdl_get_channum_file(chnum);
    mdl_free_chan_file(chnum);
    err = fclose(f);

    VITEM(chan,CHANNEL_SLOT_CHNUM)->v.w = 0;
    *VITEM(chan,CHANNEL_SLOT_FN1) = *nullstring;
    *VITEM(chan,CHANNEL_SLOT_FN2) = *nullstring;
    *VITEM(chan,CHANNEL_SLOT_DEVN) = *nullstring;
    *VITEM(chan,CHANNEL_SLOT_DIRN) = *nullstring;
    if (err) mdl_error("fclose failed");
    return chan;
}

mdl_value_t *mdl_make_localvar_ref(mdl_value_t *a, int reftype)
{
    mdl_value_t *r;

    r = (mdl_value_t *)mdl_newlist();
    r->v.p.cdr = mdl_newlist();
    r->v.p.cdr->v.p.car = a;
    r->v.p.car = mdl_get_atom_from_oblist("LVAL", mdl_value_root_oblist);
    return mdl_make_list(r, reftype);
}

mdl_value_t *mdl_make_globalvar_ref(mdl_value_t *a, int reftype)
{
    mdl_value_t *r;

    r = (mdl_value_t *)mdl_newlist();
    r->v.p.cdr = mdl_newlist();
    r->v.p.cdr->v.p.car = a;
    r->v.p.car = mdl_get_atom_from_oblist("GVAL", mdl_value_root_oblist);
    return mdl_make_list(r, reftype);
}

mdl_value_t *mdl_make_quote(mdl_value_t *a, int qtype)
{
    mdl_value_t *r;

    r = (mdl_value_t *)mdl_newlist();
    r->v.p.cdr = mdl_newlist();
    r->v.p.cdr->v.p.car = a;
    r->v.p.car = mdl_get_atom_from_oblist("QUOTE", mdl_value_root_oblist);
    return mdl_make_list(r, qtype);
}

mdl_frame_t *mdl_new_frame()
{
    mdl_frame_t *r = (struct mdl_frame_t *)GC_MALLOC(sizeof(mdl_frame_t));
    // GC_MALLOC does a clear, so no need to clear anything
    r->syms = new (UseGC)mdl_local_symbol_table_t();
    return r;
}

mdl_value_t *mdl_make_frame_value(mdl_frame_t *frame, int t = MDL_TYPE_FRAME)
{
    mdl_value_t *result = mdl_new_mdl_value();
    result->pt = PRIMTYPE_FRAME;
    result->type = t;
    result->v.f = frame
;
    return result;
}

inline mdl_frame_t *mdl_push_frame(mdl_frame_t *frame)
{
    frame->prev_frame = cur_frame;
    cur_frame = frame;
    return frame->prev_frame;
}

mdl_frame_t *mdl_pop_frame(mdl_frame_t *frame)
{
    mdl_local_symbol_table_t::iterator iter;
    if (frame != cur_frame->prev_frame)
        mdl_error("Frames confused");
#ifdef CACHE_LOCAL_SYMBOLS
    for (iter = cur_frame->syms->begin(); iter != cur_frame->syms->end(); iter++)
    {
        if ((iter->second.atom->bindid == cur_process_bindid) &&
            (iter->second.atom->binding == &iter->second))
            iter->second.atom->binding = iter->second.prev_binding;
    }
#endif

    cur_frame = frame;
    return cur_frame;
}
                          
mdl_interp_frame_t *mdl_new_interp_frame() {
    mdl_interp_frame_t *new_frame = new (UseGC)mdl_interp_frame_t;
    memset(new_frame, 0, sizeof(mdl_interp_frame_t));
    return new_frame;
}

void mdl_push_interp_frame(mdl_interp_frame_t *iframe) {
    iframe->prev_frame = mdl_interp_stack;
    mdl_interp_stack = iframe;
}

int mdl_setjmp(mdl_interp_frame_t *&iframe) {
    iframe = mdl_interp_stack;
    int old_jumpval = iframe->jumpval;    
    iframe->jumpval = 0;
    return old_jumpval;
}

void mdl_push_interp_longjmp_to(mdl_frame_t *jump_frame, int value) 
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(mdl_new_fix(value), args);
    args = mdl_cons_internal(mdl_make_frame_value(jump_frame), args);
    new_frame->args = args;
    new_frame->return_to = RETURN_DISCARD;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_LONGJMP_TO;
    mdl_push_interp_frame(new_frame);
}
#define mdl_longjmp_to(jump_frame, value) do { mdl_push_interp_longjmp_to(jump_frame, value); return NULL; } while(0)
mdl_value_t *mdl_do_longjmp_to() {
    IARGSETUP();
    IGETNEXTARG(jump_frame);
    IGETNEXTARG(jump_val);
    if (cur_frame->frame_flags & MDL_FRAME_FLAGS_UNWIND) {
        // don't want to unwind twice the same unwind.
        cur_frame->frame_flags ^= MDL_FRAME_FLAGS_UNWIND;
        mdl_value_t *unwind_val = mdl_internal_eval_nth_i(cur_frame->args, 2);
        mdl_interp_stack->started = false;
        // MDL manual says this eval is done "in the environment that was present when
        // the call to unwind is made".  Here we're doing it with the UNWIND frame present.
        // Same ENVIRONMENT, though, so maybe that's correct.
        mdl_push_interp_eval(RETURN_COPYFORWARD, unwind_val);
        return NULL;
    }
    if (cur_frame == jump_frame->v.f) {
        NEW_BINDID(cur_process_bindid);
        mdl_interp_stack->prev_frame = cur_frame->interp_frame2;
        mdl_interp_stack->prev_frame->jumpval = jump_val->v.w;
        mdl_interp_stack->prev_frame->started = false;
    } else {
        mdl_pop_frame(cur_frame->prev_frame);
        mdl_interp_stack->started = false;
        if (!cur_frame) mdl_error("Tried to jump to frame not on stack!");
    }
    return NULL;
}

void mdl_error(const char *err)
{
    fflush(stdout);
    fprintf(stderr, "%s\n",err);
    if (initial_frame)
    {
//        fprintf(stderr, "Error to initial jumpbuf\n");
        cur_frame = initial_frame;
        NEW_BINDID(cur_process_bindid);
        mdl_interp_stack = initial_frame->interp_frame2;
//        mdl_longjmp(initial_frame->interp_frame, LONGJMP_ERROR);
    }
    fprintf(stderr, "Fatal: Lost my stack\n");
    exit(-1);
}

mdl_value_t *mdl_call_error_ext(const char *erratom, const char *errstr, ...)
{
    va_list gp;
    mdl_value_t *arglist;
    mdl_value_t *lastitem;
    mdl_value_t *arg;

    va_start(gp, errstr);
    arglist = mdl_additem(NULL, mdl_value_builtin_error, &lastitem);
    // FIXME -- use atom instead of string for first arg
    mdl_additem(lastitem, mdl_new_string(erratom), &lastitem);
    mdl_additem(lastitem, mdl_new_string(errstr), &lastitem);
    while ((arg = va_arg(gp, mdl_value_t *)))
    {
        mdl_additem(lastitem, arg, &lastitem);
    }
    va_end(gp);
    // Maybe should be std apply?
    mdl_push_interp_apply(RETURN_COPYNEXT, mdl_value_builtin_error, mdl_make_list(arglist));
    return NULL;
}

mdl_value_t *mdl_call_error(const char *errstr, ...)
{
    va_list gp;
    mdl_value_t *arglist;
    mdl_value_t *lastitem;
    mdl_value_t *arg;

    va_start(gp, errstr);
    arglist = mdl_additem(NULL, mdl_value_builtin_error, &lastitem);
    mdl_additem(lastitem, mdl_new_string(errstr), &lastitem);
    while ((arg = va_arg(gp, mdl_value_t *)))
    {
        mdl_additem(lastitem, arg, &lastitem);
    }
    va_end(gp);
    // Maybe should be std apply?
    mdl_push_interp_apply(RETURN_COPYNEXT, mdl_value_builtin_error, mdl_make_list(arglist));
    return NULL;
}

mdl_value_t *mdl_parse_string(mdl_value_t *str, int radix, mdl_value_t *lookahead)
{
    mdl_value_t *chan = mdl_internal_create_channel();
    mdl_set_chan_mode(chan, "READ");
    mdl_set_chan_eof_object(chan, NULL); // error out on illegal object
    mdl_set_chan_input_source(chan, str);
    if (lookahead)
        mdl_set_chan_lookahead(chan, lookahead->v.w);
    return mdl_read_object(chan);
}

mdl_value_t *mdl_global_symbol_lookup(const atom_t *atom)
{
    mdl_symbol_table_t::iterator iter;

    iter = global_syms.find(atom);
    if (iter == global_syms.end()) return NULL;
    return iter->second.binding;
}

// It's probable this should be 1_function_only_please.
mdl_value_t *mdl_local_symbol_lookup_1_activation_only_please(const atom_t *atom, mdl_frame_t *frame)
{
    mdl_local_symbol_table_t::iterator iter;

    while (frame)
    {
        iter = frame->syms->find(atom);
        if (iter != frame->syms->end())
            return iter->second.binding;
        if (frame->frame_flags & MDL_FRAME_FLAGS_FUNCTION) return NULL;
        frame = frame->prev_frame;
    }
    return NULL;
}

mdl_local_symbol_t *mdl_local_symbol_slot(atom_t *atom, mdl_frame_t *frame)
{
#ifdef CACHE_LOCAL_SYMBOLS
    bool fixbind;
#endif

    if (!frame)
    {
        mdl_error("Bad frame passed to local symbol lookup");
    }


#ifdef CACHE_LOCAL_SYMBOLS
    fixbind = frame == cur_frame;
    // shortcut -- use atom's bind value
    if (fixbind && atom->bindid == cur_process_bindid && atom->binding)
    {
        return atom->binding;
    }
#endif

    mdl_local_symbol_table_t::iterator iter;


    while (frame)
    {
        iter = frame->syms->find(atom);
        if (iter != frame->syms->end()) break;
        frame = frame->prev_frame;
    }
    if (!frame) return NULL;

#ifdef CACHE_LOCAL_SYMBOLS
    if (fixbind)
    {
        atom->binding = &iter->second;
        atom->bindid = cur_process_bindid;
    }
#endif
    return &iter->second;
}

mdl_value_t *mdl_bind_local_symbol(atom_t *atom, mdl_value_t *val, mdl_frame_t *frame, bool allow_replacement)
{
    mdl_local_symbol_table_t::iterator iter;
#ifdef CACHE_LOCAL_SYMBOLS
    bool fixbind = frame == cur_frame;
#endif

    iter = frame->syms->find(atom);
    if (iter != frame->syms->end())
    {
        if (!allow_replacement) return NULL;
        iter->second.binding = val;
    }
    else
    {
#ifdef CACHE_LOCAL_SYMBOLS
        mdl_local_symbol_t *oldsym = mdl_local_symbol_slot(atom, frame);
#endif
        mdl_local_symbol_t sym;
        std::pair<mdl_local_symbol_table_t::iterator, bool> insresult;
        sym.atom = atom;
        sym.binding = val;
#ifdef CACHE_LOCAL_SYMBOLS
        sym.prev_binding = oldsym;
#endif
        insresult = frame->syms->insert(std::pair<const atom_t *, mdl_local_symbol_t>(atom, sym));
#ifdef CACHE_LOCAL_SYMBOLS
        if (fixbind)
        {
            atom->binding = &insresult.first->second;
            atom->bindid = cur_process_bindid;
        }
        else
            atom->binding = NULL;
#endif
    }
    return val;
}

mdl_value_t *mdl_local_symbol_lookup(atom_t *atom, mdl_frame_t *frame)
{
    mdl_local_symbol_t *sym = mdl_local_symbol_slot(atom, frame);
    if (!sym) return NULL;
    return sym->binding;
}

mdl_value_t *mdl_local_symbol_lookup_pname(const char *pname, mdl_frame_t *frame)
{
    mdl_value_t *av = mdl_get_atom(pname, true, NULL);
    if (!av) return NULL;
    return mdl_local_symbol_lookup(av->v.a, frame);
}

// VALUE style lookup, first local then global, never returning
// UNBOUND
mdl_value_t *mdl_both_symbol_lookup(atom_t *atom, mdl_frame_t *frame)
{
    mdl_value_t *result;
    result = mdl_local_symbol_lookup(atom, frame);
    if (!result || result->type == MDL_TYPE_UNBOUND)
        result = mdl_global_symbol_lookup(atom);
    if (!result || result->type == MDL_TYPE_UNBOUND) return NULL;
    return result;
}

mdl_value_t *mdl_both_symbol_lookup_pname(const char *pname, mdl_frame_t *frame)
{
    mdl_value_t *result;
    mdl_value_t * av = mdl_get_atom(pname, true, NULL);
    if (!av) return NULL;
    result = mdl_local_symbol_lookup(av->v.a, frame);
    if (!result || result->type == MDL_TYPE_UNBOUND)
        result = mdl_global_symbol_lookup(av->v.a);
    if (!result || result->type == MDL_TYPE_UNBOUND) return NULL;
    return result;
}


enum mdl_argstate_t {
    ARGSTATE_INITIAL, // looking for atoms or string
    ARGSTATE_BIND,    // just got the bind, looking for one atom
    ARGSTATE_ATOMS,   // looking for atoms or string other than BIND
    ARGSTATE_CALL,    // looking for a single atom
    ARGSTATE_OPTIONAL,// looking for atoms, 2 lists, or string
    ARGSTATE_ARGS,    // looking for a single atom
    ARGSTATE_TUPLE,   // looking for a single atom
    ARGSTATE_ANONLY,  // No more args, looking for AUX or NAME
    ARGSTATE_AUX,     // looking for atoms or 2-lists
    ARGSTATE_NAME,    // looking for atom
    ARGSTATE_NOMORE,  // that's it, nothing else
    ARGSTATE_EVALED,  // fake argstate
    ARGSTATE_INITIAL_EVALED = ARGSTATE_INITIAL + ARGSTATE_EVALED,
    ARGSTATE_ATOMS_EVALED = ARGSTATE_ATOMS + ARGSTATE_EVALED,
    ARGSTATE_OPTIONAL_EVALED = ARGSTATE_OPTIONAL + ARGSTATE_EVALED,
    ARGSTATE_TUPLE_EVALED = ARGSTATE_TUPLE + ARGSTATE_EVALED,
    ARGSTATE_AUX_EVALED = ARGSTATE_AUX + ARGSTATE_EVALED,
};

// bind arguments to function/prog/repeat in frame
// Current frame must be frame prior to prog/repeat/bind
// When bind is done, current frame will be input frame.
mdl_value_t *mdl_bind_args(mdl_value_t *fargs,
                   mdl_value_t *apply_to, // functions only
                   mdl_frame_t *frame,
                   bool called_from_apply_subr,
                   bool auxonly)
{
    mdl_value_t *argptr = NULL;
    if (apply_to) argptr = LREST(apply_to, 1);

    if (!fargs || fargs->type != MDL_TYPE_LIST)
        mdl_error("Formal arguments must be LIST");

    mdl_value_t *fargp = LREST(fargs, 0);
    mdl_argstate_t argstate = ARGSTATE_INITIAL;
    if (auxonly) argstate = ARGSTATE_AUX;
    mdl_interp_frame_t *my_iframe = mdl_interp_stack;
    mdl_interp_stack = mdl_interp_stack->prev_frame;
    mdl_push_interp_bind_arg_state_machine(my_iframe->return_to, fargp, argptr, mdl_new_fix((int)argstate), mdl_new_fix(0),
                                           apply_to, mdl_make_frame_value(frame),
                                           mdl_boolean_value(called_from_apply_subr));
    return NULL;
}

// bind arguments to function/prog/repeat in frame
// Current frame must be frame prior to prog/repeat/bind
// When bind is done, current frame will be input frame.
mdl_value_t *mdl_push_interp_bind_args(mdl_interp_return_t return_to,
                                       mdl_value_t *fargs,
                                       mdl_value_t *apply_to, // functions only
                                       mdl_frame_t *frame,
                                       bool called_from_apply_subr,
                                       bool auxonly)
{
    mdl_value_t *argptr = NULL;
    if (apply_to) argptr = LREST(apply_to, 1);

    if (!fargs || fargs->type != MDL_TYPE_LIST)
        mdl_error("Formal arguments must be LIST");

    mdl_value_t *fargp = LREST(fargs, 0);
    mdl_argstate_t argstate = ARGSTATE_INITIAL;
    if (auxonly) argstate = ARGSTATE_AUX;
    mdl_push_interp_bind_arg_state_machine(return_to, fargp, argptr, mdl_new_fix((int)argstate), mdl_new_fix(0),
                                           apply_to, mdl_make_frame_value(frame),
                                           mdl_boolean_value(called_from_apply_subr));
    return NULL;
}

mdl_value_t *mdl_bind_arg_state_machine()
{
    mdl_interp_frame_t* own_interp_frame = mdl_interp_stack;
    IARGSETUP();
    IGETNEXTARG(fargp);
    IGETNEXTARG(argptr);
    IGETNEXTARG(argstate_val);
    IGETNEXTARG(args_processed_val);
    IGETNEXTARG(apply_to);
    IGETNEXTARG(frame_val);
    IGETNEXTARG(called_from_apply_subr_value);
    int args_processed = args_processed_val->v.w;
    mdl_argstate_t argstate = (mdl_argstate_t)argstate_val->v.w;
    mdl_frame_t *frame = frame_val->v.f;
    bool called_from_apply_subr = mdl_is_true(called_from_apply_subr_value);
    mdl_value_t *retval = NULL;

    // fargp can be null if there are no formal arguments.
    if (fargp != NULL) {
        mdl_value_t *farg = fargp->v.p.car;
        mdl_value_t *default_val = &mdl_value_unassigned;
        mdl_value_t *mdl_value_atom_quote = mdl_get_atom_from_oblist("QUOTE", mdl_value_root_oblist);

        if (farg->type == MDL_TYPE_LIST)
        {
            default_val = LITEM(farg, 1);
            if (!default_val || LHASITEM(farg, 2))
                mdl_error("Only lists allowed in arg lists are 2-lists");
            farg = LITEM(farg, 0);
            if (farg->type != MDL_TYPE_FORM && farg->type != MDL_TYPE_ATOM)
                mdl_error("First element in 2-list must be atom or quoted atom");
            if (argstate != ARGSTATE_OPTIONAL && argstate != ARGSTATE_AUX && argstate != ARGSTATE_OPTIONAL_EVALED && argstate != ARGSTATE_AUX_EVALED)
                return mdl_call_error_ext("FIXME", "2-lists allowed only in OPTIONAL or AUX sections", NULL);
        }
    
        if (farg->type == MDL_TYPE_STRING)
        {
            if (mdl_string_equal_cstr(&farg->v.s, "BIND"))
            {
                if (argstate != ARGSTATE_INITIAL)
                    mdl_error("BIND must be first thing in argument list");
                argstate = ARGSTATE_BIND;
            }
            else if (mdl_string_equal_cstr(&farg->v.s, "CALL"))
            {
                if ((argstate != ARGSTATE_INITIAL &&
                     argstate != ARGSTATE_ATOMS)
                    || args_processed)
                    mdl_error("CALL must be the only argment-gatherer");
                if (called_from_apply_subr)
                    mdl_error("CALL not allowed when called from APPLY");
                argstate = ARGSTATE_CALL;
            }
            else if (mdl_string_equal_cstr(&farg->v.s, "OPT") ||
                     mdl_string_equal_cstr(&farg->v.s, "OPTIONAL"))
            {
                if (argstate != ARGSTATE_INITIAL &&
                    argstate != ARGSTATE_ATOMS)
                    mdl_error("OPTIONAL in wrong place in argument string");
                argstate = ARGSTATE_OPTIONAL;
            }
            else if (mdl_string_equal_cstr(&farg->v.s, "ARGS"))
            {
                if (argstate != ARGSTATE_INITIAL &&
                    argstate != ARGSTATE_ATOMS &&
                    argstate != ARGSTATE_OPTIONAL)
                    mdl_error("ARGS in wrong place in argument string");
                if (called_from_apply_subr)
                    mdl_error("ARGS not allowed when called from APPLY");
                argstate = ARGSTATE_ARGS;
            }
            else if (mdl_string_equal_cstr(&farg->v.s, "TUPLE"))
            {
                if (argstate != ARGSTATE_INITIAL &&
                    argstate != ARGSTATE_ATOMS && 
                    argstate != ARGSTATE_OPTIONAL
                    )
                    mdl_error("TUPLE in wrong place in argument string");
                argstate = ARGSTATE_TUPLE;
            }
            else if (mdl_string_equal_cstr(&farg->v.s, "AUX") 
                     || mdl_string_equal_cstr(&farg->v.s, "EXTRA"))
            {
                if (argstate != ARGSTATE_INITIAL &&
                    argstate != ARGSTATE_ATOMS && 
                    argstate != ARGSTATE_OPTIONAL &&
                    argstate != ARGSTATE_ANONLY
                    )
                    mdl_error("AUX/EXTRA in wrong place in argument string");
                argstate = ARGSTATE_AUX;
            }
            else if (mdl_string_equal_cstr(&farg->v.s, "NAME") 
                     || mdl_string_equal_cstr(&farg->v.s, "ACT"))
            {
                if (argstate != ARGSTATE_INITIAL &&
                    argstate != ARGSTATE_ATOMS && 
                    argstate != ARGSTATE_OPTIONAL &&
                    argstate != ARGSTATE_ANONLY &&
                    argstate != ARGSTATE_AUX
                    )
                    mdl_error("NAME/ACT in wrong place in argument string");
                argstate = ARGSTATE_NAME;
            }
        }
        else if (farg->type == MDL_TYPE_ATOM)
        {
            switch (argstate)
            {
            case ARGSTATE_INITIAL:
            case ARGSTATE_ATOMS:
            case ARGSTATE_OPTIONAL:
                if (argptr)
                {
                    mdl_value_t *arg = argptr->v.p.car;
                    if (!called_from_apply_subr)
                        mdl_push_interp_eval(RETURN_COPYFORWARD, arg); 
                    else
                        retval = arg;
                }
                else if (argstate != ARGSTATE_OPTIONAL)
                {
                    return mdl_call_error("TOO-FEW-ARGUMENTS-SUPPLIED", frame->subr, NULL);
                }
                else 
                {
                    if (default_val != &mdl_value_unassigned)
                        mdl_push_interp_eval(RETURN_COPYFORWARD, default_val, 0, NULL); 
                    else
                        retval = default_val;
                }
                argstate = (mdl_argstate_t)(argstate + ARGSTATE_EVALED);
                break;
            case ARGSTATE_INITIAL_EVALED:
            case ARGSTATE_ATOMS_EVALED:
            case ARGSTATE_OPTIONAL_EVALED:
            {
                mdl_value_t *evaled_arg = own_interp_frame->retval;
                if (!mdl_bind_local_symbol(farg->v.a, evaled_arg, frame, false)) {
                    return mdl_call_error_ext ("BAD-ARGUMENT-LIST", "Duplicate formal argument", farg, NULL);
                }
                if (argptr) {
                    argptr = argptr->v.p.cdr;
                    args_processed++;
                }
                argstate = (mdl_argstate_t)(argstate - ARGSTATE_EVALED);
                if (argstate == ARGSTATE_INITIAL) argstate = ARGSTATE_ATOMS;
                break;
            }
            case ARGSTATE_BIND:
            {
                mdl_value_t *pframeval = mdl_make_frame_value(cur_frame, MDL_TYPE_ENVIRONMENT);
                if (!mdl_bind_local_symbol(farg->v.a, pframeval, frame, false))
                    return mdl_call_error_ext ("BAD-ARGUMENT-LIST", "Duplicate formal argument", farg, NULL);
                argstate = ARGSTATE_ATOMS;
                break;
            }
            case ARGSTATE_CALL:
                if (!mdl_bind_local_symbol(farg->v.a, apply_to, frame, false))
                    return mdl_call_error_ext ("BAD-ARGUMENT-LIST", "Duplicate formal argument", farg, NULL);
                argstate = ARGSTATE_ANONLY;
                argptr = NULL;
                break;
            case ARGSTATE_ARGS:
            {
                mdl_value_t *argsval = mdl_make_list(argptr);
                if (!mdl_bind_local_symbol(farg->v.a, argsval, frame, false))
                    return mdl_call_error_ext ("BAD-ARGUMENT-LIST", "Duplicate formal argument", farg, NULL);
                argstate = ARGSTATE_ANONLY;
                argptr = NULL;
                break;
            }
            case ARGSTATE_TUPLE:
            {
                if (!called_from_apply_subr)
                {
                    mdl_value_t *argsval = mdl_make_list(argptr);
                    mdl_push_interp_std_eval(RETURN_COPYFORWARD, argsval, 0, MDL_TYPE_LIST);
                }
                else
                {
                    retval = mdl_make_tuple(argptr, MDL_TYPE_TUPLE);
                }
                argstate = ARGSTATE_TUPLE_EVALED;
                break;
            }
            case ARGSTATE_TUPLE_EVALED:
            {
                mdl_value_t* argsval = own_interp_frame->retval;
                if (!called_from_apply_subr) {
                    argsval = mdl_make_tuple(LREST(argsval, 0), MDL_TYPE_TUPLE);
                }
                if (!mdl_bind_local_symbol(farg->v.a, argsval, frame, false))
                    return mdl_call_error_ext ("BAD-ARGUMENT-LIST", "Duplicate formal argument", farg, NULL);
                argstate = ARGSTATE_ANONLY;
                argptr = NULL;
                break;
            }
            case ARGSTATE_NAME:
            {
                mdl_value_t *cframeval = mdl_make_frame_value(frame, MDL_TYPE_ACTIVATION);
                if (!mdl_bind_local_symbol(farg->v.a, cframeval, frame, false))
                    return mdl_call_error_ext ("BAD-ARGUMENT-LIST", "Duplicate formal argument", farg, NULL);
                argstate = ARGSTATE_NOMORE;
                break;
            }
            case ARGSTATE_AUX:
                if (default_val != &mdl_value_unassigned)
                    mdl_push_interp_eval(RETURN_COPYFORWARD, default_val, 0, NULL); 
                else
                    retval = default_val;
                argstate = ARGSTATE_AUX_EVALED;
                break;
            case ARGSTATE_AUX_EVALED:
            {
                mdl_value_t *default_val_evaled = own_interp_frame->retval;
                if (!mdl_bind_local_symbol(farg->v.a, default_val_evaled, frame, false))
                    return mdl_call_error_ext ("BAD-ARGUMENT-LIST", "Duplicate formal argument", farg, NULL);
                argstate = ARGSTATE_AUX;
                break;
            }
            default:
                mdl_error("Unexpected ATOM in formal argument list");
            }
        }
        else if (farg->type == MDL_TYPE_FORM)
        {
            mdl_value_t *atom = LITEM(farg, 1);
            mdl_value_t *quote = LITEM(farg, 0);
            if (!atom ||
                atom->type != MDL_TYPE_ATOM ||
                LHASITEM(farg, 2) ||
                !mdl_value_equal(quote, mdl_value_atom_quote)
                )
                mdl_error("FORM in arg list may only be <QUOTE atom>");
            if (called_from_apply_subr)
                mdl_error("Can't use APPLY to call a function with quoted arguments");
            switch (argstate)
            {
            case ARGSTATE_INITIAL:
            case ARGSTATE_ATOMS:
            case ARGSTATE_OPTIONAL:
                if (argptr)
                {
                    mdl_value_t *arg = argptr->v.p.car;
                    if (!mdl_bind_local_symbol(atom->v.a, arg, frame, false))
                        return mdl_call_error_ext ("BAD-ARGUMENT-LIST", "Duplicate formal argument", atom, NULL);
                    argptr = argptr->v.p.cdr;
                    args_processed++;
                }
                else if (argstate != ARGSTATE_OPTIONAL)
                    mdl_error("Too few args in function call");
                else
                {
                    // Despite the quote, we do want to evaluate the default arg
                    // (Zork APPLY-RANDOM demonstrates this)
                    mdl_push_interp_eval(RETURN_COPYFORWARD, default_val, 0, NULL); 
                    argstate = ARGSTATE_OPTIONAL_EVALED;
                }
                break;
            case ARGSTATE_OPTIONAL_EVALED:
            {
                mdl_value_t *evaled_arg = own_interp_frame->retval;
                if (!mdl_bind_local_symbol(atom->v.a, evaled_arg, frame, false)) {
                    return mdl_call_error_ext ("BAD-ARGUMENT-LIST", "Duplicate formal argument", atom, NULL);
                }
                if (argptr) {
                    argptr = argptr->v.p.cdr;
                    args_processed++;
                }
                argstate = ARGSTATE_OPTIONAL;
                break;
            }
            default:
                mdl_error("Unexpected quoted ATOM in formal argument list");
            }
        }
        if (argstate < ARGSTATE_EVALED) {
            fargp = fargp->v.p.cdr;
        }
    }
    // When we run out of actual arguments, push the frame (unless we need to wait for a BIND to happen)
    // This is required to make optional default and aux arguments work.  By waiting to push, we avoid switching
    // eval environments back and forth.
    if (argptr == NULL && cur_frame != frame && argstate != ARGSTATE_BIND) {
        mdl_push_frame(frame);
    }
    if (fargp) {
        // Tail recursion is fun.
        own_interp_frame->started = false;
        IARGRESET(own_interp_frame);
        ISETNEXTARG(fargp);
        ISETNEXTARG(argptr);
        ISETNEXTARG(mdl_new_fix((int)argstate));
        ISETNEXTARG(mdl_new_fix(args_processed));
    }
    else if (argptr != NULL)
        return mdl_call_error("TOO-MANY-ARGUMENTS-SUPPLIED", NULL);
    return retval;
}

mdl_value_t *mdl_internal_prog_repeat_bind(mdl_value_t *subr, mdl_value_t *args, bool bind_to_lastprog, bool repeat)
{
    mdl_frame_t *frame = mdl_new_frame();
    mdl_frame_t *prev_frame = cur_frame;
    mdl_value_t *fargsp = LREST(args, 0);
    mdl_value_t *fargs;
    mdl_value_t *act_atom = NULL;
    frame->prev_frame = prev_frame;
    
    if (fargsp->v.p.car->type == MDL_TYPE_ATOM)
    {
        act_atom = fargsp->v.p.car;
        fargsp = fargsp->v.p.cdr;
    }
    fargs = fargsp->v.p.car;
    // This activation really shouldn't be generated for BIND, but we use it internally
    mdl_value_t *activation = mdl_make_frame_value(frame, MDL_TYPE_ACTIVATION);
    frame->subr = subr;
    if (bind_to_lastprog || act_atom)
    {
        if (bind_to_lastprog)
            mdl_bind_local_symbol(mdl_value_atom_lastprog->v.a, activation, frame, false);
        if (act_atom)
            mdl_bind_local_symbol(act_atom->v.a, activation, frame, false);
    }
    
    frame->frame_flags = MDL_FRAME_FLAGS_ACTIVATION;
    if (repeat) {
        frame->frame_flags |= MDL_FRAME_FLAGS_REPEAT;
    }
    
    // Give stuff which happens during evaluation a place to go; not sure if this is right.
    memcpy(&frame->interp_frame, &cur_frame->interp_frame, sizeof(frame->interp_frame));
    mdl_push_interp_eval_body(RETURN_COPYDOWN, fargsp->v.p.cdr, activation);
    mdl_push_interp_bind_args(RETURN_COPYFORWARD,
                              fargs, NULL, frame,
                              false /* not called from apply */,
                              true /* AUX arguments only */);
    return NULL;
}

mdl_value_t *mdl_apply_function(mdl_value_t *applier, mdl_value_t *apply_to, bool called_from_apply_subr)
{
    mdl_frame_t *frame = mdl_new_frame();
    mdl_frame_t *prev_frame = cur_frame;
    mdl_value_t *fargsp, *fargs;
    mdl_value_t *fname;
    
    frame->prev_frame = prev_frame;
    frame->frame_flags = MDL_FRAME_FLAGS_ACTIVATION | MDL_FRAME_FLAGS_FUNCTION;
    fname = LITEM(apply_to, 0);
    if (fname->type == MDL_TYPE_ATOM)
    {
        frame->frame_flags |= MDL_FRAME_FLAGS_NAMED_FUNC;
        frame->subr = fname;
        // these are actually unevaluated args, but for debugging it will do
        frame->args = mdl_make_list(LREST(apply_to, 1));
    }

    if (applier->pt != PRIMTYPE_LIST)
        mdl_error("A FUNCTION must be of primtype LIST");

    fargsp = LREST(applier, 0);
    fargs = fargsp->v.p.car;
    // According to the MDL manual, an "activation" is not generated unless there is an atom which precedes the argument
    // list (below) or a "NAME" in bind_args.  However, I do not believe generating this activation in all cases 
    // is of any consequence (aside from extra garbage), as it is not visible to the program.
    mdl_value_t *activation = mdl_make_frame_value(frame, MDL_TYPE_ACTIVATION);
    if (fargs && fargs->type == MDL_TYPE_ATOM)
    {
        mdl_bind_local_symbol(fargs->v.a, activation, frame, false);
        fargsp = fargsp->v.p.cdr;
        fargs = fargsp->v.p.car;
    }

    // Give stuff which happens during evaluation a place to go; not sure if this is right.
    memcpy(&frame->interp_frame, &cur_frame->interp_frame, sizeof(frame->interp_frame));
    frame->interp_frame2 = cur_frame->interp_frame2;

    mdl_push_interp_eval_body(RETURN_COPYDOWN, fargsp->v.p.cdr, activation);

    // mdl_bind_args returns NULL on success, return from ERRET on error
    mdl_push_interp_bind_args(RETURN_COPYFORWARD, fargs, apply_to, frame,
                              called_from_apply_subr, false);
    return NULL;
}

mdl_value_t *mdl_eval_body() {
    IARGSETUP();
    IGETNEXTARG(fbodyp);
    IGETNEXTARG(activation);

    mdl_frame_t* frame = activation->v.f;
    frame->interp_frame2 = mdl_interp_stack;
    mdl_setjmp(frame->interp_frame2);
    // RETURN and AGAIN come here
    if (!frame->result) {
        mdl_value_t *fexprs = fbodyp;
        if (fexprs == NULL) {
            mdl_pop_frame(frame->prev_frame);
            return mdl_call_error("HAS-EMPTY-BODY", NULL);
        }
        mdl_push_interp_pop_frame(RETURN_COPYDOWN, mdl_make_frame_value(frame->prev_frame));
        mdl_push_interp_eval_body_state_machine(RETURN_COPYFORWARD, fexprs, activation);
        return NULL;
    } else {
        // RETURN
        mdl_pop_frame(frame->prev_frame);
        return frame->result;
    }
}

mdl_value_t *mdl_eval_body_state_machine() {
    IARGSETUP();
    IGETNEXTARG(fexprs);
    IGETNEXTARG(activation);
    mdl_frame_t* frame = activation ? activation->v.f : NULL;
    mdl_interp_frame_t* own_interp_frame = mdl_interp_stack;
    mdl_value_t* retval = own_interp_frame->retval;

    if (fexprs != NULL) {
        own_interp_frame->started = false;  // tail recursion
        IARGRESET(own_interp_frame);
        ISETNEXTARG(fexprs->v.p.cdr);
        mdl_push_interp_eval(RETURN_COPYFORWARD, fexprs->v.p.car, 0);
    } else if (frame && (frame->frame_flags & MDL_FRAME_FLAGS_REPEAT)) {
        mdl_longjmp_to(frame, LONGJMP_AGAIN);
    }
    return retval;
}

bool mdl_type_is_applicable(int type)
{
    switch (type)
    {
    case MDL_TYPE_CLOSURE:
    case MDL_TYPE_FIX:
    case MDL_TYPE_FSUBR:
    case MDL_TYPE_FUNCTION:
    case MDL_TYPE_MACRO:
    case MDL_TYPE_OFFSET:
    case MDL_TYPE_SUBR:
    case MDL_TYPE_NOTATYPE: // custom application
        // compiled stuff -- is not and will not be implemented
//    case MDL_TYPE_QUICK_ENTRY:
//    case MDL_TYPE_QUICK_RSUBR:
//    case MDL_TYPE_RSUBR:
//    case MDL_TYPE_RSUBR_ENTRY:
        return true;
    default:
        return false;
    }
}

mdl_value_t *mdl_apply_builtin_pt3() {
    int jumpval;
    IARGSETUP();
    IGETNEXTARG(applier);
    mdl_value_t *result;
    mdl_built_in_t built_in = built_in_table[applier->v.w];

    cur_frame->interp_frame2 = mdl_interp_stack;
    jumpval = mdl_setjmp(cur_frame->interp_frame2);
    if (jumpval == 0 || jumpval == LONGJMP_RETRY)
    {
        result = built_in.proc();
    }
    else if (jumpval == LONGJMP_ERRET)
    {
        result = cur_frame->result;
    }
    else
    {
        fprintf(stderr, "Bad longjmp in F/SUBR apply: %d", jumpval);
        //Huh?  pass it on
        mdl_longjmp_to(cur_frame->prev_frame, jumpval);
    }
    return result;
}

mdl_value_t *mdl_apply_builtin_pt2() {
    IARGSETUP();
    IGETNEXTARG(applier);
    IGETRETVAL(arglist);
    mdl_frame_t *frame;
    mdl_built_in_t built_in = built_in_table[applier->v.w];

    frame = mdl_new_frame();
    frame->subr = built_in.a;
    frame->args = arglist;
    frame->prev_frame = cur_frame;
    frame->frame_flags = MDL_FRAME_FLAGS_TRUEFRAME;
    mdl_push_interp_pop_frame(RETURN_COPYDOWN, mdl_make_frame_value(cur_frame));
    mdl_push_frame(frame);
    mdl_push_interp_apply_builtin_pt3(RETURN_COPYFORWARD, applier);
    return NULL;
}

mdl_value_t *mdl_apply_fix_pt2()
{
    IARGSETUP();
    IGETNEXTARG(index);
    IGETRETVAL(apply_to);
    mdl_value_t *struc = LREST(apply_to, 0);
    mdl_value_t *newitem = struc->v.p.cdr == NULL ? NULL : struc->v.p.cdr->v.p.car;
    struc = struc->v.p.car;
    mdl_value_t *result;
    if (newitem)
        result = mdl_internal_eval_put(struc, index, newitem);
    else
        result = mdl_internal_eval_nth_copy(struc, index);
    mdl_pop_frame(cur_frame->prev_frame);
    return result;
}

// this is not the SUBR APPLY.  Rather, it is the internal function used to
// apply things.  The apply_to is the FORM to be applied, including the
// first element.  
// If called from the apply subr, the apply_to is
// pre-evaluated.  For simplicity it contains a list where the first item
// is the eval of the thing applied.
mdl_value_t *mdl_std_apply(mdl_value_t *applier, mdl_value_t *apply_to, int apply_as, bool called_from_apply_subr, bool is_expand)
{
#if 0
    mdl_print_value(stderr, applier);
    fprintf(stderr, "\n");
    mdl_print_value(stderr, apply_to);
    fprintf(stderr, "\n");
#endif
    if (apply_as == MDL_TYPE_NOTATYPE) apply_as = applier->type;
    if (apply_as == MDL_TYPE_SUBR || 
        apply_as == MDL_TYPE_FSUBR)
    {
        if (applier->v.w < (MDL_INT)built_in_table.size())
        {
            if (apply_as == MDL_TYPE_FSUBR && called_from_apply_subr)
                mdl_error("Can't use APPLY with FSUBRs");

            mdl_push_interp_apply_builtin_pt2(RETURN_COPYDOWN, applier);
            if (apply_as == MDL_TYPE_FSUBR || called_from_apply_subr) {
                ISETRETVAL(mdl_make_list(apply_to->v.p.cdr->v.p.cdr));
            }
            else {
                mdl_push_interp_std_eval(RETURN_COPYFORWARD, mdl_make_list(apply_to->v.p.cdr->v.p.cdr));
            }
            return NULL;
        }
        else
            mdl_error("Invalid built-in");
    }
    else if (apply_as == MDL_TYPE_FUNCTION)
    {
        return mdl_apply_function(applier, apply_to, called_from_apply_subr);
    }
    else if (apply_as == MDL_TYPE_MACRO)
    {
        if (called_from_apply_subr)
            mdl_error("Can't use APPLY with MACROs"); // I don't think
        if (is_expand)
        {
            return mdl_apply_function(applier, apply_to, called_from_apply_subr);
        }
        else
        {
            mdl_push_interp_eval_from_istack(RETURN_COPYDOWN);
            mdl_push_interp_eval(RETURN_COPYFORWARD, apply_to, EVAL_EXPAND, mdl_make_frame_value(toplevel_frame, MDL_TYPE_ENVIRONMENT));
        }
        return NULL;
    }
    else if (apply_as == MDL_TYPE_FIX)
    {
        mdl_frame_t *frame;
        frame = mdl_new_frame();
        frame->prev_frame = cur_frame;
        frame->subr = applier;
        // args is not quite right; it should be evalled
        frame->args = mdl_make_list(LREST(apply_to, 1));
        frame->frame_flags = MDL_FRAME_FLAGS_TRUEFRAME;
        mdl_push_frame(frame);

        // handle this case in APPLY, because MAPF/MAPR need to be able
        // to do this
//        if (called_from_apply_subr)
//            mdl_error("Can't use APPLY with FIXes");

        if (LHASITEM(apply_to, 3))
            return mdl_call_error_ext("TOO-MANY-ARGUMENTS-SUPPLIED", "Too many arguments to FIX", NULL);
        if (!LHASITEM(apply_to, 1))
            return mdl_call_error_ext("TOO-FEW-ARGUMENTS-SUPPLIED","Too few arguments to FIX", NULL);
        mdl_value_t *index = applier;
        mdl_push_interp_apply_fix_pt2(RETURN_COPYDOWN, index);
        if (called_from_apply_subr)
            ISETRETVAL(frame->args);
        else
            mdl_push_interp_std_eval(RETURN_COPYFORWARD, frame->args);
        return NULL;
    }
    else
        return mdl_call_error("NON-APPLICABLE-TYPE", applier, apply_to, NULL);
    return NULL;
}

mdl_value_t *mdl_internal_apply(mdl_value_t *applier, mdl_value_t *apply_to, bool called_from_apply_subr, bool is_expand)
{
    int apply_as = MDL_TYPE_NOTATYPE;
    mdl_value_t *applytype = mdl_get_applytype(applier->type);

    if (applytype && applytype->type != MDL_TYPE_ATOM)
    {
        apply_to = mdl_cons_internal(applier, LREST(apply_to, 1));
        apply_to = mdl_cons_internal(applytype, apply_to);
        apply_to = mdl_make_list(apply_to);
        return mdl_std_apply(applytype, apply_to, apply_as, called_from_apply_subr, is_expand);
    }
    else
    {
        if (applytype)
        {
            apply_as = mdl_get_typenum(applytype);
            if (apply_as == MDL_TYPE_NOTATYPE)
                mdl_error("BAD APPLYTYPE: atom not a type");
        }
        return mdl_std_apply(applier, apply_to, apply_as, called_from_apply_subr, is_expand);
    }
}

int mdl_eval_type(int t)
{
    // the type this type evaluates as, or MDL_TYPE_NOTATYPE
    // for custom evaluation
    mdl_value_t *evaltype = mdl_get_evaltype(t);
    if (evaltype)
    {
        if (evaltype->type == MDL_TYPE_ATOM)
            return mdl_get_typenum(evaltype);
        return MDL_TYPE_NOTATYPE;
    }
    return t;
}

int mdl_apply_type(int t)
{
    // the type this type ultmately should be applied as, or MDL_TYPE_NOTATYPE
    // for custom application
    mdl_value_t *applytype = mdl_get_applytype(t);
    if (applytype)
    {
        if (applytype->type == MDL_TYPE_ATOM)
            return mdl_get_typenum(applytype);
        return MDL_TYPE_NOTATYPE;
    }
    return t;
    return t;
}

typedef mdl_value_t *mdl_walker_next_t(struct mdl_struct_walker_t *w);
typedef mdl_value_t *mdl_walker_rest_t(struct mdl_struct_walker_t *w);

typedef struct mdl_struct_walker_t
{
    mdl_walker_next_t *next;
    mdl_walker_rest_t *rest;
    mdl_value_t *sv; // original structure
    mdl_value_t *vle; // vector/tuple/list element
    uvector_element_t *uve; // uvector element
    char *se;   // string element
    int length; // remaining length vector/uvector/string
} mdl_struct_walker_t;

mdl_value_t *mdl_next_list_element(mdl_struct_walker_t *w)
{
    if (!w->vle) return NULL;
    w->vle = w->vle->v.p.cdr;
    if (!w->vle) return NULL;
    mdl_value_t *result = w->vle->v.p.car;
    return result;
}

mdl_value_t *mdl_next_vector_tuple_element(mdl_struct_walker_t *w)
{
    // note: must not execute GROW for a vector while a walker is active on it
    w->vle++;
    w->length--;
    if (w->length <= 0) return NULL;
    mdl_value_t *result = w->vle;
    return result;
}

mdl_value_t *mdl_next_uvector_element(mdl_struct_walker_t *w)
{
    // note: must not execute GROW for a uvector while a walker is active on it
    w->uve++;
    w->length--;
    if (w->length <= 0) return NULL;
    mdl_value_t *result = mdl_uvector_element_to_value(w->sv, w->uve);
    return result;
}

mdl_value_t *mdl_next_string_element(mdl_struct_walker_t *w)
{
    w->se++;
    w->length--;
    if (w->length <= 0) return NULL;
    mdl_value_t *result = mdl_new_fix((int)(*w->se));
    result->type = MDL_TYPE_CHARACTER;
    return result;
}

mdl_value_t *mdl_rest_string_element(mdl_struct_walker_t *w)
{
    return mdl_make_string(w->length, w->se);
}

mdl_value_t *mdl_rest_list_element(mdl_struct_walker_t *w)
{
    return mdl_make_list(w->vle, MDL_TYPE_LIST);
}

mdl_value_t *mdl_rest_vector_element(mdl_struct_walker_t *w)
{
    mdl_value_t *result = mdl_new_mdl_value();
    result->pt = PRIMTYPE_VECTOR;
    result->type = MDL_TYPE_VECTOR;
    result->v.v.p = w->sv->v.v.p;
    result->v.v.offset = w->sv->v.v.offset + (w->vle - VREST(w->sv, 0));
    return result;
}

mdl_value_t *mdl_rest_tuple_element(mdl_struct_walker_t *w)
{
    mdl_value_t *result = mdl_new_mdl_value();
    result->pt = PRIMTYPE_TUPLE;
    result->type = MDL_TYPE_TUPLE;
    result->v.tp.p = w->sv->v.tp.p;
    result->v.tp.offset = w->sv->v.tp.offset + (w->vle - TPREST(w->sv, 0));
    return result;
}

mdl_value_t *mdl_rest_uvector_element(mdl_struct_walker_t *w)
{
    mdl_value_t *result = mdl_new_mdl_value();
    result->pt = PRIMTYPE_UVECTOR;
    result->type = MDL_TYPE_UVECTOR;
    result->v.uv.p = w->sv->v.uv.p;
    result->v.uv.offset = w->sv->v.tp.offset + (w->uve - UVREST(w->sv, 0));
    return result;
}

void mdl_init_struct_walker(mdl_struct_walker_t *w, mdl_value_t *sv)
{
    w->sv = sv;
    switch(sv->pt)
    {
    case PRIMTYPE_LIST:
        w->vle = sv;
        w->next = mdl_next_list_element;
        w->rest = mdl_rest_list_element;
        break;
    case PRIMTYPE_VECTOR:
        w->vle = VREST(sv, 0) - 1;
        w->length = VLENGTH(sv) + 1;
        w->next = mdl_next_vector_tuple_element;
        w->rest = mdl_rest_vector_element;
        break;
    case PRIMTYPE_TUPLE:
        w->vle = TPREST(sv, 0) - 1;
        w->length = TPLENGTH(sv) + 1;
        w->next = mdl_next_vector_tuple_element;
        w->rest = mdl_rest_tuple_element;
        break;

    case PRIMTYPE_UVECTOR:
        w->uve = UVREST(sv, 0) - 1;
        w->length = UVLENGTH(sv) + 1;
        w->next = mdl_next_uvector_element;
        w->rest = mdl_rest_uvector_element;
        break;

    case PRIMTYPE_STRING:
        w->se = sv->v.s.p - 1;
        w->length = sv->v.s.l + 1;
        w->next = mdl_next_string_element;
        w->rest = mdl_rest_string_element;
        break;
    }
}

mdl_value_t *mdl_internal_shallow_copy_list(mdl_value_t *oldlist)
{
    mdl_value_t *result = NULL;
    mdl_value_t *cursor = NULL;
    mdl_value_t *lastitem = NULL;
    if (oldlist)
    {
        lastitem = cursor = result = mdl_newlist();
        cursor->v.p.car = oldlist->v.p.car;
        oldlist = oldlist->v.p.cdr;
    }
    while (oldlist)
    {
        cursor = mdl_newlist();
        cursor->v.p.car = oldlist->v.p.car;
        lastitem->v.p.cdr = cursor;
        lastitem = cursor;
        oldlist = oldlist->v.p.cdr;
    }
    return result;
}

mdl_value_t *mdl_internal_copy_structured(mdl_value_t *v)
{
    mdl_value_t *copy;
    switch(v->pt)
    {
    case PRIMTYPE_LIST:
        copy = mdl_make_list(mdl_internal_shallow_copy_list(LREST(v,0)));
        break;
    case PRIMTYPE_VECTOR:
        copy = mdl_new_empty_vector(VLENGTH(v), MDL_TYPE_VECTOR);
        memcpy(VREST(copy, 0), VREST(v,0), VLENGTH(v) * sizeof(mdl_value_t));
        break;
    case PRIMTYPE_TUPLE:
        copy = mdl_new_empty_vector(TPLENGTH(v), MDL_TYPE_VECTOR);
        memcpy(VREST(copy, 0), TPREST(v,0), TPLENGTH(v) * sizeof(mdl_value_t));
        break;
    case PRIMTYPE_UVECTOR:
        copy = mdl_new_empty_uvector(UVLENGTH(v), MDL_TYPE_UVECTOR);
        memcpy(UVREST(copy, 0), UVREST(v,0), UVLENGTH(v) * sizeof(uvector_element_t));
        break;
    case PRIMTYPE_STRING:
        copy = mdl_new_string(v->v.s.l, v->v.s.p);
        break;
    default:
        return NULL;
    }
    return copy;
}

int mdl_internal_list_length(mdl_value_t *l)
{
    /* returns the length of an "internal" list lacking its initial word */
    int length = 0;
    mdl_value_t *cursor = l;

    while (cursor)
    {
        cursor = cursor->v.p.cdr;
        length++;
    }
    return length;
}

int mdl_internal_struct_length(mdl_value_t *sv)
{
    int length = -1;
    switch(sv->pt)
    {
    case PRIMTYPE_LIST:
    {
        mdl_value_t *cursor = sv->v.p.cdr;
        length = 0;
        while (cursor)
        {
            cursor = cursor->v.p.cdr;
            length++;
        }
        break;
    }
    case PRIMTYPE_VECTOR:
        length = VLENGTH(sv);
        break;
    case PRIMTYPE_TUPLE:
        length = TPLENGTH(sv);
        break;
    case PRIMTYPE_UVECTOR:
        length = UVLENGTH(sv);
        break;
    case PRIMTYPE_STRING:
        length = sv->v.s.l;
        break;
    }
    return length;
}

void mdl_resize_vector(mdl_value_t *v, int addend, int addbeg, bool makelose)
{
    int newsize;
    int sizechange = addbeg + addend;
    switch (v->pt)
    {
    case PRIMTYPE_VECTOR:
    {
        int saveelements = v->v.v.p->size;
        if ((addend == 0) && (addbeg == 0))
            return;

        newsize = v->v.v.p->size + sizechange;
        mdl_value_t *elems;
        if (addbeg < 0) saveelements += addbeg;
        if (addend < 0) saveelements += addend;
        elems = v->v.v.p->elements;
        
        if (sizechange > 0)
            elems = (mdl_value_t *)GC_MALLOC_IGNORE_OFF_PAGE(newsize * sizeof(mdl_value_t));
        if ((addbeg > 0) || (addbeg == 0 && sizechange > 0))
            memmove(elems + addbeg, v->v.v.p->elements, saveelements * sizeof(mdl_value_t));
        else if (addbeg < 0)
            memmove(elems, v->v.v.p->elements - addbeg, saveelements * sizeof(mdl_value_t));
        if (sizechange < 0)
            elems = (mdl_value_t *)GC_REALLOC(elems, newsize * sizeof(mdl_value_t));
        
        if (makelose) 
        {
            int loseme;
            mdl_value_t *loser;
            loser = elems;
            for (loseme = 0; loseme < addbeg; loseme++)
            {
                loser->pt = PRIMTYPE_WORD;
                loser->type = MDL_TYPE_LOSE;
                loser++;
            }
            loseme += saveelements;
            for (loseme = 0; loseme < addend; loseme++)
            {
                loser->pt = PRIMTYPE_WORD;
                loser->type = MDL_TYPE_LOSE;
                loser++;
            }
        }
        v->v.v.p->size = newsize;
        v->v.v.p->startoffset += addbeg;
        v->v.v.p->elements = elems;
        break;
    }
    case PRIMTYPE_UVECTOR:
    {
        int saveelements = v->v.uv.p->size;
        newsize = v->v.uv.p->size + sizechange;
        uvector_element_t *elems;
        if (addbeg < 0) saveelements += addbeg;
        if (addend < 0) saveelements += addend;
        elems = v->v.uv.p->elements;
        
        if (sizechange > 0)
            elems = (uvector_element_t *)GC_MALLOC_IGNORE_OFF_PAGE(newsize * sizeof(uvector_element_t));
        if ((addbeg > 0) || (addbeg == 0 && sizechange > 0))
            memmove(elems + addbeg, v->v.uv.p->elements, saveelements * sizeof(uvector_element_t));
        else if (addbeg < 0)
            memmove(elems, v->v.uv.p->elements - addbeg, saveelements * sizeof(uvector_element_t));
        if (sizechange < 0)
            elems = (uvector_element_t *)GC_REALLOC(elems, newsize * sizeof(uvector_element_t));
        if (makelose) 
        {
            if (addbeg > 0)
                memset(elems, 0, addbeg * sizeof(uvector_element_t));
            if (addend > 0)
                memset(elems + newsize - addend, 0, addend * sizeof(uvector_element_t));
        }
        v->v.uv.p->size = newsize;
        v->v.uv.p->startoffset += addbeg;
        v->v.uv.p->elements = elems;
    }
    break;
    default:
        mdl_error("Resize of unsupported type");
    }
}

//mdl_eval_apply_expr does the special evaluation of the first
//item of a form -- if an atom, returns gval, then lval, otherwise
//standard application
mdl_value_t *mdl_eval_apply_expr()
{
    IARGSETUP();
    IGETNEXTARG(appl_expr);
    mdl_value_t *applier = NULL;
    if (appl_expr->type == MDL_TYPE_ATOM)
    {
        applier = mdl_global_symbol_lookup(appl_expr->v.a);
        if (!applier || applier->type == MDL_TYPE_UNBOUND)
            applier = mdl_local_symbol_lookup(appl_expr->v.a);
        if (!applier || applier->type == MDL_TYPE_UNBOUND)
        {
            // This makes an erret return the value of the apply rather than the applier
            mdl_interp_stack->prev_frame = mdl_interp_stack->prev_frame->prev_frame;
            mdl_interp_stack->return_to = RETURN_COPYNEXT;
            return mdl_call_error_ext("UNBOUND-VARIABLE", "In apply of form", appl_expr, NULL);
        }
    }
    else
    {
        mdl_push_interp_eval(RETURN_COPYDOWN, appl_expr, 0);
    }
    return applier;
}

mdl_value_t *mdl_std_eval(mdl_value_t *l, bool in_struct, int as_type) {
    mdl_push_interp_std_eval(RETURN_RETURN, l, in_struct?EVAL_IN_STRUCT:0, as_type);
    return mdl_interp_from_stack();
}

mdl_value_t *mdl_eval_list()
{
    IARGSETUP();
    IGETNEXTARG(rest);  // points to the element which was just evalled, or to head of list.
    IGETNEXTARG(result);
    IGETNEXTARG(lastitem);
    IGETNEXTARG(l);  // the original item to be evalled, used for its type
#if 0
    mdl_print_value(stderr, rest);
    fprintf(stderr, "\n");
    mdl_print_value(stderr, result);
    fprintf(stderr, "\n");
#endif
    mdl_value_t *prev_result = mdl_interp_stack->retval;
    if (prev_result) {
        mdl_value_t *prev_item = rest->v.p.car;
        if (mdl_eval_type(prev_item->type) == MDL_TYPE_SEGMENT) {
            if (mdl_primtype_nonstructured(prev_result->pt))
            {
                return mdl_call_error_ext("ILLEGAL-SEGMENT","Segment evaluated to nonstructured type", prev_item, NULL);
            }
            if (!rest->v.p.cdr && prev_result->pt == PRIMTYPE_LIST)
            {
                if (result == NULL)
                {
                    result = prev_result->v.p.cdr;
                }
                else
                {
                    lastitem->v.p.cdr = prev_result->v.p.cdr;
                    // lastitem is now wrong, but it won't be used
                    // again anyway
                }
            }
            else
            {
                mdl_value_t *tmp, *elem;
                mdl_struct_walker_t w;

                mdl_init_struct_walker(&w, prev_result);
                elem = w.next(&w);
                while (elem)
                {
                    tmp = mdl_additem(lastitem, elem, &lastitem);
                    if (result == NULL) result = tmp;
                    elem= w.next(&w);
                }
            }
        }
        else
        {
            mdl_additem(lastitem, prev_result, &lastitem);
        }
        if (result == NULL) result = lastitem;
    }
    rest = rest->v.p.cdr;
    if (!rest) {
        // eval is done
        return mdl_make_list(result, l->type);
    }
    mdl_value_t *item = rest->v.p.car;
    IARGRESET(mdl_interp_stack);
    ISETNEXTARG(rest);
    ISETNEXTARG(result);
    ISETNEXTARG(lastitem);
    mdl_interp_stack->started = false;  // tail recursion
    mdl_push_interp_eval(RETURN_COPYFORWARD, item, EVAL_IN_STRUCT);
    return NULL;
}

mdl_value_t *mdl_eval_vector()
{
    IARGSETUP();
    IGETNEXTARG(result); // output
    IGETNEXTARG(in_offset_val); // where in the l structure we are.
    IGETNEXTARG(out_offset_val); // where in the result structure we are.
    IGETNEXTARG(l);
    mdl_value_t *prev_result = mdl_interp_stack->retval;
    if (prev_result) {
        mdl_value_t* prev_item = VREST(l, in_offset_val->v.w);
        if (mdl_eval_type(prev_item->type) == MDL_TYPE_SEGMENT)
        {
            if (mdl_primtype_nonstructured(prev_result->pt))
            {
                mdl_error("Segment evaluated to nonstructured type");
            }
            int seglength = mdl_internal_struct_length(prev_result);
            mdl_value_t *elem;
            mdl_struct_walker_t w;
            mdl_resize_vector(result, seglength - 1, 0, false);
            
            mdl_value_t *relems = VREST(result, out_offset_val->v.w);
            mdl_init_struct_walker(&w, prev_result);
            elem = w.next(&w);
            while (elem)
            {
                *relems++ = *elem;
                elem = w.next(&w);
            }
            // Note directly modifying FIXes here, may screw up optimizations.
            out_offset_val->v.w += seglength;
        }
        else 
        {
            *VREST(result, out_offset_val->v.w) = *prev_result;
            // Note directly modifying FIXes here, may screw up optimizations.
            out_offset_val->v.w++;
        }
        in_offset_val->v.w++;
    }
    else
    {
        result = mdl_new_empty_vector(VLENGTH(l), l->type);
        IARGRESET(mdl_interp_stack);
        ISETNEXTARG(result);
    }
    if (in_offset_val->v.w >= VLENGTH(l)) return result;
    mdl_value_t *item = VREST(l, in_offset_val->v.w);
    mdl_interp_stack->started = false; // tail recursion
    mdl_push_interp_eval(RETURN_COPYFORWARD, item, EVAL_IN_STRUCT);
    return NULL;
}

mdl_value_t *mdl_eval_uvector()
{
    IARGSETUP();
    IGETNEXTARG(result); // output
    IGETNEXTARG(in_offset_val); // where in the l structure we are.
    IGETNEXTARG(out_offset_val); // where in the result structure we are.
    IGETNEXTARG(l);
    mdl_value_t *prev_result = mdl_interp_stack->retval;
    if (prev_result) {
        // a UVECTOR of SEGMENTs is an odd thing indeed.
        if (mdl_eval_type(UVTYPE(l)) == MDL_TYPE_SEGMENT)
        {
            if (mdl_primtype_nonstructured(prev_result->pt))
            {
                mdl_error("Segment evaluated to nonstructured type");
            }
            int seglength = mdl_internal_struct_length(prev_result);
            mdl_value_t *elem;
            mdl_struct_walker_t w;
            mdl_resize_vector(result, seglength - 1, 0, false);
            
            uvector_element_t *relems = UVREST(result, out_offset_val->v.w);
            mdl_init_struct_walker(&w, prev_result);
            elem = w.next(&w);
            while (elem)
            {
                if (UVTYPE(result) == MDL_TYPE_LOSE)
                {
                    if (!mdl_valid_uvector_primtype(elem->pt))
                    {
                        mdl_error("Invalid type for UVECTOR");
                    }
                    UVTYPE(result) = elem->type;
                }
                else if (UVTYPE(result) != elem->type)
                {
                    return mdl_call_error("TYPES-DIFFER-IN-UNIFORM-VECTOR", NULL);
                }
                mdl_uvector_value_to_element(elem, relems++);
                elem = w.next(&w);
            }
            // Note directly modifying FIXes here, may screw up optimizations.
            out_offset_val->v.w += seglength;
        }
        else 
        {
            if (UVTYPE(result) == MDL_TYPE_LOSE)
            {
                if (!mdl_valid_uvector_primtype(prev_result->pt))
                {
                    mdl_error("Invalid type for UVECTOR");
                }
                UVTYPE(result) = prev_result->type;
            }
            else if (UVTYPE(result) != prev_result->type)
            {
                return mdl_call_error("TYPES-DIFFER-IN-UNIFORM-VECTOR", NULL);
            }
            mdl_uvector_value_to_element(prev_result, UVREST(result, out_offset_val->v.w));
            // Note directly modifying FIXes here, may screw up optimizations.
            out_offset_val->v.w++;
        }
        in_offset_val->v.w++;
    }
    else
    {
        result = mdl_new_empty_uvector(UVLENGTH(l), l->type);
        IARGRESET(mdl_interp_stack);
        ISETNEXTARG(result);
    }
    if (in_offset_val->v.w >= UVLENGTH(l)) return result;
    uvector_element_t *uv_item = UVREST(l, in_offset_val->v.w);
    mdl_value_t *item = mdl_uvector_element_to_value(l, uv_item);
    mdl_interp_stack->started = false; // tail recursion
    mdl_push_interp_eval(RETURN_COPYFORWARD, item, EVAL_IN_STRUCT);
    return NULL;
}

mdl_value_t *mdl_old_std_eval(mdl_value_t *l, int eval_flags, int as_type)
{
    bool in_struct = (eval_flags & EVAL_IN_STRUCT) != 0;
    mdl_value_t *result = NULL;
    if (as_type == MDL_TYPE_NOTATYPE) as_type = l->type;
    switch (as_type)
    {
    case MDL_TYPE_LIST:
    {
        mdl_push_interp_eval_list(RETURN_COPYDOWN, l);
        return NULL;
    }
    case MDL_TYPE_SEGMENT:
        if (!in_struct)
            return mdl_call_error_ext("ILLEGAL-SEGMENT", "Attempt to evaluate segment outside structured type", NULL);
        /* FALLTHROUGH */
    case MDL_TYPE_FORM:
        if (l->v.p.cdr)
        {
            mdl_value_t *appl_expr = (l->v.p.cdr->v.p.car);
            mdl_push_interp_apply_from_istack(RETURN_COPYDOWN, l, (eval_flags & EVAL_EXPAND) != 0);
            mdl_push_interp_eval_apply_expr(RETURN_COPYFORWARD, appl_expr);
            return NULL;
        }
        else
        {
            // an empty form is a FALSE
            result =  mdl_make_list(NULL, MDL_TYPE_FALSE);
        }
        break;
    case MDL_TYPE_VECTOR:
    {
        mdl_push_interp_eval_vector(RETURN_COPYDOWN, l);
        return NULL;
    }
    case MDL_TYPE_UVECTOR:
    {
        mdl_push_interp_eval_uvector(RETURN_COPYDOWN, l);
        return NULL;
    }
    default:
        // atoms, strings, fixes, falses and undefined types
        result = l;
        break;
    }
    return result;
}

void mdl_push_interp_toplevel(mdl_interp_return_t return_to)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
//    new_frame->method = INTERP_EVAL;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_eval(mdl_interp_return_t return_to, mdl_value_t *l, int eval_flags, mdl_value_t *environment)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(environment, args);
    args = mdl_cons_internal(mdl_new_fix(eval_flags), args);
    args = mdl_cons_internal(l, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_EVAL;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_eval_list(mdl_interp_return_t return_to, mdl_value_t *l)
{
    // * args: list to be evalled, placeholder for result, placeholder for lastitem,
    //         list to be evalled (used for final type)
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(l, args);
    args = mdl_cons_internal(NULL, args); // lastitem
    args = mdl_cons_internal(NULL, args); // result
    args = mdl_cons_internal(l, args);  // rest
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_EVAL_LIST;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_eval_vector(mdl_interp_return_t return_to, mdl_value_t *l)
{
    // * args: list to be evalled, placeholder for result, placeholder for lastitem,
    //         list to be evalled (used for final type)
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(l, args);
    args = mdl_cons_internal(mdl_new_fix(0), args); // result offset
    args = mdl_cons_internal(mdl_new_fix(0), args); // input offset
    args = mdl_cons_internal(l, args);  // result
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_EVAL_VECTOR;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_eval_uvector(mdl_interp_return_t return_to, mdl_value_t *l)
{
    // * args: list to be evalled, placeholder for result, placeholder for lastitem,
    //         list to be evalled (used for final type)
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(l, args);
    args = mdl_cons_internal(mdl_new_fix(0), args); // result offset
    args = mdl_cons_internal(mdl_new_fix(0), args); // input offset
    args = mdl_cons_internal(l, args);  // result
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_EVAL_UVECTOR;
    new_frame->prev_frame = mdl_interp_stack;
    mdl_interp_stack = new_frame;
}

void mdl_push_interp_eval_apply_expr(mdl_interp_return_t return_to, mdl_value_t *apply_expr)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(apply_expr, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_EVAL_APPLY_EXPR;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_apply_from_istack(mdl_interp_return_t return_to, mdl_value_t *l, bool is_expand)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(mdl_boolean_value(is_expand), args);
    args = mdl_cons_internal(l, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_APPLY_FROM_ISTACK;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_apply(mdl_interp_return_t return_to, mdl_value_t *applier, mdl_value_t *l, bool called_from_apply_subr, bool is_expand)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(mdl_boolean_value(is_expand), args);
    args = mdl_cons_internal(mdl_boolean_value(called_from_apply_subr), args);
    args = mdl_cons_internal(l, args);
    args = mdl_cons_internal(applier, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_APPLY;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_std_eval(mdl_interp_return_t return_to, mdl_value_t *l, int eval_flags, int eval_as_type)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(mdl_new_fix(eval_as_type), args);
    args = mdl_cons_internal(mdl_new_fix(eval_flags), args);
    args = mdl_cons_internal(l, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_STD_EVAL;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_apply_builtin_pt2(mdl_interp_return_t return_to, mdl_value_t *applier)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(applier, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_APPLY_BUILTIN_PT2;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_apply_builtin_pt3(mdl_interp_return_t return_to, mdl_value_t *applier)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(applier, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_APPLY_BUILTIN_PT3;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_apply_fix_pt2(mdl_interp_return_t return_to, mdl_value_t *index)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(index, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_APPLY_FIX_PT2;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_set_environment(mdl_interp_return_t return_to, mdl_value_t *env)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(env, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_SET_ENVIRONMENT;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_pop_frame(mdl_interp_return_t return_to, mdl_value_t *frame)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(frame, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_POP_FRAME;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_listen_error_pt2(mdl_interp_return_t return_to, bool is_error)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(mdl_boolean_value(is_error), args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_LISTEN_ERROR_PT2;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_bind_arg_state_machine(mdl_interp_return_t return_to, mdl_value_t *fargp, mdl_value_t *argptr,
                                            mdl_value_t *argstate, mdl_value_t *argcount,
                                            mdl_value_t *apply_to, mdl_value_t *frame,
                                            mdl_value_t *called_from_apply)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(called_from_apply, args);
    args = mdl_cons_internal(frame, args);
    args = mdl_cons_internal(apply_to, args);
    args = mdl_cons_internal(argcount, args);
    args = mdl_cons_internal(argstate, args);
    args = mdl_cons_internal(argptr, args);
    args = mdl_cons_internal(fargp, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_BIND_ARG_STATE_MACHINE;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_eval_body(mdl_interp_return_t return_to, mdl_value_t *applier, mdl_value_t *activation)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(activation, args);
    args = mdl_cons_internal(applier, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_EVAL_BODY;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_eval_body_state_machine(mdl_interp_return_t return_to, mdl_value_t *fargp, mdl_value_t *activation)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(activation, args);
    args = mdl_cons_internal(fargp, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_EVAL_BODY_STATE_MACHINE;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_rep_state_machine(mdl_interp_return_t return_to, mdl_value_t *state)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(state, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_REP_STATE_MACHINE;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_cond_pt2(mdl_interp_return_t return_to, mdl_value_t *cur_clause_arg)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(cur_clause_arg, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_COND_PT2;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_ilist_pt2(mdl_interp_return_t return_to, mdl_value_t *list_head)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(list_head, args);
    args = mdl_cons_internal(NULL, args);  // placeholder for # elems remaining
    args = mdl_cons_internal(NULL, args);  // placeholder for lastitem
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_ILIST_PT2;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_i_u_vector_pt2(mdl_interp_return_t return_to, mdl_value_t *empty_u_vector)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(empty_u_vector, args);
    args = mdl_cons_internal(NULL, args);  // placeholder for index
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_I_U_VECTOR_PT2;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_istring_pt2(mdl_interp_return_t return_to, mdl_value_t *result)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(result, args);
    args = mdl_cons_internal(NULL, args);  // placeholder for index
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_ISTRING_PT2;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_and_pt2(mdl_interp_return_t return_to, mdl_value_t *rest)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(rest, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_AND_PT2;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_or_pt2(mdl_interp_return_t return_to, mdl_value_t *rest)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(rest, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_OR_PT2;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_eval_from_istack(mdl_interp_return_t return_to)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    new_frame->args = NULL;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_EVAL_FROM_ISTACK;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_mapfr_loop(mdl_interp_return_t return_to, bool is_mapr, mdl_value_t *loopf, mdl_value_t *flist, mdl_value_t *flastitem, mdl_value_t *stup)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(stup, args);
    args = mdl_cons_internal(flastitem, args);
    args = mdl_cons_internal(flist, args);
    args = mdl_cons_internal(loopf, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = is_mapr?INTERP_MAPR_LOOP:INTERP_MAPF_LOOP;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_mapfr_final(mdl_interp_return_t return_to, mdl_value_t *result_ilist)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    new_frame->args = mdl_cons_internal(result_ilist, NULL);
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_MAPFR_FINAL;
    mdl_push_interp_frame(new_frame);
}

void mdl_push_interp_def_pt2(mdl_interp_return_t return_to, mdl_value_t *def_list)
{
    mdl_interp_frame_t *new_frame = mdl_new_interp_frame();
    mdl_value_t *args = NULL;
    args = mdl_cons_internal(def_list, args);
    new_frame->args = args;
    new_frame->return_to = return_to;
    new_frame->started = false;
    new_frame->retval = NULL;
    new_frame->method = INTERP_DEF_PT2;
    mdl_push_interp_frame(new_frame);
}

mdl_value_t *mdl_eval(mdl_value_t *l, bool in_struct, mdl_value_t *environment)
{
//    fprintf(stderr, "Eval\n");
    mdl_push_interp_eval(RETURN_RETURN, l, in_struct?EVAL_IN_STRUCT:0, environment);
    return mdl_interp_from_stack();
}

mdl_value_t *mdl_old_eval(mdl_value_t *l, int eval_flags, mdl_value_t *environment);

mdl_value_t *mdl_interp_from_stack() 
{
//    fprintf(stderr, "Eval from stack\n");
    while (mdl_interp_stack)
    {
        mdl_interp_frame_t* frame = mdl_interp_stack;
        if (frame->started)
        {
            mdl_interp_stack = frame->prev_frame;
//            mdl_print_value(stderr, frame->retval);
//            fprintf(stderr, " Return_to: %d (method %d)\n", frame->return_to, frame->method);
            switch (frame->return_to) {
            case RETURN_RETURN:
                return frame->retval;
            case RETURN_COPYDOWN:
            {
                mdl_interp_frame_t* cursor = mdl_interp_stack;
                while (!cursor->started) {
                    cursor = cursor->prev_frame;
                    assert(cursor != NULL);
                }
                cursor->retval = frame->retval;
                break;
            }
            case RETURN_COPYFORWARD:
                assert(!mdl_interp_stack->started);
            case RETURN_COPYNEXT:
                mdl_interp_stack->retval = frame->retval;
                break;
            case RETURN_DISCARD:
                break;
            }
        }
        else
        {
            frame->started = true;
//            fprintf(stderr, "Method: %d Ret: %d\n", frame->method, frame->return_to);
            switch (frame->method) {
            case INTERP_EVAL:
            {
                IARGSETUP();
                IGETNEXTARG(l);
                IGETNEXTARG(eval_flags);
                IGETNEXTARG(environment);
                frame->retval = mdl_old_eval(l, eval_flags->v.w, environment);
                break;
            }
            case INTERP_EVAL_FROM_ISTACK:
            {
                frame->retval = mdl_old_eval(frame->retval, 0, NULL);
                break;
            }
            case INTERP_STD_EVAL:
            {
                IARGSETUP();
                IGETNEXTARG(l);
                IGETNEXTARG(eval_flags);
                IGETNEXTARG(type);
                frame->retval = mdl_old_std_eval(l, eval_flags->v.w, type->v.w);
                break;
            }
            case INTERP_EVAL_LIST:
                frame->retval = mdl_eval_list();
                break;
            case INTERP_EVAL_VECTOR:
                frame->retval = mdl_eval_vector();
                break;
            case INTERP_EVAL_UVECTOR:
                frame->retval = mdl_eval_uvector();
                break;
            case INTERP_EVAL_APPLY_EXPR:
                frame->retval = mdl_eval_apply_expr();
                break;
            case INTERP_APPLY:
            {
                IARGSETUP();
                IGETNEXTARG(applier);
                IGETNEXTARG(l);
                IGETNEXTARG(called_from_apply_subr);
                IGETNEXTARG(is_expand);
                frame->retval = mdl_internal_apply(applier, l, mdl_is_true(called_from_apply_subr), mdl_is_true(is_expand));
                break;
            }
            case INTERP_APPLY_FROM_ISTACK:
            {
                IARGSETUP();
                IGETNEXTARG(l);
                IGETNEXTARG(is_expand);
                frame->retval = mdl_internal_apply(frame->retval, l, false, mdl_is_true(is_expand));
                break;
            }
            case INTERP_APPLY_BUILTIN_PT2:
                frame->retval = mdl_apply_builtin_pt2();
                break;
            case INTERP_APPLY_BUILTIN_PT3:
            {
                // mdl_interp_stack can change after builtin_pt3 due to restore.
                mdl_frame_t *old_initial_frame = initial_frame;
                mdl_value_t *retval = mdl_apply_builtin_pt3();
                if (initial_frame != old_initial_frame) {
                    // A restore occurred!  Fix up as best we can
                    // (depends on restore not pushing anything)
                    // better fix: have the routines themselves save their retvals instead
                    // of doing it here.
                    frame = mdl_interp_stack;
                }
                frame->retval = retval;
                break;
            }
            case INTERP_APPLY_FIX_PT2:
                frame->retval = mdl_apply_fix_pt2();
                break;
            case INTERP_BIND_ARG_STATE_MACHINE:
                frame->retval = mdl_bind_arg_state_machine();
                break;
            case INTERP_SET_ENVIRONMENT:
                frame->retval = mdl_set_environment();
                break;
            case INTERP_POP_FRAME:
            {
                IARGSETUP();
                IGETNEXTARG(pop_to);
                mdl_pop_frame(pop_to->v.f);
                break;
            }
            case INTERP_LISTEN_ERROR_PT2:
                frame->retval = mdl_listen_error_pt2();
                break;
            case INTERP_EVAL_BODY:
                frame->retval = mdl_eval_body();
                break;
            case INTERP_EVAL_BODY_STATE_MACHINE:
                frame->retval = mdl_eval_body_state_machine();
                break;
            case INTERP_REP_STATE_MACHINE:
                frame->retval = mdl_rep_state_machine();
                break;
            case INTERP_COND_PT2:
                frame->retval = mdl_cond_pt2();
                break;
            case INTERP_ILIST_PT2:
                frame->retval = mdl_ilist_pt2();
                break;
            case INTERP_I_U_VECTOR_PT2:
                frame->retval = mdl_i_u_vector_pt2();
                break;
            case INTERP_ISTRING_PT2:
                frame->retval = mdl_istring_pt2();
                break;
            case INTERP_AND_PT2:
                frame->retval = mdl_and_pt2();
                break;
            case INTERP_OR_PT2:
                frame->retval = mdl_or_pt2();
                break;
            case INTERP_MAPF_LOOP:
            case INTERP_MAPR_LOOP:
                frame->retval = mdl_mapfr_loop();
                break;
            case INTERP_MAPFR_FINAL:
                frame->retval = mdl_mapfr_final();
                break;
            case INTERP_DEF_PT2:
                frame->retval = mdl_def_pt2();
                break;
            case INTERP_LONGJMP_TO:
                frame->retval = mdl_do_longjmp_to();
                break;
            default:
                fprintf(stderr, "Bad interpreter call\n");
                exit(-1);
            }
        }
    }
    assert(0);  // Should alwaye be a return frame on the stack
}

// Set the environemnt, then return any passed-through return value.
mdl_value_t *mdl_set_environment() 
{
    IARGSETUP();
    IGETNEXTARG(environment);
    // Set the environment and pass through the current return value;
    cur_frame = environment->v.f;
    NEW_BINDID(cur_process_bindid);
    return mdl_interp_stack->retval;
}

mdl_value_t *mdl_old_eval(mdl_value_t *l, int eval_flags, mdl_value_t *environment)
{
    mdl_value_t *result = NULL;
    int eval_as_type;
    mdl_value_t *evaltype;
    if (environment)
    {
        // Push a "set environment" to the current frame beneath the current interp frame.
        mdl_interp_frame_t *iframe = mdl_interp_stack;
        mdl_interp_stack = iframe->prev_frame;
        mdl_push_interp_set_environment(iframe->return_to, mdl_make_frame_value(cur_frame));
        iframe->prev_frame = mdl_interp_stack;
        mdl_interp_stack = iframe;
        iframe->return_to = RETURN_COPYFORWARD;
        cur_frame = environment->v.f;
        NEW_BINDID(cur_process_bindid);
    }

    eval_as_type = l->type;
    evaltype = mdl_get_evaltype(l->type);
    if (evaltype && evaltype->type != MDL_TYPE_ATOM)
    {
        mdl_value_t *arglist = mdl_cons_internal(l, NULL);
        arglist = mdl_cons_internal(evaltype, arglist);
        arglist = mdl_make_list(arglist);
        result = mdl_internal_apply(evaltype, arglist, true);
    }
    else
    {
        if (evaltype)
        {
            eval_as_type = mdl_get_typenum(evaltype);
            if (eval_as_type == MDL_TYPE_NOTATYPE)
                mdl_error("BAD EVALTYPE: atom not a type");
        }
        mdl_push_interp_std_eval(RETURN_COPYDOWN, l, eval_flags, eval_as_type);
        result = NULL;
    }
    return result;
}

mdl_value_t *mdl_set_gval(atom_t *a, mdl_value_t *val)
{
    mdl_symbol_t *symbol = &global_syms[a];
    symbol->binding = val;
    symbol->atom = a;
    return val;
}

mdl_value_t *mdl_set_lval(atom_t *a, mdl_value_t *val, mdl_frame_t *frame)
{
    mdl_local_symbol_t *symbol = mdl_local_symbol_slot(a, frame);
    if (symbol)
    {
        symbol->binding = val;
        symbol->atom = a;
    }
    else
    {
        // Binding on initial stack, not current stack
        mdl_bind_local_symbol(a, val, cur_process_initial_frame, false);
    }
    return val;
}

void mdl_interp_init()
{
    extern void mdl_create_builtins();

    if (sizeof(MDL_FLOAT) != sizeof(MDL_INT))
    {
        // math and UVECTORS don't work right if this isn't true
        // could fix it by making FLOAT its own primtype, but
        // that might break MDL
        printf("sizeof(MDL_FLOAT) %zd != sizeof(MDL_INT) %zd\n", sizeof(MDL_FLOAT), sizeof(MDL_INT));
        exit(-1);
    }

    srand48(1);
    mdl_assoc_table = mdl_create_assoc_table();

    // must initialize root oblist before the built-in types
    mdl_value_initial_oblist = mdl_new_empty_uvector(MDL_OBLIST_HASHBUCKET_DEFAULT, MDL_TYPE_OBLIST);
    UVTYPE(mdl_value_initial_oblist) = MDL_TYPE_LIST;
    mdl_value_root_oblist = mdl_new_empty_uvector(MDL_ROOT_OBLIST_HASHBUCKET_DEFAULT, MDL_TYPE_OBLIST);
    UVTYPE(mdl_value_root_oblist) = MDL_TYPE_LIST;

    mdl_create_builtins();
    mdl_init_built_in_types();

    mdl_value_oblist = mdl_get_atom_from_oblist("OBLIST", mdl_value_root_oblist);
    atom_oblist = mdl_value_oblist->v.a;

    // MUDDLE!- is the version number of MUDDLE.  101 is a lie
    mdl_value_t *muddle = mdl_value_T = mdl_create_atom_on_oblist("MUDDLE", mdl_value_root_oblist);
    mdl_set_gval(muddle->v.a, mdl_new_fix(101));


    mdl_value_T = mdl_create_atom_on_oblist("T", mdl_value_root_oblist);


    mdl_value_atom_redefine = mdl_create_atom_on_oblist("REDEFINE", mdl_value_root_oblist);
    mdl_value_atom_default = mdl_create_atom_on_oblist("DEFAULT", mdl_value_root_oblist);
    mdl_create_atom_on_oblist("OPT", mdl_value_root_oblist); // for DECL checking
    mdl_create_atom_on_oblist("COMMENT", mdl_value_root_oblist);

    mdl_value_t *mdl_value_atom_interrupts = mdl_create_atom_on_oblist("INTERRUPTS", mdl_value_root_oblist);
    mdl_value_t *iobl = mdl_create_oblist(mdl_value_atom_interrupts, MDL_OBLIST_HASHBUCKET_DEFAULT);
    
    mdl_value_atom_lastprog = mdl_create_atom_on_oblist("LPROG ", iobl);
    mdl_value_atom_lastmap = mdl_create_atom_on_oblist("LMAP ", iobl);
    
    mdl_value_t *atomval_initial = mdl_create_atom_on_oblist("INITIAL", mdl_value_root_oblist);
    // ROOT already exists on ROOT because it's a SUBR
    mdl_value_t *atomval_root = mdl_get_atom_from_oblist("ROOT", mdl_value_root_oblist);

    mdl_internal_eval_putprop(mdl_value_initial_oblist, mdl_value_oblist, atomval_initial);
    mdl_internal_eval_putprop(mdl_value_root_oblist, mdl_value_oblist, atomval_root);
    mdl_internal_eval_putprop(atomval_initial, mdl_value_oblist, mdl_value_initial_oblist);
    mdl_internal_eval_putprop(atomval_root, mdl_value_oblist, mdl_value_root_oblist);

    cur_process_bindid = 1;

    toplevel_frame = mdl_new_frame();
    toplevel_frame->subr = mdl_get_atom("TOPLEVEL!-", true, NULL);  // fixme? One of these TOPLEVELS is wrong.
    toplevel_frame->frame_flags = MDL_FRAME_FLAGS_TRUEFRAME;
    initial_frame = mdl_new_frame();
    initial_frame->subr = mdl_get_atom("TOPLEVEL!-", true, NULL);
    initial_frame->frame_flags = MDL_FRAME_FLAGS_TRUEFRAME;
    initial_frame->prev_frame = toplevel_frame;

    mdl_value_t *dotoblist = mdl_additem(NULL, mdl_value_initial_oblist);
    mdl_additem(dotoblist, mdl_value_root_oblist);
    mdl_value_t *commaoblist = mdl_internal_shallow_copy_list(dotoblist);
    dotoblist = mdl_make_list(dotoblist);
    commaoblist = mdl_make_list(commaoblist);

    mdl_set_lval(atom_oblist, dotoblist, initial_frame);
    mdl_set_gval(atom_oblist, commaoblist);

    // initialize channels
    mdl_value_t *mdl_value_atom_inchan = mdl_create_atom_on_oblist("INCHAN", mdl_value_root_oblist);
    mdl_value_t *mdl_value_atom_outchan = mdl_create_atom_on_oblist("OUTCHAN", mdl_value_root_oblist);

    mdl_value_t *def_inchan = mdl_create_default_inchan();
    mdl_set_lval(mdl_value_atom_inchan->v.a, def_inchan, initial_frame);
    mdl_set_gval(mdl_value_atom_inchan->v.a, def_inchan);

    mdl_value_t *def_outchan = mdl_create_default_outchan();
    mdl_set_lval(mdl_value_atom_outchan->v.a, def_outchan, initial_frame);
    mdl_set_gval(mdl_value_atom_outchan->v.a, def_outchan);
    
    last_assoc_clean = GC_gc_no;
}

bool mdl_is_true(mdl_value_t *item)
{
    if (!item) mdl_call_error("WTF", NULL);
    return item->type != MDL_TYPE_FALSE;
}

mdl_value_t *mdl_boolean_value(bool v)
{
    return v?mdl_value_T:&mdl_value_false;
}

mdl_value_t *mdl_internal_list_rest(const mdl_value_t *val, int skip)
{
    // takes a value of primtype "list", returns the portion of the list without
    // initial "type" element and without the "skip" elements after that
    // i.e. mdl_internal_list_rest(list,0) is the entire list without the type
    // returns null for an empty list, (mdl_value_t *)-1 for error

    if (val->pt != PRIMTYPE_LIST) return (mdl_value_t *)-1;
    mdl_value_t *result = val->v.p.cdr;
    while (skip--)
    {
        if (!result) return (mdl_value_t *)-1;
        result = result->v.p.cdr;
    }
    return result;
}

mdl_value_t *mdl_internal_list_nth(const mdl_value_t *val, int skip)
{
    mdl_value_t *result = mdl_internal_list_rest(val, skip);
    if (result == NULL || (result == (mdl_value_t *)-1)) return NULL;
    return result->v.p.car;
}

mdl_value_t *mdl_internal_vector_rest(const mdl_value_t *val, int skip)
{
    // takes a value of primtype "vector", returns a pointer to the
    // vector items starting at "skip"

    if (val->pt != PRIMTYPE_VECTOR) return (mdl_value_t *)-1;
    mdl_value_t *result = val->v.v.p->elements + val->v.v.offset + val->v.v.p->startoffset + skip;
    return result;
}

mdl_value_t *mdl_internal_tuple_rest(const mdl_value_t *val, int skip)
{
    // takes a value of primtype "tuple", returns a pointer to the
    // vector items starting at "skip"

    if (val->pt != PRIMTYPE_TUPLE) return (mdl_value_t *)-1;
    if (skip >= (val->v.tp.p->size - val->v.tp.offset)) return NULL;
    mdl_value_t *result = val->v.tp.p->elements + val->v.tp.offset + skip;
    return result;
}

uvector_element_t *mdl_internal_uvector_rest(const mdl_value_t *val, int skip)
{
    // takes a value of primtype "uvector", returns a pointer to the
    // uvector elements starting at "skip"

    if (val->pt != PRIMTYPE_UVECTOR) return (uvector_element_t *)-1;
    uvector_element_t *result = val->v.uv.p->elements + val->v.uv.p->startoffset + val->v.uv.offset + skip;
    return result;
}

bool mdl_valid_uvector_primtype(int pt)
{
    switch (pt)
    {
    case PRIMTYPE_ATOM:
    case PRIMTYPE_LIST:
    case PRIMTYPE_WORD:
    case PRIMTYPE_VECTOR:
    case PRIMTYPE_UVECTOR:
        return true;
    }
    return false;
}

mdl_value_t *mdl_uvector_element_to_value(const mdl_value_t *uv, const uvector_element_t *elem, mdl_value_t *to)
{
    if (!to) to = mdl_new_mdl_value();
    to->type = UVTYPE(uv);
    to->pt = mdl_type_primtype(UVTYPE(uv));
    switch (to->pt)
    {
    case PRIMTYPE_ATOM:
        to->v.a = elem->a;
        break;
    case PRIMTYPE_LIST:
        to->v.p.cdr = elem->l;
        break;
    case PRIMTYPE_WORD:
        to->v.w = elem->w;
        break;
    case PRIMTYPE_VECTOR:
        to->v.v = elem->v;
        break;
    case PRIMTYPE_UVECTOR:
        to->v.uv = elem->uv;
        break;
    }
    return to;
}

mdl_value_t *mdl_internal_uvector_nth(const mdl_value_t *val, int skip)
{
    uvector_element_t *elem;

    if (val->pt != PRIMTYPE_UVECTOR) return (mdl_value_t *)-1;
    elem = mdl_internal_uvector_rest(val, skip);
    if (!elem) return NULL;
    return mdl_uvector_element_to_value(val, elem);
}

uvector_element_t *mdl_uvector_value_to_element(const mdl_value_t *newval, uvector_element_t *elem)
{
    switch (newval->pt)
    {
    case PRIMTYPE_ATOM:
        elem->a = newval->v.a;
        break;
    case PRIMTYPE_LIST:
        elem->l = newval->v.p.cdr;
        break;
    case PRIMTYPE_WORD:
        elem->w = newval->v.w;
        break;
    case PRIMTYPE_VECTOR:
        elem->v = newval->v.v;
        break;
    case PRIMTYPE_UVECTOR:
        elem->uv = newval->v.uv;
        break;
    }
    return elem;
}

mdl_value_t *mdl_internal_uvector_put(mdl_value_t *val, int skip, mdl_value_t *newval)
{
    uvector_element_t *elem;

    if (val->pt != PRIMTYPE_UVECTOR) return (mdl_value_t *)-1;
    if (newval->type != UVTYPE(val)) return (mdl_value_t *)-1;
    elem = mdl_internal_uvector_rest(val, skip);
    if (!elem) return NULL;
    mdl_uvector_value_to_element(newval, elem);
    return val;
}

mdl_value_t *mdl_internal_eval_nth(mdl_value_t *arg, mdl_value_t *indexval)
{
    int index = 1;
    if (indexval) 
    {
        if (indexval->type != MDL_TYPE_FIX )
            return mdl_call_error("Second argument to NTH must be a FIX", NULL);
        index = indexval->v.w;
    }
    return mdl_internal_eval_nth_i(arg, index);
}
    
mdl_value_t *mdl_internal_eval_nth_i(mdl_value_t *arg, int index)
{
    if (index <= 0)
        return mdl_call_error("ARGUMENT-OUT-OF-RANGE",NULL);

    mdl_value_t *result;
    switch (arg->pt)
    {
    case PRIMTYPE_LIST:
        result = LITEM(arg, index - 1);
        if (!result)
            return mdl_call_error("ARGUMENT-OUT-OF-RANGE",NULL);
        break;
    case PRIMTYPE_STRING:
        if (index > arg->v.s.l)
            return mdl_call_error("ARGUMENT-OUT-OF-RANGE",NULL);
        result = mdl_new_fix((int)arg->v.s.p[index - 1]);
        result->type = MDL_TYPE_CHARACTER;
        break;
    case PRIMTYPE_VECTOR:
        if (index > VLENGTH(arg))
            return mdl_call_error("ARGUMENT-OUT-OF-RANGE",NULL);
        result = VITEM(arg, index - 1);
        break;
    case PRIMTYPE_UVECTOR:
        if (index > UVLENGTH(arg))
            return mdl_call_error("ARGUMENT-OUT-OF-RANGE",NULL);
        result = UVITEM(arg, index - 1);
        break;
    case PRIMTYPE_TUPLE:
        if (index > TPLENGTH(arg))
            return mdl_call_error("ARGUMENT-OUT-OF-RANGE",NULL);
        result = TPITEM(arg, index - 1);
        break;
    default:
        if (mdl_primtype_nonstructured(arg->pt))
        {
            return mdl_call_error_ext("FIRST-ARG-WRONG-TYPE", "First arg to NTH must be structured", NULL);
        }
        // other structued types such as BYTES
        mdl_error("UNIMPLEMENTED PRIMTYPE");
    }
    return result;
}

mdl_value_t *mdl_internal_eval_nth_copy(mdl_value_t *arg, mdl_value_t *indexval)
{
    // need to ensure that objects internal to other structures aren't
    // returned (in some cases)
    mdl_value_t *val = mdl_internal_eval_nth(arg, indexval);
    if (val && 
        (arg->pt == PRIMTYPE_VECTOR ||
         arg->pt == PRIMTYPE_LIST ||
         arg->pt == PRIMTYPE_TUPLE))
    {
        mdl_value_t *copy = mdl_new_mdl_value();
        *copy = *val;
        return copy;
    }
    return val;
}

bool mdl_internal_struct_is_empty(mdl_value_t *arg)
{
    mdl_value_t *tmp;
    bool result;
    switch (arg->pt)
    {
    case PRIMTYPE_LIST:
        tmp = LREST(arg, 0);
        result = !tmp || (tmp == (mdl_value_t *)-1);
        break;
    case PRIMTYPE_STRING:
        result = arg->v.s.l == 0;
        break;
    case PRIMTYPE_VECTOR:
        result = VLENGTH(arg) == 0;
        break;
    case PRIMTYPE_UVECTOR:
        result = UVLENGTH(arg) == 0;
        break;
    case PRIMTYPE_TUPLE:
        result = TPLENGTH(arg) == 0;
        break;
    default:
        if (mdl_primtype_nonstructured(arg->pt))
        {
            mdl_error("Argument to LENGTH must be structured");
        }
        // BYTES
        mdl_error("UNIMPLEMENTED PRIMTYPE");
    }
    return result;
}

mdl_value_t *mdl_internal_eval_rest_i(mdl_value_t *arg, int index)
{
    mdl_value_t *result;

    if (index < 0)
        mdl_error("REST index too small");

    switch (arg->pt)
    {
    case PRIMTYPE_LIST:
        result = LREST(arg, index);
        if (result == (mdl_value_t *)-1)
//            mdl_error("REST index too large");
            return mdl_call_error_ext("OUT-OF-BOUNDS", "REST", NULL);
        result = mdl_make_list(result);
        break;
    case PRIMTYPE_STRING:
        if (index > arg->v.s.l)
            mdl_error("REST index too large");
        result = mdl_make_string(arg->v.s.l - index, arg->v.s.p + index);
        break;
    case PRIMTYPE_VECTOR:
        if (index > VLENGTH(arg))
            mdl_error("REST index too large");
        result = mdl_new_mdl_value();
        result->pt = PRIMTYPE_VECTOR;
        result->type = MDL_TYPE_VECTOR;
        result->v.v.p = arg->v.v.p;
        result->v.v.offset = arg->v.v.offset + index;
        break;
    case PRIMTYPE_UVECTOR:
        if (index > UVLENGTH(arg))
            mdl_error("REST index too large");
        result = mdl_new_mdl_value();
        result->pt = PRIMTYPE_UVECTOR;
        result->type = MDL_TYPE_UVECTOR;
        result->v.uv.p = arg->v.uv.p;
        result->v.uv.offset = arg->v.uv.offset + index;
        break;
    case PRIMTYPE_TUPLE:
        if (index > TPLENGTH(arg))
            mdl_error("REST index too large");
        result = mdl_new_mdl_value();
        result->pt = PRIMTYPE_TUPLE;
        result->type = MDL_TYPE_TUPLE;
        result->v.tp.p = arg->v.tp.p;
        result->v.tp.offset = arg->v.tp.offset + index;
        break;
    default:
        if (mdl_primtype_nonstructured(arg->pt))
        {
            mdl_error("Argument to REST must be structured");
        }
        // BYTES
        mdl_error("UNIMPLEMENTED PRIMTYPE");
    }
    return result;
}

mdl_value_t *mdl_internal_eval_rest(mdl_value_t *arg, mdl_value_t *indexval)
{
    int index = 1;
    if (indexval)
    {
        if (indexval->type != MDL_TYPE_FIX)
            mdl_error("Second argument to REST must be a FIX");
        index = indexval->v.w;
    }
    return mdl_internal_eval_rest_i(arg, index);
}


mdl_value_t *mdl_internal_eval_put(mdl_value_t *arg, mdl_value_t *indexval, mdl_value_t *newitem)
{
    if (indexval->type != MDL_TYPE_FIX)
        return mdl_call_error_ext("SECOND-ARG-WRONG-TYPE", "Must be a FIX", cur_frame->subr, NULL);
    int index = indexval->v.w;
    
    if (index <= 0)
        return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "PUT index too small", NULL);

    mdl_value_t *tail;
    switch (arg->pt)
    {
    case PRIMTYPE_LIST:

        tail = LREST(arg, index - 1);
        if (!tail || tail == (mdl_value_t *)-1)
            return mdl_call_error_ext("ARGUMENT-OUT-OF_RANGE", "PUT index too large", NULL);
        tail->v.p.car = newitem;
        break;
    case PRIMTYPE_VECTOR:

        tail = VREST(arg, index - 1);
        if (!tail || tail == (mdl_value_t *)-1)
            return mdl_call_error_ext("ARGUMENT-OUT-OF_RANGE", "PUT index too large", NULL);
        *tail = *newitem;
        break;
    case PRIMTYPE_UVECTOR:
        uvector_element_t *uvtail;
        uvtail = UVREST(arg, index - 1);
        if (!uvtail || uvtail == (uvector_element_t *)-1)
            mdl_error("PUT index too large");
        if (UVTYPE(arg) != newitem->type)
            return mdl_call_error("UVECTOR-PUT-TYPE-VIOLATION", cur_frame->subr, NULL);
        mdl_uvector_value_to_element(newitem, uvtail);
        break;

    case PRIMTYPE_STRING:
        if (index > arg->v.s.l)
            mdl_error("PUT index too large");
        if (mdl_string_immutable(arg))
            mdl_error("String is immutable");
        if (newitem->type != MDL_TYPE_CHARACTER)
            mdl_error("Error in PUT: Strings may only contain CHARACTER type");
        arg->v.s.p[index - 1] = (char)newitem->v.w;
        break;

    case PRIMTYPE_TUPLE:
        tail = TPREST(arg, index - 1);
        if (!tail || tail == (mdl_value_t *)-1)
            mdl_error("PUT index too large");
        *tail = *newitem;
        break;

    default:
        if (mdl_primtype_nonstructured(arg->pt))
        {
            return mdl_call_error_ext("FIXME", "First argument to PUT must be structured", cur_frame->subr, NULL);
        }
        // VECTOR, UVECTOR, BYTES
        mdl_error("UNIMPLEMENTED PRIMTYPE");
    }
    return arg;
}

mdl_value_t *mdl_internal_eval_putprop(mdl_value_t *item, mdl_value_t *indicator, mdl_value_t *val)
{
    struct mdl_assoc_key_t key;

    key.item = item;
    key.indicator = indicator;
    if (val == NULL)
        mdl_delete_assoc(mdl_assoc_table, &key);
    else
        mdl_add_assoc(mdl_assoc_table, &key, val);
    return item;
}

mdl_value_t *mdl_internal_eval_getprop(mdl_value_t *item, mdl_value_t *indicator)
{
    struct mdl_assoc_key_t key = {item, indicator};

    return mdl_assoc_find_value(mdl_assoc_table, &key);
}

mdl_value_t *mdl_mapfr_loop()
{
    mdl_interp_frame_t *my_iframe = mdl_interp_stack;
    IARGSETUP();
    IGETNEXTARG(loopf);
    IGETNEXTARG(flist);
    mdl_value_t *flastitemptr = iargs_cursor;
    IGETNEXTARG(flastitem);
    IGETNEXTARG(structs);
    bool mapstop_called = false;
    bool mapret_called = false;
    switch (my_iframe->jumpval)
    {
    case 0: // normal case
        break;
    case LONGJMP_MAPSTOP:
        mapstop_called = true;
        /* FALLTHROUGH */
    case LONGJMP_MAPRET:
        mapret_called = true;
        if (!cur_frame->result || cur_frame->result->type != MDL_TYPE_LIST)
            mdl_error("MAPSTOP/MAPRET must return list");
        if (flist) {
            flastitem->v.p.cdr = cur_frame->result->v.p.cdr;
            while (flastitem->v.p.cdr)
                flastitem = flastitem->v.p.cdr;
            flastitemptr->v.p.car = flastitem;
        }
        else
            mdl_error("MAPSTOP/MAPRET with no finalf?");
        cur_frame->result = NULL;
        break;
    case LONGJMP_MAPLEAVE:
        my_iframe->retval = cur_frame->result;
        mdl_pop_frame(cur_frame->prev_frame);
        return my_iframe->retval;
    default:
        mdl_error("Bad longjmp to MAPF/R");
    }

    if (my_iframe->retval != NULL && flist != NULL && !mapret_called) 
    {
        // This is not the first time through the loop; save the results
        mdl_additem(flist, my_iframe->retval, &flastitem);
        flastitemptr->v.p.car = flastitem;
    }
    // check for termination

    int num_structs = structs ? TPLENGTH(structs) : 0;
    for (int i = 0; mapstop_called || i < num_structs; i++)
    {
        mdl_value_t *s_n = mapstop_called ? NULL : TPITEM(structs, i);
        if (mapstop_called || mdl_internal_struct_is_empty(s_n)) {
            if (my_iframe->retval == NULL) 
                my_iframe->retval = &mdl_value_false;
            if (flist)
                mdl_push_interp_mapfr_final(RETURN_COPYDOWN, flist);
            // why pop the frame before final?  Because you can't use mapstop/leave/return from final.
            mdl_pop_frame(cur_frame->prev_frame);
            return my_iframe->retval;
        }
    }
    mdl_value_t *lastitem = NULL;
    mdl_value_t *apply_list = mdl_additem(NULL, loopf, &lastitem);
    for (int i = 0; i < num_structs; i++)
    {
        mdl_value_t *s_n = TPITEM(structs, i);
        mdl_value_t *val = NULL;
        if (my_iframe->method == INTERP_MAPR_LOOP)
        {
            val = mdl_internal_eval_rest_i(s_n, 0);
        }
        else 
        {
            val = mdl_internal_eval_nth_copy(s_n, 0);
        }
        mdl_additem(lastitem, val, &lastitem);
        mdl_value_t *nextval = mdl_internal_eval_rest_i(s_n, 1);
        *TPITEM(structs, i) = *nextval;
    }
    apply_list = mdl_make_list(apply_list, MDL_TYPE_FORM);
    mdl_push_interp_apply(RETURN_COPYFORWARD, loopf, apply_list, true);
    my_iframe->started = false;
    return NULL;
}

mdl_value_t *mdl_mapfr_final()
{
    IARGSETUP();
    IGETNEXTARG(result_ilist);
    mdl_value_t* results = mdl_make_list(result_ilist, MDL_TYPE_FORM);
    mdl_push_interp_apply(RETURN_COPYDOWN, result_ilist->v.p.car, results, true);
    return NULL;
}

mdl_value_t *mdl_internal_eval_mapfr(mdl_value_t *args, bool is_mapr)
{
    mdl_value_t *finalf = LITEM(args, 0);
    mdl_value_t *loopf = LITEM(args, 1);
    mdl_value_t *sp = LREST(args, 2);
    bool hasfinal;
    mdl_frame_t *frame = mdl_new_frame();
    mdl_frame_t *prev_frame = cur_frame;

    if (!loopf)
        mdl_error("Not enough args to MAPF/R");

    // set up the frame
    frame->prev_frame = prev_frame;

    // frame->subr = mdl_value_atom_mapf; FIXME
    // setup the activation (or should it be a frame?)
    mdl_value_t *act = mdl_make_frame_value(frame, MDL_TYPE_ACTIVATION); 
    mdl_bind_local_symbol(mdl_value_atom_lastmap->v.a, act, frame, false);
    frame->frame_flags = MDL_FRAME_FLAGS_ACTIVATION;
    mdl_push_frame(frame);

    hasfinal = mdl_is_true(finalf);
    int num_structs = 0;
    mdl_value_t *stup = NULL;
    if (sp)
    {
        stup = mdl_make_tuple(sp, MDL_TYPE_TUPLE, false);
        num_structs = TPLENGTH(stup);
    }
    for (int i = 0; i < num_structs; i++) {
        mdl_value_t *s = TPITEM(stup, i);
        if (mdl_primtype_nonstructured(s->pt))
            mdl_error("Arguments to MAPF must be structured");
    }
    if (hasfinal) 
    {
        mdl_value_t *flastitem = NULL;
        mdl_value_t *flist = mdl_additem(NULL, finalf, &flastitem);
        mdl_push_interp_mapfr_loop(RETURN_COPYDOWN, is_mapr, loopf, flist, flastitem, stup);
    }
    else 
    {
        mdl_push_interp_mapfr_loop(RETURN_COPYDOWN, is_mapr, loopf, NULL, NULL, stup);
    }
    // Longjmp/mapret/mapstop/mapleave returns to loop.
    frame->interp_frame2 = mdl_interp_stack;
    return NULL;
}

mdl_value_t *mdl_listen_error_pt2() 
{
    // Zork's behavior implies this loops, but the documentation
    // suggests the looping is within REP... hmm, looking at Zork, maybe older
    // Confusion was in error -- ZREP repeats.
    IARGSETUP();
    IGETNEXTARG(is_error_val);
    mdl_interp_frame_t *my_iframe = mdl_interp_stack;
    cur_frame->interp_frame2 = mdl_interp_stack;
    int jumpval = mdl_setjmp(cur_frame->interp_frame2);
    if (jumpval == LONGJMP_ERRET)
    {
        my_iframe->started = true;  // End tail recursion
        return cur_frame->result;
    }
    else if (jumpval != 0)
    {
        fprintf(stderr, "Bad longjmp to LISTEN frame %d\n", jumpval);
        return NULL;
    }

    mdl_value_t *result;
    // Not sure if this should be REP or REP!-.  Probably "REP" from zork
    mdl_value_t *atom_rep = mdl_get_atom("REP", true, NULL);
    mdl_value_t *rep = mdl_both_symbol_lookup(atom_rep->v.a, cur_frame);
    // FIXME -- is_error doesn't belong here.  I think I did it to allow escape from bad
    // REP.
    my_iframe->started = false; // tail recurse forever.
    if (!mdl_is_true(is_error_val) && rep && mdl_type_is_applicable(mdl_apply_type(rep->type)))
    {
        result = mdl_internal_apply(rep, mdl_make_list(mdl_cons_internal(atom_rep, NULL)), true);
    }
    else
    {
        // a new frame is not made here; this is called out
        // explicitly in the documentation
        mdl_value_t *mdl_builtin_eval_rep(void);
        
        fprintf(stderr, "Atom REP has neither LVAL nor GVAL\n");
        mdl_builtin_eval_rep();
    }
    // Fix return value so it comes here correctly.
    if (my_iframe != mdl_interp_stack && mdl_interp_stack->return_to == RETURN_COPYDOWN)
        mdl_interp_stack->return_to = RETURN_COPYFORWARD;
    return result;
}

mdl_value_t *mdl_internal_listen_error(mdl_value_t *args, bool is_error)
{
    mdl_value_t *oblists;
    mdl_value_t *inchan;
    mdl_value_t *outchan;
    mdl_value_t *atom_inchan, *atom_outchan;
    mdl_value_t *atom_l_level;
    mdl_value_t *atom_lerr;
    mdl_value_t *l_level;
    mdl_value_t *intlevel, *atom_intlevel;
    int printflags;

    oblists = mdl_local_symbol_lookup(atom_oblist, cur_frame);
    if (!mdl_oblists_are_reasonable(oblists))
    {
        fprintf(stderr, "LVAL of OBLIST not reasonable\n");
        oblists = mdl_global_symbol_lookup(atom_oblist);
        if (!mdl_oblists_are_reasonable(oblists))
        {
            fprintf(stderr, "GVAL of OBLIST not reasonable\n");
            oblists = mdl_cons_internal(mdl_value_root_oblist, NULL);
            oblists = mdl_cons_internal(mdl_value_initial_oblist, oblists);
            oblists = mdl_make_list(oblists);
        }
    }
    mdl_bind_local_symbol(atom_oblist, oblists, cur_frame, false);

    atom_inchan = mdl_get_atom("INCHAN!-", true, NULL);
    inchan = mdl_local_symbol_lookup(atom_inchan->v.a, cur_frame);
    if (!mdl_inchan_is_reasonable(inchan))
    {
        fprintf(stderr, "LVAL of INCHAN not reasonable\n");
        inchan = mdl_global_symbol_lookup(atom_inchan->v.a);
        if (!mdl_inchan_is_reasonable(inchan))
        {
            fprintf(stderr, "GVAL of INCHAN not reasonable\n");
            inchan = mdl_create_default_inchan();
        }
    }
    mdl_bind_local_symbol(atom_inchan->v.a, inchan, cur_frame, false);

    atom_outchan = mdl_get_atom("OUTCHAN!-", true, NULL);
    outchan = mdl_local_symbol_lookup(atom_outchan->v.a, cur_frame);
    if (!mdl_outchan_is_reasonable(outchan))
    {
        fprintf(stderr, "LVAL of OUTCHAN not reasonable\n");
        outchan = mdl_global_symbol_lookup(atom_outchan->v.a);
        if (!mdl_outchan_is_reasonable(outchan))
        {
            fprintf(stderr, "GVAL of OUTCHAN not reasonable\n");
            outchan = mdl_create_default_outchan();
        }
    }
    mdl_bind_local_symbol(atom_outchan->v.a, outchan, cur_frame, false);
    printflags = mdl_chan_mode_is_print_binary(outchan)?MDL_PF_BINARY:0;
    
    atom_l_level = mdl_get_atom("L-LEVEL !-INTERRUPTS!-", true, NULL);
    l_level = mdl_local_symbol_lookup(atom_l_level->v.a, cur_frame);
    mdl_print_value(stderr, l_level);
    fprintf(stderr, "\n ll after = ");
    if (!l_level || l_level->type != MDL_TYPE_FIX)
        l_level = mdl_new_fix(1);
    else
        l_level = mdl_new_fix(l_level->v.w + 1);
    mdl_bind_local_symbol(atom_l_level->v.a, l_level, cur_frame, false);
    mdl_print_value(stderr, l_level);
    fprintf(stderr, "\n");

    atom_lerr = mdl_get_atom("L-ERR !-INTERRUPTS!-", true, NULL);
    mdl_bind_local_symbol(atom_lerr->v.a, mdl_make_frame_value(cur_frame), cur_frame, false);
    if (is_error)
    {
        mdl_print_newline_to_chan(outchan, printflags, NULL);
        mdl_print_string_to_chan(outchan, "*ERROR*", 7, 0, true, true);
        mdl_print_char_to_chan(outchan, ' ', printflags, NULL);
    }

    if (!suppress_listen_message)
    {
        // print args
        args = args->v.p.cdr;
        while (args)
        {
            mdl_print_newline_to_chan(outchan, printflags, NULL);
            mdl_print_value_to_chan(outchan, args->v.p.car, false, true, NULL);
            mdl_print_char_to_chan(outchan, ' ', printflags, NULL);
            args = args->v.p.cdr;
        }
        
        atom_intlevel = mdl_get_atom("INT-LEVEL!-INTERRUPTS!-", true, NULL);
        intlevel = mdl_global_symbol_lookup(atom_intlevel->v.a);
        
        mdl_print_newline_to_chan(outchan, printflags, NULL);
        mdl_print_string_to_chan(outchan, "LISTENING-AT-LEVEL ", 18, 0, true, false);
        mdl_print_value_to_chan(outchan, l_level, false, true, NULL);
        mdl_print_string_to_chan(outchan, "PROCESS", 7, 0, true, true);
        mdl_print_string_to_chan(outchan, "1", 1, 0, true, true);
        if (intlevel && (intlevel->type != MDL_TYPE_FIX || intlevel->v.w != 0))
        {
            mdl_print_string_to_chan(outchan, "INT-LEVEL", 9, 0, true, true);
            mdl_print_value_to_chan(outchan, intlevel, false, true, NULL);
        }
        mdl_print_newline_to_chan(outchan, printflags, NULL);
    }
    suppress_listen_message = false;

    mdl_push_interp_listen_error_pt2(RETURN_COPYDOWN, is_error);
    return NULL;
}

void mdl_toplevel(FILE *restorefile)
{
    if (restorefile)
    {
        bool restored = mdl_read_image(restorefile);
        fclose(restorefile);
        if (!restored) {
            fprintf(stderr, "Initial restore failed");
            exit(-1);
        }
        suppress_listen_message = true;
        mdl_interp_stack->retval = mdl_new_string(8, "RESTORED");
        if (!mdl_chan_at_eof(mdl_get_default_inchan()))
        {
            mdl_interp_from_stack();
        }
    } 
    else
    {
        suppress_listen_message = false;
        mdl_push_interp_toplevel(RETURN_RETURN);
        mdl_interp_stack->started = true;
        cur_frame = initial_frame;
        cur_frame->frame_flags |= MDL_FRAME_FLAGS_TRUEFRAME;
        cur_frame->args = mdl_make_list(NULL);
        cur_frame->interp_frame2 = mdl_interp_stack;
        cur_frame->subr = mdl_get_atom("TOPLEVEL!-", true, NULL);
        cur_frame->result = NULL;
        if (!mdl_chan_at_eof(mdl_get_default_inchan()))
        {
            mdl_std_apply(mdl_value_builtin_listen, mdl_make_list(mdl_cons_internal(mdl_value_builtin_listen, NULL)), MDL_TYPE_SUBR, true);
            mdl_interp_from_stack();
        }
    }
    cur_frame = NULL;
}

mdl_value_t *mdl_internal_erret(mdl_value_t *result, mdl_value_t *frame)
{
    if (result)
    {
        if (!frame) frame = mdl_local_symbol_lookup_pname("L-ERR !-INTERRUPTS!-", cur_frame);
        if (!frame) mdl_error("No frame in ERRET!");
        frame->v.f->result = result;
        mdl_longjmp_to(frame->v.f, LONGJMP_ERRET);
    }
    else
    {
        mdl_longjmp_to(cur_process_initial_frame, LONGJMP_ERRET);
    }
}

// built-in support macros

// Random access to args
#define GETARG(a, n) ((a) = LITEM(cur_frame->args, n))
#define GETREQARG(a, n) do {(a) = LITEM(cur_frame->args, n); if (!a) return mdl_call_error("TOO-FEW-ARGUMENTS-SUPPLIED", cur_frame->subr, NULL); }  while(0)
#define DENYARG(n) do {if (LITEM(cur_frame->args, n)) return mdl_call_error("TOO-MANY-ARGUMENTS-SUPPLIED", cur_frame->subr, NULL); } while(0)

// Sequential access to args
#define ARGSETUP() mdl_value_t *args_cursor = cur_frame->args->v.p.cdr

#define GETNEXTARG(a) do { if (args_cursor) { (a) = args_cursor->v.p.car; args_cursor = args_cursor->v.p.cdr; } else { (a) = NULL; } } while(0)
#define REQNEXTARG() do { if (!args_cursor) { return mdl_call_error("TOO-FEW-ARGUMENTS-SUPPLIED", cur_frame->subr, NULL);  } } while(0)
#define GETNEXTREQARG(a) do { if (args_cursor) { (a) = args_cursor->v.p.car; args_cursor = args_cursor->v.p.cdr; } else { return mdl_call_error("TOO-FEW-ARGUMENTS-SUPPLIED", cur_frame->subr, NULL);  } } while(0)
#define REMAINING_ARGS() (args_cursor)
#define NOMOREARGS() do { if (args_cursor) { return mdl_call_error("TOO-MANY-ARGUMENTS-SUPPLIED", cur_frame->subr, NULL); }} while(0)

// below this point are built-ins
// BEGIN BUILT-INS (do not remove this line)

mdl_value_t *mdl_builtin_eval_quote()
/* FSUBR */
{
    mdl_value_t *arg;
    ARGSETUP();

    GETNEXTREQARG(arg);
    NOMOREARGS();

    return arg;
}

mdl_value_t *mdl_builtin_eval_lval()
/* SUBR */
{
    ARGSETUP();
    mdl_frame_t *frame;
    mdl_value_t *atomval; 
    mdl_value_t *frameval;

    GETNEXTREQARG(atomval);
    GETNEXTARG(frameval);
    NOMOREARGS();

    if (frameval)
    {
        if (frameval->pt != PRIMTYPE_FRAME)
            mdl_error("Type Mismatch, 2nd arg to LVAL must be frame");
        frame = frameval->v.f;
    }
    else frame = cur_frame;

    if (atomval->pt != PRIMTYPE_ATOM)
        return mdl_call_error("Type Mismatch, 1st arg to LVAL must be atom", NULL);
    mdl_value_t *result = mdl_local_symbol_lookup(atomval->v.a, frame);
    if (!result || result->type == MDL_TYPE_UNBOUND)
    {
        return mdl_call_error_ext("UNBOUND-VARIABLE", "LVAL", atomval, NULL);
    }
    return result;
}

mdl_value_t *mdl_builtin_eval_gval()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *atomval;

    GETNEXTREQARG(atomval);
    NOMOREARGS();

    if (atomval->pt != PRIMTYPE_ATOM)
        mdl_call_error_ext("FIRST-ARG-WRONG-TYPE", "First arg to GVAL must be atom", NULL);
    mdl_value_t *result = mdl_global_symbol_lookup(atomval->v.a);
    if (!result || result->type == MDL_TYPE_UNBOUND)
    {
        return mdl_call_error_ext("UNBOUND-VARIABLE", "GVAL", atomval, NULL);
    }
    return result;
}

mdl_value_t *mdl_builtin_eval_value()
/* SUBR */
{
    ARGSETUP();
    mdl_frame_t *frame;
    mdl_value_t *frameval;
    mdl_value_t *atomval;

    GETNEXTREQARG(atomval);
    GETNEXTARG(frameval);
    NOMOREARGS();

    if (frameval)
    {
        if (frameval->pt != PRIMTYPE_FRAME)
            mdl_error("Type Mismatch, 2nd arg to VALUE must be frame");
        frame = frameval->v.f;
    }
    else frame = cur_frame;

    if (atomval->pt != PRIMTYPE_ATOM)
            mdl_error("Type Mismatch, 1st arg to VALUE must be atom");
    mdl_value_t *result = mdl_local_symbol_lookup(atomval->v.a, frame);
    if (!result || result->type == MDL_TYPE_UNBOUND)
        result = mdl_global_symbol_lookup(atomval->v.a);
    if (!result || result->type == MDL_TYPE_UNBOUND)
        mdl_error("Attempt to determine VALUE of atom without one");
    return result;
}

mdl_value_t *mdl_builtin_eval_bound()
/* SUBR BOUND? */
{
    ARGSETUP();
    mdl_frame_t *frame;
    mdl_value_t *frameval;
    mdl_value_t *atomval;

    GETNEXTREQARG(atomval);
    GETNEXTARG(frameval);
    NOMOREARGS();

    if (frameval)
    {
        if (frameval->pt != PRIMTYPE_FRAME)
            mdl_error("Type Mismatch, 2nd arg to BOUND? must be frame");
        frame = frameval->v.f;
    }
    else frame = cur_frame;
    if (atomval->pt != PRIMTYPE_ATOM)
            mdl_error("Type Mismatch, 1st arg to BOUND? must be atom");
    mdl_value_t *result = mdl_local_symbol_lookup(atomval->v.a, frame);
    return mdl_boolean_value(result != NULL);
}

mdl_value_t *mdl_builtin_eval_assigned()
/* SUBR ASSIGNED? */
{
    ARGSETUP();
    mdl_frame_t *frame;
    mdl_value_t *frameval;
    mdl_value_t *atomval;

    GETNEXTREQARG(atomval);
    GETNEXTARG(frameval);
    NOMOREARGS();

    if (frameval)
    {
        if (frameval->pt != PRIMTYPE_FRAME)
            mdl_error("Type Mismatch, 2nd arg to ASSIGNED? must be frame");
        frame = frameval->v.f;
    }
    else frame = cur_frame;
    if (atomval->pt != PRIMTYPE_ATOM)
            mdl_error("Type Mismatch, 1st arg to ASSIGNED? must be atom");
    mdl_value_t *result = mdl_local_symbol_lookup(atomval->v.a, frame);
    return mdl_boolean_value(result && result->type != MDL_TYPE_UNBOUND);
}

mdl_value_t *mdl_builtin_eval_gbound()
/* SUBR GBOUND? */
{
    ARGSETUP();
    mdl_value_t *atomval;

    GETNEXTREQARG(atomval);
    NOMOREARGS();

    if (atomval->pt != PRIMTYPE_ATOM)
            mdl_error("Type Mismatch, 1st arg to GBOUND? must be atom");
    mdl_value_t *result = mdl_global_symbol_lookup(atomval->v.a);
    return mdl_boolean_value(result != NULL);
}

mdl_value_t *mdl_builtin_eval_gassigned()
/* SUBR GASSIGNED? */
{
    ARGSETUP();
    mdl_value_t *atomval;

    GETNEXTREQARG(atomval);
    NOMOREARGS();

    if (atomval->pt != PRIMTYPE_ATOM)
            mdl_error("Type Mismatch, 1st arg to GASSIGNED? must be atom");
    mdl_value_t *result = mdl_global_symbol_lookup(atomval->v.a);
    return mdl_boolean_value(result && result->type != MDL_TYPE_UNBOUND);
}

mdl_value_t *mdl_builtin_eval_gunassign()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *atomval;

    GETNEXTREQARG(atomval);
    NOMOREARGS();

    if (atomval->pt != PRIMTYPE_ATOM)
        return mdl_call_error("FIRST-ARG-WRONG-TYPE", "First arg to GUNASSIGN must be atom", NULL);
    mdl_set_gval(atomval->v.a, &mdl_value_unassigned);
    return atomval;
}

mdl_value_t *mdl_builtin_eval_setg()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *atomval;
    mdl_value_t *newval;

    GETNEXTREQARG(atomval);
    GETNEXTREQARG(newval);
    NOMOREARGS();

    if (atomval->pt != PRIMTYPE_ATOM)
            mdl_error("Type Mismatch, 1st arg to SETG must be atom");
    return mdl_set_gval(atomval->v.a, newval);
}

mdl_value_t *mdl_builtin_eval_set()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *atomval;
    mdl_value_t *newval;
    mdl_frame_t *frame = cur_frame;
    mdl_value_t *frameval;

    GETNEXTREQARG(atomval);
    GETNEXTREQARG(newval);
    GETNEXTARG(frameval);
    NOMOREARGS();

    if (atomval->pt != PRIMTYPE_ATOM)
            mdl_error("Type Mismatch, 1st arg to SET must be atom");
    if (frameval)
    {
        if (frameval->pt != PRIMTYPE_FRAME)
            mdl_error("Type Mismatch, 3rd arg to SET must be frame");
        frame = frameval->v.f;
    }
    return mdl_set_lval(atomval->v.a, newval, frame);
}

mdl_value_t *mdl_cond_pt2() {
    IARGSETUP();
    IGETNEXTARG(cur_clause_arg); 

    mdl_value_t *retval = mdl_interp_stack->retval;
    if (retval) {
        if (mdl_is_true(mdl_interp_stack->retval)) {
            mdl_value_t *eval_list = LREST(cur_clause_arg->v.p.car, 1);
            if (eval_list != NULL)
                mdl_push_interp_eval_body_state_machine(RETURN_COPYDOWN, eval_list, NULL);
            return retval;
        }
        cur_clause_arg = cur_clause_arg->v.p.cdr;
        IARGRESET(mdl_interp_stack);
        ISETNEXTARG(cur_clause_arg);
    }
    if (cur_clause_arg) {
        mdl_value_t *cur_clause = cur_clause_arg->v.p.car;
        if (cur_clause->type != MDL_TYPE_LIST)
            return mdl_call_error_ext("BAD-CLAUSE", "COND clauses must be LISTs", NULL);
        if (!LHASITEM(cur_clause, 0))
            return mdl_call_error_ext("BAD-CLAUSE", "COND clauses must have at least one item", NULL);
        mdl_interp_stack->started = false;
        mdl_push_interp_eval(RETURN_COPYFORWARD, LITEM(cur_clause,0), 0);
    }
    return retval;
}

mdl_value_t *mdl_builtin_eval_cond()
/* FSUBR */
{
    ARGSETUP();
    mdl_value_t *cur_clause_arg = REMAINING_ARGS();
    REQNEXTARG();
    mdl_push_interp_cond_pt2(RETURN_COPYDOWN, cur_clause_arg);
    return NULL;
}

// TYPE subrs (6.3)
mdl_value_t *mdl_builtin_eval_type()
{
    ARGSETUP();
    mdl_value_t *tobj;

    GETNEXTREQARG(tobj);
    NOMOREARGS();

    return mdl_newatomval(mdl_type_atom(tobj->type));
}

mdl_value_t *mdl_builtin_eval_primtype()
{
    ARGSETUP();
    mdl_value_t *tobj;

    GETNEXTREQARG(tobj);
    NOMOREARGS();
    
    return mdl_newatomval(mdl_type_atom(tobj->pt));
}

mdl_value_t *mdl_builtin_eval_typeprim()
{
    ARGSETUP();
    mdl_value_t *tobj;
    int typenum;

    GETNEXTREQARG(tobj);
    NOMOREARGS();
    
    typenum = mdl_get_typenum(tobj);
    if (typenum == MDL_TYPE_NOTATYPE)
        mdl_error("Value passed to TYPEPRIM was not a type");
    return mdl_newatomval(mdl_type_atom(mdl_type_primtype(typenum)));}

mdl_value_t *mdl_builtin_eval_chtype()
{
    ARGSETUP();
    mdl_value_t *tobj;
    mdl_value_t *newtype;
    mdl_value_t *nobj;
    int typecode;

    GETNEXTREQARG(tobj);
    GETNEXTREQARG(newtype);
    NOMOREARGS();

    typecode = mdl_get_typenum(newtype);
    if (mdl_type_primtype(typecode) != tobj->pt)
        mdl_error("PRIMTYPES do not match in CHTYPE");
    nobj = mdl_new_mdl_value();
    *nobj = *tobj;
    nobj->type = typecode;
    return nobj;
    
}
// More SUBRs related to types (6.4)
mdl_value_t *mdl_builtin_eval_alltypes()
/* SUBR */
{
    DENYARG(0);
    return mdl_typevector();
}

mdl_value_t *mdl_builtin_eval_valid_typep()
/* SUBR VALID-TYPE? */
{
    ARGSETUP();
    int typenum;
    mdl_value_t *obj;

    GETNEXTREQARG(obj);
    NOMOREARGS();

    typenum = mdl_get_typenum(obj);
    if (typenum == MDL_TYPE_NOTATYPE)
        return &mdl_value_false;
    return mdl_new_word(typenum, MDL_TYPE_TYPE_C);
}

mdl_value_t *mdl_builtin_eval_newtype()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *newtype = NULL;
    mdl_value_t *oldtype = NULL;
    mdl_value_t *desc = NULL;
    int oldtypenum, prevtypenum;

    GETNEXTARG(newtype);
    GETNEXTREQARG(oldtype);
    GETNEXTARG(desc);
    NOMOREARGS();

    oldtypenum = mdl_get_typenum(oldtype);
    if (oldtypenum == MDL_TYPE_NOTATYPE)
        mdl_error("Second argument to NEWTYPE must name a type");
    prevtypenum = mdl_get_typenum(newtype);

    mdl_value_t *redefine = mdl_local_symbol_lookup(mdl_value_atom_redefine->v.a);
    if (!redefine || redefine->type == MDL_TYPE_UNBOUND ||
        !mdl_is_true(redefine))
    {
        // FIXME -- not sure if REDEFINE allows redefining types
        if (prevtypenum != MDL_TYPE_NOTATYPE && 
            mdl_type_primtype(prevtypenum) != mdl_type_primtype(oldtypenum))
            mdl_error("First arg to NEWTYPE must not already be a type");
    }
    return mdl_internal_newtype(newtype, oldtypenum);
}

mdl_value_t *mdl_builtin_eval_evaltype()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *thetype = NULL;
    mdl_value_t *how = NULL;
    mdl_value_t *result;
    int typenum;

    GETNEXTREQARG(thetype);
    GETNEXTARG(how);
    NOMOREARGS();

    typenum = mdl_get_typenum(thetype);
    if (typenum == MDL_TYPE_NOTATYPE)
        mdl_error("First argument to EVALTYPE must name a type");
    if (!how) 
    {
        result = mdl_get_evaltype(typenum);
        if (result == NULL) result = &mdl_value_false;
    }
    else
    {
        
        result = thetype;
        if (mdl_value_double_equal(how, mdl_value_builtin_eval)) 
            mdl_set_evaltype(typenum, NULL);
        else
            mdl_set_evaltype(typenum, how);
    }
    return result;    
}

mdl_value_t *mdl_builtin_eval_applytype()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *thetype = NULL;
    mdl_value_t *how = NULL;
    mdl_value_t *result;
    int typenum;

    GETNEXTREQARG(thetype);
    GETNEXTARG(how);
    NOMOREARGS();
    typenum = mdl_get_typenum(thetype);
    if (typenum == MDL_TYPE_NOTATYPE)
        mdl_error("First argument to APPLYTYPE must name a type");
    if (!how) 
    {
        result = mdl_get_applytype(typenum);
        if (result == NULL) result = &mdl_value_false;
    }
    else
    {
        result = thetype;
        if (mdl_value_double_equal(how, mdl_value_builtin_apply)) 
            mdl_set_applytype(typenum, NULL);
        else
            mdl_set_applytype(typenum, how);
    }
    return result;    
}

mdl_value_t *mdl_builtin_eval_printtype()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *thetype = NULL;
    mdl_value_t *how = NULL;
    mdl_value_t *result;
    int typenum;

    GETNEXTREQARG(thetype);
    GETNEXTARG(how);
    NOMOREARGS();
    typenum = mdl_get_typenum(thetype);
    if (typenum == MDL_TYPE_NOTATYPE)
        mdl_error("First argument to PRINTTYPE must name a type");
    if (!how) 
    {
        result = mdl_get_printtype(typenum);
        if (result == NULL) result = &mdl_value_false;
    }
    else
    {
        result = thetype;
        if (mdl_value_double_equal(how, mdl_value_builtin_print)) 
            mdl_set_printtype(typenum, NULL);
        else
            mdl_set_printtype(typenum, how);
    }
    return result;    
}
// STRUCTURE OBJECT builtins

mdl_value_t *mdl_builtin_eval_length()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *arg;
    int count = 0;

    GETNEXTREQARG(arg);
    NOMOREARGS();

    switch (arg->pt)
    {
    case PRIMTYPE_LIST:
    case PRIMTYPE_STRING:
    case PRIMTYPE_VECTOR:
    case PRIMTYPE_UVECTOR:
    case PRIMTYPE_TUPLE:
        count = mdl_internal_struct_length(arg);
        break;
    default:
        if (mdl_primtype_nonstructured(arg->pt))
        {
            mdl_error("Argument to LENGTH must be structured");
        }
        // BYTES
        mdl_error("UNIMPLEMENTED PRIMTYPE");
    }
    return mdl_new_fix(count);
}

mdl_value_t *mdl_builtin_eval_nth()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *arg;
    mdl_value_t *indexval;

    GETNEXTREQARG(arg);
    GETNEXTARG(indexval);
    NOMOREARGS();
    
    return mdl_internal_eval_nth_copy(arg, indexval);
}

mdl_value_t *mdl_builtin_eval_rest()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *arg;
    mdl_value_t *indexval;

    GETNEXTREQARG(arg);
    GETNEXTARG(indexval);
    NOMOREARGS();

    return mdl_internal_eval_rest(arg, indexval);
}

mdl_value_t *mdl_builtin_eval_put()
/* SUBR */
{
    
    ARGSETUP();
    mdl_value_t *arg;
    mdl_value_t *indexval;
    mdl_value_t *newitem;

    GETNEXTARG(arg);
    GETNEXTREQARG(indexval);
    GETNEXTARG(newitem);
    NOMOREARGS();

    if (mdl_primtype_structured(arg->pt) &&
        (indexval->type == MDL_TYPE_FIX || indexval->type == MDL_TYPE_OFFSET))
    {
        if (newitem == NULL)
        {
            return mdl_call_error("TOO-FEW-ARGUMENTS-SUPPLIED", NULL);
        }
        return mdl_internal_eval_put(arg, indexval, newitem);
    }
    else
        return mdl_internal_eval_putprop(arg, indexval, newitem);
}

mdl_value_t *mdl_builtin_eval_get()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *arg;
    mdl_value_t *indexval;
    mdl_value_t *exp;

    GETNEXTREQARG(arg);
    GETNEXTARG(indexval);
    GETNEXTARG(exp);
    NOMOREARGS();

    if (mdl_primtype_structured(arg->pt) &&
        (!indexval || indexval->type == MDL_TYPE_FIX || indexval->type == MDL_TYPE_OFFSET))
    {
        if (exp != NULL) 
            return mdl_call_error_ext("TOO-MANY-ARGUMENTS-SUPPLIED", "No EXP allowed for GET on structure", NULL);
        return mdl_internal_eval_nth_copy(arg, indexval);
    }
    else
    {
        mdl_value_t *result =  mdl_internal_eval_getprop(arg, indexval);
        if (result == NULL)
        {
            if (exp) {
                mdl_push_interp_eval(RETURN_COPYDOWN, exp);
            }
            else result = &mdl_value_false;
        }
        return result;
    }
}

// SUBSTRUC -- note that this makes a shallow copy.

mdl_value_t *mdl_builtin_eval_substruc()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *from;
    mdl_value_t *restval;
    mdl_value_t *amountval;
    mdl_value_t *to;

    GETNEXTREQARG(from);
    GETNEXTARG(restval);
    GETNEXTARG(amountval);
    GETNEXTARG(to);
    NOMOREARGS();
    
    if (mdl_primtype_nonstructured(from->pt))
        return mdl_call_error_ext("FIRST-ARG-WRONG-TYPE", "First arg to SUBSTRUC must be structured", NULL);
    if (restval && restval->type != MDL_TYPE_FIX)
        return mdl_call_error_ext("SECOND-ARG-WRONG-TYPE", "Second argument to SUBSTRUC must be a FIX", NULL);
    if (amountval && amountval->type != MDL_TYPE_FIX)
        return mdl_call_error_ext("THIRD-ARG-WRONG-TYPE", "Third argument to SUBSTRUC must be a FIX", NULL);
    if (to && ((int)from->pt != to->type) && (from->pt != PRIMTYPE_TUPLE && to->type != PRIMTYPE_VECTOR))
    {
        return mdl_call_error_ext("ARG-WRONG-TYPE", "Last arg to SUBSTRUC must be same type as primtype of first arg", NULL);
    }
    if (amountval && amountval->v.w < 0)
    {
        return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC amount cannot be negative", NULL);
    }
    int rest = 0;
    int amount = -1;

    if (restval) rest = restval->v.w;
    if (amountval) amount = amountval->v.w;

    if (rest < 0)
    {
        return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC rest cannot be negative", NULL);
    }

    switch (from->pt)
    {
    case PRIMTYPE_LIST: 
    {
        mdl_value_t *start = LREST(from, rest);
        if (start == (mdl_value_t *)-1)
        {
            return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC rest too large", NULL);
        }
        if (!to)
        {
            mdl_value_t *lastitem = NULL;
            if (start && amount--)
            {
                to = lastitem = mdl_newlist();
                to->v.p.car = start->v.p.car;
                start = start->v.p.cdr;
            }
            while(start && amount--)
            {
                mdl_additem(lastitem, start->v.p.car, &lastitem);
                start = start->v.p.cdr;
            }
            to = mdl_make_list(to);
        }
        else
        {
            mdl_value_t *cursor = LREST(to, 0);
            while(start && amount--)
            {
                if (!cursor)
                    return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC destination too short", NULL);
                cursor->v.p.car = start->v.p.car;
                cursor = cursor->v.p.cdr;
                start = start->v.p.cdr;
            }
        }
        if (amount > 0)
            return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC amount too big for source", NULL);
        break;
    }
    case PRIMTYPE_VECTOR:
        if (rest > VLENGTH(from))
            return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC rest too large", NULL);
        if (amount < 0) amount = VLENGTH(from) - rest;
        if ((rest + amount) > VLENGTH(from))
            return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC amount too big for source", NULL);
        if (to && (amount > VLENGTH(to)))
            return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC destination too short", NULL);
        if (!to) to = mdl_new_empty_vector(amount, MDL_TYPE_VECTOR);
        memcpy(VREST(to,0), VREST(from, rest), amount * sizeof(mdl_value_t));
        break;
    case PRIMTYPE_UVECTOR:
        if (rest > UVLENGTH(from))
            return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC rest too large", NULL);
        if (amount < 0) amount = UVLENGTH(from) - rest;
        if ((rest + amount) > UVLENGTH(from))
            return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC amount too big for source", NULL);
        if (to && (amount > UVLENGTH(to)))
            return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC destination too short", NULL);
        if (to && (UVTYPE(to) != UVTYPE(from)))
            return mdl_call_error_ext("TYPES-DIFFER-IN-UNIFORM-VECTOR", "SUBSTRUC UVECTOR to and from must be same type", NULL);
        if (!to) 
        {
            to = mdl_new_empty_uvector(amount, MDL_TYPE_UVECTOR);
            UVTYPE(to) = UVTYPE(from);
        }
        memcpy(UVREST(to,0), UVREST(from, rest), amount * sizeof(uvector_element_t));
        break;
    case PRIMTYPE_TUPLE:
        if (rest > TPLENGTH(from))
            return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC rest too large", NULL);
        if (amount < 0) amount = TPLENGTH(from) - rest;
        if ((rest + amount) > TPLENGTH(from))
            return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC amount too big for source", NULL);
        if (to && (amount > mdl_internal_struct_length(to)))
            return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC destination too short", NULL);
        if (!to) to = mdl_new_empty_vector(amount, MDL_TYPE_VECTOR);
        if (to->pt == PRIMTYPE_VECTOR)
            memcpy(VREST(to,0), TPREST(from, rest), amount * sizeof(mdl_value_t));
        else // must be another tuple
            memcpy(TPREST(to,0), TPREST(from, rest), amount * sizeof(mdl_value_t));
        break;
    case PRIMTYPE_STRING:
        if (rest > from->v.s.l)
            return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC rest too large", NULL);
        if (amount < 0) amount = from->v.s.l - rest;
        if ((rest + amount) > from->v.s.l)
            return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC amount too big for source", NULL);
        if (to && (amount > to->v.s.l))
            return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SUBSTRUC destination too short", NULL);
        if (to) memcpy(to->v.s.p, from->v.s.p + rest, amount);
        else to = mdl_new_string(amount, from->v.s.p + rest);
        break;
    default:
        mdl_error("UNIMPLEMENTED PRIMTYPE");
    }

    return to;
}

// 7.5.3 LIST, VECTOR, UVECTOR, STRING (also TUPLE, which doesn't belong)
mdl_value_t *mdl_builtin_eval_list()
/* SUBR */
{
    // Naively, one could just return args here.  But if there's a SEGMENT
    // passed as an argument that won't work; instead, a copy must be returned
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *result = mdl_internal_shallow_copy_list(LREST(args,0));
    return mdl_make_list(result);
}

mdl_value_t *mdl_builtin_eval_form()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *result = mdl_internal_shallow_copy_list(LREST(args,0));
    return mdl_make_list(result, MDL_TYPE_FORM);
    return result;
}

mdl_value_t *mdl_builtin_eval_vector()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    return mdl_make_vector(LREST(args,0), MDL_TYPE_VECTOR);
}

mdl_value_t *mdl_builtin_eval_tuple()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    return mdl_make_tuple(LREST(args,0), MDL_TYPE_TUPLE);
}

mdl_value_t *mdl_builtin_eval_uvector()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    return mdl_make_uvector(LREST(args,0), MDL_TYPE_UVECTOR);
}

// ahh, yes, form and function -- but note that function is an fsubr
mdl_value_t *mdl_builtin_eval_function()
/* FSUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *result = mdl_internal_shallow_copy_list(LREST(args,0));
    return mdl_make_list(result, MDL_TYPE_FUNCTION);
}

mdl_value_t *mdl_builtin_eval_string()
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *cursor = LREST(args,0);
    mdl_value_t *result;
    int length = 0;
    while (cursor)
    {
        switch (cursor->v.p.car->type)
        {
        case MDL_TYPE_CHARACTER:
            length++;
            break;
        case MDL_TYPE_STRING:
            length += cursor->v.p.car->v.s.l;
            break;
        default:
            mdl_call_error_ext("ARG-WRONG-TYPE", "Arguments to STRING must be strings or characters", NULL);
        }
        cursor = cursor->v.p.cdr;
    }
    result = mdl_new_string(length);
    char *s = result->v.s.p;
    cursor = LREST(args,0);
    while (cursor)
    {
        switch (cursor->v.p.car->type)
        {
        case MDL_TYPE_CHARACTER:
            *s++ = (char)cursor->v.p.car->v.w;
            break;
        case MDL_TYPE_STRING:
            memcpy(s, cursor->v.p.car->v.s.p, cursor->v.p.car->v.s.l);
            s += cursor->v.p.car->v.s.l;
            break;
        }
        cursor = cursor->v.p.cdr;
    }
    return result;
}

// 7.5.4-7.5.5 ILIST, IVECTOR, IUVECTOR, ISTRING, IFORM, ITUPLE
mdl_value_t *mdl_internal_eval_ilist(int type)
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *nelem;
    mdl_value_t *expr;
    int nelements;
    mdl_value_t *lastelem = NULL;
    mdl_value_t *firstelem = NULL;
    mdl_value_t *tmp;
    
    GETNEXTREQARG(nelem);
    GETNEXTARG(expr);
    NOMOREARGS();
    if (nelem->type != MDL_TYPE_FIX) return mdl_call_error_ext("FIRST-ARG-WRONG-TYPE", "First argument must be FIX", cur_frame->subr, NULL);
    nelements = nelem->v.w;
    if (nelements < 0) return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "Number of elements must be >= 0", cur_frame->subr, NULL);
    if (nelements && expr) {
        mdl_push_interp_ilist_pt2(RETURN_COPYDOWN, mdl_make_list(NULL, type));
        return NULL;
    }
    else {
        while (nelements--)
        {
            tmp = mdl_additem(lastelem, mdl_new_word(0, MDL_TYPE_LOSE), &lastelem);
            if (!firstelem) firstelem = tmp;
        }
        return mdl_make_list(firstelem, type);
    }
}
mdl_value_t *mdl_builtin_eval_ilist()
/* SUBR */
{
    return mdl_internal_eval_ilist(MDL_TYPE_LIST);
}

mdl_value_t *mdl_builtin_eval_iform()
/* SUBR */
{
    return mdl_internal_eval_ilist(MDL_TYPE_FORM);
}

mdl_value_t *mdl_builtin_eval_ituple()
/* SUBR */
{
    // BOGUS -- tuples should live on stack.
    return mdl_internal_eval_ilist(MDL_TYPE_TUPLE);
}

mdl_value_t *mdl_ilist_pt2() {
    mdl_value_t *nelem;
    mdl_value_t *expr;
    ARGSETUP();
    GETNEXTARG(nelem);
    GETNEXTARG(expr);
    IARGSETUP();
    IGETNEXTARG(lastelem);
    IGETNEXTARG(nelements_val);
    IGETNEXTARG(firstelem);
    if (mdl_interp_stack->retval) {
        mdl_value_t *tmp = mdl_additem(lastelem, mdl_interp_stack->retval, &lastelem);
        if (!firstelem->v.p.cdr) firstelem->v.p.cdr = tmp;
    }
    if (nelements_val == NULL) {
        nelements_val = mdl_new_fix(nelem->v.w);
    }
    if (nelements_val->v.w) {
        IARGRESET(mdl_interp_stack);
        ISETNEXTARG(lastelem);
        // WARNING: Modifying FIX
        nelements_val->v.w--;
        ISETNEXTARG(nelements_val);
        mdl_interp_stack->started = false;
        mdl_push_interp_eval(RETURN_COPYFORWARD, expr);
        return NULL;
    }
    return firstelem;
}

mdl_value_t *mdl_builtin_eval_ivector()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *nelem;
    mdl_value_t *expr;
    int nelements;
    mdl_value_t *result;
    mdl_value_t *elem;
    
    GETNEXTREQARG(nelem);
    GETNEXTARG(expr);
    NOMOREARGS();
    if (nelem->type != MDL_TYPE_FIX) return mdl_call_error_ext("FIRST-ARG-WRONG-TYPE", "First argument must be FIX", cur_frame->subr, NULL);
    nelements = nelem->v.w;
    if (nelements < 0) return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "Number of elements must be >= 0", cur_frame->subr, NULL);
    result = mdl_new_empty_vector(nelements, MDL_TYPE_VECTOR);
    if (expr && nelements) {
        mdl_push_interp_i_u_vector_pt2(RETURN_COPYDOWN, result);
        return NULL;
    } else if (nelements) {
        elem = VREST(result,0);
        while (nelements--)
        {
            elem->type = MDL_TYPE_LOSE;
            elem->pt = PRIMTYPE_WORD;
            elem->v.w = 0;
            elem++;
        }
    }
    return result;
}

mdl_value_t *mdl_builtin_eval_iuvector()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *nelem;
    mdl_value_t *expr;
    int nelements;
    mdl_value_t *result;
    
    GETNEXTREQARG(nelem);
    GETNEXTARG(expr);
    NOMOREARGS();
    if (nelem->type != MDL_TYPE_FIX) return mdl_call_error_ext("FIRST-ARG-WRONG-TYPE", "First argument must be FIX", cur_frame->subr, NULL);
    nelements = nelem->v.w;
    if (nelements < 0) return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "Number of elements must be >= 0", cur_frame->subr, NULL);
    result = mdl_new_empty_uvector(nelements, MDL_TYPE_UVECTOR);
    if (expr && nelements)
    {
        mdl_push_interp_i_u_vector_pt2(RETURN_COPYDOWN, result);
        return NULL;
    }
    else UVTYPE(result) = MDL_TYPE_LOSE;
    return result;
}

// Handles vector and uvector
mdl_value_t *mdl_i_u_vector_pt2() {
    mdl_value_t *nelem;
    mdl_value_t *expr;
    ARGSETUP();
    GETNEXTARG(nelem);
    GETNEXTARG(expr);
    IARGSETUP();
    IGETNEXTARG(curelement_val);
    IGETNEXTARG(result);
    mdl_interp_frame_t *my_iframe = mdl_interp_stack;
    if (my_iframe->retval) {
        if (result->pt == PRIMTYPE_UVECTOR) {
            if (curelement_val->v.w == 1) {
                UVTYPE(result) = mdl_interp_stack->retval->type;
            }
        }

        mdl_internal_eval_put(result, curelement_val, mdl_interp_stack->retval);
        if (my_iframe != mdl_interp_stack) {
            // This indicates an error in the put, should be TYPES-DIFFER-IN-UNIFORM-VECTOR
            return NULL;
        }
    }
    if (curelement_val == NULL) {
        curelement_val = mdl_new_fix(0);
        IARGRESET(mdl_interp_stack);
        ISETNEXTARG(curelement_val);
    }
    if (curelement_val->v.w < nelem->v.w) {
        // WARNING: Modifying FIX
        curelement_val->v.w++;
        my_iframe->started = false;
        mdl_push_interp_eval(RETURN_COPYFORWARD, expr);
        return NULL;
    }
    return result;
}

mdl_value_t *mdl_builtin_eval_istring()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *nelem;
    mdl_value_t *expr;
    int nelements;
    mdl_value_t *result;
    
    GETNEXTREQARG(nelem);
    GETNEXTARG(expr);
    NOMOREARGS();
    if (nelem->type != MDL_TYPE_FIX) return mdl_call_error_ext("FIRST-ARG-WRONG-TYPE", "First argument must be FIX", cur_frame->subr, NULL);
    nelements = nelem->v.w;
    if (nelements < 0) return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "Number of elements must be >= 0", cur_frame->subr, NULL);
    result = mdl_new_string(nelements);
    if (expr && nelements)
    {
        mdl_push_interp_istring_pt2(RETURN_COPYDOWN, result);
    }
    return result;
}

mdl_value_t *mdl_istring_pt2() {
    mdl_value_t *nelem;
    mdl_value_t *expr;
    ARGSETUP();
    GETNEXTARG(nelem);
    GETNEXTARG(expr);
    IARGSETUP();
    IGETNEXTARG(curelement_val);
    IGETNEXTARG(result);
    mdl_interp_frame_t *my_iframe = mdl_interp_stack;
    if (my_iframe->retval) {
        if (my_iframe->retval->type != MDL_TYPE_CHARACTER ||
            my_iframe->retval->v.w > 0177) {
            return mdl_call_error("BAD-ASCII-CHARACTER", cur_frame->subr, my_iframe->retval, NULL);
        }
        result->v.s.p[curelement_val->v.w] = my_iframe->retval->v.w;
    }
    if (curelement_val == NULL) {
        curelement_val = mdl_new_fix(0);
        IARGRESET(my_iframe);
        ISETNEXTARG(curelement_val);
    } else {
        // WARNING: Modifying FIX
        curelement_val->v.w++;
    }
    if (curelement_val->v.w < nelem->v.w) {
        my_iframe->started = false;
        mdl_push_interp_eval(RETURN_COPYFORWARD, expr);
        return NULL;
    }
    return result;
}

// 7.6 PRIMTYPE LIST operations
mdl_value_t *mdl_builtin_eval_putrest()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *head;
    mdl_value_t *tail;

    GETNEXTARG(head);
    GETNEXTREQARG(tail);
    NOMOREARGS();
    if (head->pt != PRIMTYPE_LIST) return mdl_call_error_ext("FIRST-ARG-WRONG-TYPE", "First arg to PUTREST must have primtype LIST", cur_frame->subr, head, NULL);
    if (tail->pt != PRIMTYPE_LIST) return mdl_call_error_ext("SECOND-ARG-WRONG-TYPE", "Second arg to PUTREST must have primtype LIST", cur_frame->subr, head, NULL);
    
    if (!head->v.p.cdr)
        mdl_error("Can't PUTREST on an empty list");
    head->v.p.cdr->v.p.cdr = tail->v.p.cdr;
    return head;
}

mdl_value_t *mdl_builtin_eval_cons()
{
    ARGSETUP();
    mdl_value_t *newfirst;
    mdl_value_t *list;

    GETNEXTARG(newfirst);
    GETNEXTREQARG(list);
    NOMOREARGS();

    if (list->pt != PRIMTYPE_LIST) mdl_error("Second arg to CONS must have primtype LIST");

    return mdl_make_list(mdl_cons_internal(newfirst, LREST(list,0)));
}

// ARRAY items (7.6.2)

mdl_value_t *mdl_builtin_eval_back()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *array;
    mdl_value_t *fix;
    mdl_value_t *result;
    int backup = 1;

    GETNEXTREQARG(array);
    GETNEXTARG(fix);
    NOMOREARGS();

    if (fix) backup = fix->v.w;
    if (backup < 1) mdl_error("Must BACK at least 1 element");
    
    switch (array->pt)
    {
    case PRIMTYPE_VECTOR:
        if (backup > (array->v.v.offset + array->v.v.p->startoffset))
            mdl_error("Offset to BACK too large");
        result = mdl_new_mdl_value();
        *result = *array;
        result->v.v.offset -= backup;
        break;
    case PRIMTYPE_UVECTOR:
        if (backup > (array->v.uv.offset + array->v.uv.p->startoffset))
            mdl_error("Offset to BACK too large");
        result = mdl_new_mdl_value();
        *result = *array;
        result->v.uv.offset -= backup;
        break;
    case PRIMTYPE_STRING:
    {
        int full_length = mdl_string_length(array);
        if (backup > (full_length - array->v.s.l))
            mdl_error("Offset to BACK too large");
        result = mdl_new_mdl_value();
        *result = *array;
        result->v.s.p -= backup;
        result->v.s.l += backup;
        break;
    }
    default:
        mdl_error("Bad type for BACK");
    }
    return result;
}

mdl_value_t *mdl_builtin_eval_top()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *array;
    mdl_value_t *result;

    GETNEXTREQARG(array);
    NOMOREARGS();
    
    switch (array->pt)
    {
    case PRIMTYPE_VECTOR:
        result = mdl_new_mdl_value();
        *result = *array;
        result->v.v.offset = -array->v.v.p->startoffset;
        break;
    case PRIMTYPE_UVECTOR:
        result = mdl_new_mdl_value();
        *result = *array;
        result->v.uv.offset = -array->v.v.p->startoffset;
        break;
    case PRIMTYPE_STRING:
    {
        int full_length = mdl_string_length(array);
        result = mdl_new_mdl_value();
        *result = *array;
        result->v.s.p = array->v.s.p + array->v.s.l - full_length;
        result->v.s.l = full_length;
        break;
    }
    default:
        mdl_error("Bad type for TOP");
    }
    return result;
}

template <class T, class R> R mdl_get_union(T);
template <class T> counted_string_t * mdl_get_string(T)
{
    return NULL;
}

template <> inline mdl_value_union *mdl_get_union<mdl_value_t *, mdl_value_union *>(mdl_value_t *v)
{
    return &v->v;
}

template <> inline uvector_element_t *mdl_get_union<uvector_element_t *, uvector_element_t *>(uvector_element_t *v)
{
    return v;
}

template <> inline counted_string_t *mdl_get_string<mdl_value_t *>(mdl_value_t *v)
{
    return &v->v.s;
}

// gets the cpos-th character of str, or null.  Sets notnull if not null
// hack, hack, kludge, kludge
char mdl_sort_pname_char(char *str, int cpos, bool *notnull)
{
    char *s = str;
    while (*s && cpos)
    {
        s++; cpos--;
    }
    if (*s) *notnull = true;
    return *s;
}

template <class T, class U>
void mdl_radix_exchange_1(T *array, int reclen, int keyoff, primtype_t primtype, int bitno, bool negate, int startrec, int nrecs, mdl_value_t *aux)
{
    T *front, *back;
    T tmp;
    MDL_INT mask;
    int newnrecs1 = 0;
    int i;
    
    if (nrecs < 2) return;
    front = array + startrec * reclen + keyoff;
    back = array + (startrec + nrecs - 1) * reclen + keyoff;

    switch (primtype)
    {
    case PRIMTYPE_ATOM:
    case PRIMTYPE_STRING:
    {
        int cpos = bitno >> 3;
        int bpos = (7-(bitno & 7));
        unsigned char mask = 1<<bpos;
        int frontbit, backbit;
        counted_string_t *frontstr, *backstr;
        char *frontpname, *backpname;
        
        bool didtest;

        while (front < back)
        {
            
            if (primtype == PRIMTYPE_STRING)
            {
                frontstr = mdl_get_string<T*>(front);
                frontbit = (frontstr->l > cpos) && (didtest = true) && (frontstr->p[cpos] & mask);
            }
            else
            {
                // this is very inefficient, as it must iterate each string
                // at each pass -- perhaps atoms should use counted 
                // strings as well
                frontpname = mdl_get_union<T*,U*>(front)->a->pname;
                frontbit = (mdl_sort_pname_char(frontpname, cpos, &didtest) & mask) != 0;
            }
            if (!frontbit) 
            {
                front += reclen;
                newnrecs1++;
            }
            else
            {
                if (primtype == PRIMTYPE_STRING)
                {
                    backstr = mdl_get_string<T*>(back);
                    backbit = (backstr->l > cpos) && (didtest = true) && (backstr->p[cpos] & mask);
                }
                else
                {
                    backpname = mdl_get_union<T*,U*>(back)->a->pname;
                    backbit = (mdl_sort_pname_char(backpname, cpos, &didtest) & mask) != 0;
                }
                if (backbit) 
                {
                    back -= reclen;
                }
                else
                {
                    for (i = -keyoff; i < (reclen - keyoff); i++)
                    {
                        tmp = front[i];
                        front[i] = back[i];
                        back[i] = tmp;
                    }
                    front += reclen;
                    newnrecs1++;
                    back -= reclen;
                }
            }
        }
        if (front == back)
        {
            if (primtype == PRIMTYPE_STRING)
            {
                frontstr = mdl_get_string<T*>(front);
                frontbit = (frontstr->l > cpos) && (didtest = true) && (frontstr->p[cpos] & mask);
            }
            else
            {
                frontpname = mdl_get_union<T*,U*>(front)->a->pname;
                frontbit = (mdl_sort_pname_char(frontpname, cpos, &didtest) & mask) != 0;
            }

            if (!frontbit)
            {
                newnrecs1++;
            }
        }
        if (!didtest) return;
        break;
    }
    case PRIMTYPE_WORD:
        if (bitno >= ((int)sizeof(MDL_INT) << 3)) return;
        mask = (MDL_INT)1 << ((sizeof(MDL_INT)<< 3) - bitno - 1);
        while (front < back)
        {
            if (negate ^ !(mdl_get_union<T*,U*>(front)->w & mask)) 
            {
                front += reclen;
                newnrecs1++;
            }
            else if (negate ^ ((mdl_get_union<T*,U*>(back)->w & mask) != 0)) back -= reclen;
            else
            {
                for (i = -keyoff; i < (reclen - keyoff); i++)
                {
                    tmp = front[i];
                    front[i] = back[i];
                    back[i] = tmp;
                }
                front += reclen;
                newnrecs1++;
                back -= reclen;
            }
        }
        if (negate ^ !(mdl_get_union<T*,U*>(front)->w & mask))
        {
            newnrecs1++;
        }
    }
    mdl_radix_exchange_1<T,U>(array, reclen, keyoff, primtype, bitno + 1, false, startrec, newnrecs1, aux);
    mdl_radix_exchange_1<T,U>(array, reclen, keyoff, primtype, bitno + 1, false, startrec + newnrecs1, nrecs - newnrecs1, aux);
}

template <class T, class U> void mdl_radix_exchange_0(T *array, int reclen, int keyoff, primtype_t primtype, int nrecs, mdl_value_t *aux)
{
    int i, off;
    int bitno = 0;
    MDL_INT pstandard = -1, nstandard = 0;
    MDL_UINT tdiff = 0;
    bool negate = false;

    if (primtype == PRIMTYPE_WORD)
    {
        for (i = 0, off = keyoff; i < nrecs; i++, off += reclen)
        {
            if (mdl_get_union<T*,U*>(&array[off])->w < 0)
            {
                if (nstandard >= 0) nstandard = mdl_get_union<T*,U*>(&array[off])->w;
                else tdiff |= (MDL_UINT)mdl_get_union<T*,U*>(&array[off])->w ^ (MDL_UINT)nstandard;
            }
            else
            {
                if (pstandard < 0) pstandard = mdl_get_union<T*,U*>(&array[off])->w;
                else tdiff |= (MDL_UINT)mdl_get_union<T*,U*>(&array[off])->w ^ (MDL_UINT)pstandard;
            }
        }
    }
    
    if (primtype == PRIMTYPE_WORD)
    {
        bitno = (sizeof(MDL_INT) << 3);
        while (tdiff)
        {
            bitno--;
            tdiff >>= 1;
        }
        if ((pstandard >= 0) && (nstandard < 0) && bitno) 
        {
            negate = true;
            bitno--;
        }
        if (bitno == (sizeof(MDL_INT) << 3)) return;
    }
    mdl_radix_exchange_1<T,U>(array, reclen, keyoff, primtype, bitno, negate, 0, nrecs, aux);
}

void mdl_radix_exchange_vector_tuple(mdl_value_t *array, int reclen, int keyoff, int nrecs, mdl_value_t *aux)
{
    int off, i;
    primtype_t primtype = array[keyoff].pt;

    for (i = 1, off = (keyoff + reclen); i < nrecs; i++, off += reclen)
    {
        if (array[off].pt != primtype)
            mdl_error("Mixed primtypes for keys not allowed with FALSE predicate in SORT");
    }
    mdl_radix_exchange_0<mdl_value_t, mdl_value_union>(array, reclen, keyoff, primtype, nrecs, aux);
}

void mdl_radix_exchange_uvector(mdl_value_t *uv, int reclen, int keyoff, int nrecs, mdl_value_t *aux)
{
    primtype_t primtype = mdl_type_primtype(UVTYPE(uv));
    uvector_element_t *elems = UVREST(uv, 0);

    mdl_radix_exchange_0<uvector_element_t, uvector_element_t>(elems, reclen, keyoff, primtype, nrecs, aux);
}

// VECTOR primtypes (7.6.3)
// GROW is missing
mdl_value_t *mdl_builtin_eval_sort()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *pred;
    mdl_value_t *s1;
    mdl_value_t *l1;
    mdl_value_t *off;
    mdl_value_t *arr1;
    int keyoffset = 0;
    mdl_value_t *auxlist;
    int reclen1 = 1;
    int nrecs;
    
    // FIXME -- does not support auxiliary values or predicates
    GETNEXTARG(pred);
    GETNEXTREQARG(s1);
    GETNEXTARG(l1);
    GETNEXTARG(off);
    auxlist = mdl_make_tuple(REMAINING_ARGS());
    if (s1->pt != PRIMTYPE_VECTOR &&
        s1->pt != PRIMTYPE_UVECTOR &&
        s1->pt != PRIMTYPE_TUPLE)
        mdl_error("SORT can sort vectors/tuples only");
    if (l1 && l1->type != MDL_TYPE_FIX)
        mdl_error("Length argument to SORT must be FIX");
    if (off && off->type != MDL_TYPE_FIX)
        mdl_error("Offset argument to SORT must be FIX");
    if (l1) reclen1 = l1->v.w;
    if (off) keyoffset = off->v.w;

    if (keyoffset >= reclen1) mdl_error("Keys outside record in SORT");
    
    switch (s1->pt)
    {
    case PRIMTYPE_VECTOR:
        if ((VLENGTH(s1) % reclen1) != 0)
            mdl_error("Bad record size in SORT");
        nrecs = VLENGTH(s1)/reclen1;
        arr1 = VREST(s1, 0);
        mdl_radix_exchange_vector_tuple(arr1, reclen1, keyoffset, nrecs, auxlist);
        break;
    case PRIMTYPE_TUPLE:
        if ((TPLENGTH(s1) % reclen1) != 0)
            mdl_error("Bad record size in SORT");
        nrecs = TPLENGTH(s1)/reclen1;
        arr1 = TPREST(s1, 0);
        mdl_radix_exchange_vector_tuple(arr1, reclen1, keyoffset, nrecs, auxlist);
        break;
    case PRIMTYPE_UVECTOR:
        if ((UVLENGTH(s1) % reclen1) != 0)
            mdl_error("Bad record size in SORT");
        nrecs = UVLENGTH(s1)/reclen1;
        mdl_radix_exchange_uvector(s1, reclen1, keyoffset, nrecs, auxlist);
        break;
    default:
        mdl_error("Primtype for <SORT <> ...> must be TUPLE, VECTOR, or UVECTOR");
    }
    return s1;
}
// 7.6.5 UVECTOR subrs
mdl_value_t *mdl_builtin_eval_utype()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *uv;

    GETNEXTREQARG(uv);
    NOMOREARGS();
    return mdl_newatomval(mdl_type_atom(UVTYPE(uv)));
}

mdl_value_t *mdl_builtin_eval_chutype()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *uv;
    mdl_value_t *newtype;
    int oldtypenum, newtypenum;
    primtype_t oldprim, newprim;
    
    GETNEXTARG(uv);
    GETNEXTREQARG(newtype);
    NOMOREARGS();

    oldtypenum = UVTYPE(uv);
    oldprim = mdl_type_primtype(oldtypenum);
    newtypenum = mdl_get_typenum(newtype);
    newprim = mdl_type_primtype(newtypenum);
    if (oldtypenum != MDL_TYPE_LOSE)
    {
        if (oldprim != newprim) mdl_error("Can't change primtypes with CHUTYPE");
    }
    else
    {
        if (!mdl_valid_uvector_primtype(newprim))
            mdl_error("Type not valid for UVECTOR");
    }
    UVTYPE(uv) = newtypenum;
    return uv;
}

// 7.6.6 STRING and character
mdl_value_t *mdl_builtin_eval_ascii()
{
    ARGSETUP();
    mdl_value_t *forc;
    mdl_value_t *result;

    GETNEXTREQARG(forc);
    NOMOREARGS();

    if (forc->type == MDL_TYPE_CHARACTER)
    {
        result = mdl_new_mdl_value();
        *result = *forc;
        result->type = MDL_TYPE_FIX;
    }
    else if (forc->type == MDL_TYPE_FIX)
    {
        if (forc->v.w < 0 || forc->v.w > 127) // MDL only does 7-bit ascii
            return mdl_call_error("ARGUMENT-OUT-OF-RANGE", cur_frame->subr, NULL);
        result = mdl_new_mdl_value();
        *result = *forc;
        result->type = MDL_TYPE_CHARACTER;
    }
    else
        return mdl_call_error("FIRST-ARG-WRONG-TYPE", "Argument to ASCII must be FIX or CHARACTER",
                              cur_frame->subr, NULL);
    return result;
}

mdl_value_t *mdl_builtin_eval_parse()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *parse_string = NULL;
    mdl_value_t *radix = NULL;
    mdl_value_t *lookup = NULL;
    mdl_value_t *parse_table = NULL;
    mdl_value_t *look_ahead = NULL;
    mdl_value_t *result;

    GETNEXTARG(parse_string);
    GETNEXTARG(radix);
    GETNEXTARG(parse_table);
    GETNEXTARG(look_ahead);
    NOMOREARGS();

    if (!parse_string)
    {
        mdl_error("FIXME -- use PARSE-STRING");
    }
    else if (parse_string->type != MDL_TYPE_STRING)
        mdl_error("Can't parse a non-string");
    int radixint = 10;
    if (radix)
    {
        if (radix->type == MDL_TYPE_FIX)
            radixint = radix->v.w;
        else
            mdl_error("Radix must be a FIX");
    }
    if (look_ahead && look_ahead->type != MDL_TYPE_CHARACTER)
        mdl_error("Look-ahead must be a character");

    if (lookup)
    {
        mdl_bind_local_symbol(atom_oblist, lookup, cur_frame, false);
    }
    if (parse_string)
    {
        mdl_value_t *mdl_value_atom_parse_string = mdl_get_atom("PARSE-STRING!-", true, NULL);
        mdl_bind_local_symbol(mdl_value_atom_parse_string->v.a, parse_string, cur_frame, false);
    }
    if (parse_table)
    {
        mdl_value_t *mdl_value_atom_parse_table = mdl_get_atom("PARSE-TABLE!-", true, NULL);
        mdl_bind_local_symbol(mdl_value_atom_parse_table->v.a, parse_table, cur_frame, false);
    }

    result = mdl_parse_string(parse_string, radixint, look_ahead);
    return result;
}

// FIXME: LPARSE is missing

mdl_value_t *mdl_builtin_eval_unparse()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *obj = NULL;
    mdl_value_t *radix = NULL;
    mdl_value_t *chan;
    mdl_value_t *result;
    mdl_value_t *mdl_value_atom_outchan;

    GETNEXTREQARG(obj);
    GETNEXTARG(radix);
    NOMOREARGS();

    int radixint = 10;
    if (radix)
    {
        if (radix->type == MDL_TYPE_FIX)
            radixint = radix->v.w;
        else
            mdl_error("Radix must be a FIX");
    }

    chan = mdl_create_internal_output_channel(INTERNAL_BUFSIZE, 0, NULL);
    mdl_value_atom_outchan = mdl_get_atom("OUTCHAN!-", true, NULL);
    mdl_bind_local_symbol(mdl_value_atom_outchan->v.a, chan, cur_frame, false);

    mdl_print_value_to_chan(chan, obj, false, false, NULL);
    result = mdl_get_internal_output_channel_string(chan);
    return result;
}

// DEFMAC and define
mdl_value_t* mdl_def_pt2()
{
    mdl_value_t *args = cur_frame->args;
    // ERRET comes here.
    cur_frame->interp_frame2 = mdl_interp_stack;
    IARGSETUP();
    if (iargs_cursor->v.p.cdr == NULL)
        iargs_cursor->v.p.cdr = mdl_cons_internal(mdl_interp_stack->retval, NULL);
    IGETNEXTARG(list_to_build);
    IGETNEXTARG(firstarg);

    bool is_erret_redefine = false;
    if (mdl_interp_stack->jumpval == LONGJMP_ERRECOVER) 
    {
        if (mdl_is_true(mdl_interp_stack->retval)) {
            mdl_print_value(stderr, mdl_interp_stack->retval);
            is_erret_redefine = true;
        }
        else
            return mdl_interp_stack->retval;
    } else if (mdl_interp_stack->jumpval != 0) {
        return cur_frame->result;
    }

    if (firstarg->type != MDL_TYPE_ATOM)
        return mdl_call_error("FIRST-ARG-WRONG-TYPE", cur_frame->subr, firstarg, NULL);

    if (!is_erret_redefine)
    {
        mdl_value_t *redefine = mdl_local_symbol_lookup(mdl_value_atom_redefine->v.a);
        if (!redefine || redefine->type == MDL_TYPE_UNBOUND || !mdl_is_true(redefine))
        {
            mdl_value_t *result = mdl_global_symbol_lookup(firstarg->v.a);
            if (result && result->type != MDL_TYPE_UNBOUND)
            {
                mdl_print_value(stderr, firstarg);
                mdl_interp_stack->started = false;
                mdl_interp_stack->jumpval = LONGJMP_ERRECOVER;
                return mdl_call_error("ALREADY-DEFINED-ERRET-NON-FALSE-TO-REDEFINE", cur_frame->subr, firstarg, NULL);
            }
        }
    }

    mdl_value_t *func = mdl_internal_shallow_copy_list(LREST(args,1));
    list_to_build->v.p.cdr = func;
    mdl_set_gval(firstarg->v.a, list_to_build);
    return firstarg;
}
/// DEFINE and friends
mdl_value_t *mdl_builtin_eval_define()
/* FSUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *firstarg = LITEM(args, 0);
    if (!firstarg)
        return mdl_call_error("TOO-FEW-ARGUMENTS-SUPPLIED", cur_frame->subr, NULL);
    mdl_push_interp_def_pt2(RETURN_COPYDOWN, mdl_make_list(NULL, MDL_TYPE_FUNCTION));
    mdl_push_interp_eval(RETURN_COPYFORWARD, firstarg);
    return NULL;
}

mdl_value_t *mdl_builtin_eval_defmac()
/* FSUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *firstarg = LITEM(args, 0);
    if (!firstarg)
        return mdl_call_error("TOO-FEW-ARGUMENTS-SUPPLIED", cur_frame->subr, NULL);
    mdl_push_interp_def_pt2(RETURN_COPYDOWN, mdl_make_list(NULL, MDL_TYPE_MACRO));
    mdl_push_interp_eval(RETURN_COPYFORWARD, firstarg);
    return NULL;
}

mdl_value_t *mdl_builtin_eval_expand()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *arg;

    GETNEXTREQARG(arg);
    NOMOREARGS();

    mdl_push_interp_eval(RETURN_COPYDOWN, arg, EVAL_EXPAND, mdl_make_frame_value(toplevel_frame, MDL_TYPE_ENVIRONMENT));
    return NULL;
}

mdl_value_t *mdl_builtin_eval_eval()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *arg;
    mdl_value_t *env;

    GETNEXTREQARG(arg);
    GETNEXTARG(env);
    NOMOREARGS();
    
    mdl_push_interp_eval(RETURN_COPYDOWN, arg, 0, env);
    return NULL;
}

mdl_value_t *mdl_builtin_eval_apply()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    ARGSETUP();
    mdl_value_t *applier;

    GETNEXTREQARG(applier);

#if 0
    // Disabled for help in testing other things.
    if (mdl_apply_type(applier->type) == MDL_TYPE_FIX) {
        mdl_error("Can't use APPLY with FIXes");
    }
#endif
    return mdl_internal_apply(applier, args, true);
}

// LOOPING - PROG, REPEAT, BIND, RETURN, AGAIN
mdl_value_t *mdl_builtin_eval_bind()
/* FSUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *subr = cur_frame->subr;
    return mdl_internal_prog_repeat_bind(subr, args, false, false);
}

mdl_value_t *mdl_builtin_eval_repeat()
/* FSUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *subr = cur_frame->subr;
    return mdl_internal_prog_repeat_bind(subr, args, true, true);
}

mdl_value_t *mdl_builtin_eval_again()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *act;

    GETNEXTARG(act);
    NOMOREARGS();

    if (!act)
    {
        act = mdl_local_symbol_lookup_1_activation_only_please(mdl_value_atom_lastprog->v.a, cur_frame);
    }
    if (!act)
        mdl_error("No activation in AGAIN");
    
    mdl_longjmp_to(act->v.f, LONGJMP_AGAIN);
}

mdl_value_t *mdl_builtin_eval_return()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *val;
    mdl_value_t *act;

    GETNEXTARG(val);
    GETNEXTARG(act);
    NOMOREARGS();

    if (!val) val = mdl_value_T;
    if (!act)
    {
        act = mdl_local_symbol_lookup_1_activation_only_please(mdl_value_atom_lastprog->v.a, cur_frame);
    }
    if (!act)
        return mdl_call_error_ext("NOT-IN-PROG", "No activation in RETURN", NULL);
    if (act->pt != PRIMTYPE_FRAME)
        return mdl_call_error_ext("ILLEGAL-FRAME", "Return to a non-frame", act, NULL);
    
    act->v.f->result = val;
    mdl_longjmp_to(act->v.f, LONGJMP_RETURN);
}

mdl_value_t *mdl_builtin_eval_prog()
/* FSUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *subr = cur_frame->subr;
    return mdl_internal_prog_repeat_bind(subr, args, true, false);
}

// MAPF/MAPR
mdl_value_t *mdl_builtin_eval_mapf()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    return mdl_internal_eval_mapfr(args, false);
}

mdl_value_t *mdl_builtin_eval_mapr()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    return mdl_internal_eval_mapfr(args, true);
}

mdl_value_t *mdl_builtin_eval_mapret()
/* SUBR */
{
    mdl_value_t *act;
    mdl_value_t *args = cur_frame->args;
    act = mdl_local_symbol_lookup(mdl_value_atom_lastmap->v.a, cur_frame);
    if (!act)
        mdl_error("No map in MAPRET");
    
    act->v.f->result = args;
    mdl_longjmp_to(act->v.f, LONGJMP_MAPRET);
}

mdl_value_t *mdl_builtin_eval_mapstop()
/* SUBR */
{
    mdl_value_t *act;
    mdl_value_t *args = cur_frame->args;
    act = mdl_local_symbol_lookup(mdl_value_atom_lastmap->v.a, cur_frame);
    if (!act)
        mdl_error("No map in MAPSTOP");
    
    act->v.f->result = args;
    mdl_longjmp_to(act->v.f, LONGJMP_MAPSTOP);
}

mdl_value_t *mdl_builtin_eval_mapleave()
/* SUBR */
{
    mdl_value_t *val;

    GETARG(val, 0);

    if (!val) val = mdl_value_T;
    mdl_value_t *act = mdl_local_symbol_lookup(mdl_value_atom_lastmap->v.a, cur_frame);

    if (!act)
        mdl_error("No map in MAPLEAVE");
    
    act->v.f->result = val;
    mdl_longjmp_to(act->v.f, LONGJMP_MAPLEAVE);
}

// Arithmetic predicates (8.2.1)

mdl_value_t *mdl_builtin_eval_zerop()
/* SUBR 0? */
{
    ARGSETUP();
    mdl_value_t *e1;
    GETNEXTREQARG(e1);
    NOMOREARGS();
    if (e1->type == MDL_TYPE_FIX)
        return mdl_boolean_value(e1->v.w == 0);
    else if (e1->type == MDL_TYPE_FLOAT)
        return mdl_boolean_value(e1->v.fl == 0.0);
    else
    {
        mdl_error("First arg to 0? must be FIX or FLOAT");
    }
}

mdl_value_t *mdl_builtin_eval_onep()
/* SUBR 1? */
{
    ARGSETUP();
    mdl_value_t *e1;
    GETNEXTREQARG(e1);
    NOMOREARGS();
    if (e1->type == MDL_TYPE_FIX)
        return mdl_boolean_value(e1->v.w == 1);
    else if (e1->type == MDL_TYPE_FLOAT)
        return mdl_boolean_value(e1->v.fl == 1.0);
    else
        mdl_error("First arg to 1? must be FIX or FLOAT");
}


#define MDL_COMPARE_NUMERIC(e1, e2, op) \
    (((e1)->type == MDL_TYPE_FIX)?(((e2)->type == MDL_TYPE_FIX)?((e1)->v.w op (e2)->v.w):((e1)->v.w op (e2)->v.fl)):(((e2)->type == MDL_TYPE_FIX)?((e1)->v.fl op (e2)->v.w):((e1)->v.fl op (e2)->v.fl)))

mdl_value_t *mdl_builtin_eval_greaterp()
/* SUBR G? */
{
    ARGSETUP();
    mdl_value_t *e1;
    mdl_value_t *e2;
    GETNEXTREQARG(e1);
    GETNEXTREQARG(e2);
    NOMOREARGS();

    if (e1->type != MDL_TYPE_FIX && e1->type != MDL_TYPE_FLOAT)
        mdl_error("First arg to G? must be FIX or FLOAT");
    if (e2->type != MDL_TYPE_FIX && e2->type != MDL_TYPE_FLOAT)
        mdl_error("Second arg to G? must be FIX or FLOAT");
    return mdl_boolean_value(MDL_COMPARE_NUMERIC(e1, e2, >));
}

mdl_value_t *mdl_builtin_eval_lessp()
/* SUBR L? */
{
    ARGSETUP();
    mdl_value_t *e1;
    mdl_value_t *e2;
    GETNEXTREQARG(e1);
    GETNEXTREQARG(e2);
    NOMOREARGS();

    if (e1->type != MDL_TYPE_FIX && e1->type != MDL_TYPE_FLOAT)
        mdl_error("First arg to L? must be FIX or FLOAT");
    if (e2->type != MDL_TYPE_FIX && e2->type != MDL_TYPE_FLOAT)
        mdl_error("Second arg to L? must be FIX or FLOAT");
    return mdl_boolean_value(MDL_COMPARE_NUMERIC(e1, e2, <));
}

mdl_value_t *mdl_builtin_eval_greaterequalp()
/* SUBR G=? */
{
    ARGSETUP();
    mdl_value_t *e1;
    mdl_value_t *e2;
    GETNEXTREQARG(e1);
    GETNEXTREQARG(e2);
    NOMOREARGS();

    if (e1->type != MDL_TYPE_FIX && e1->type != MDL_TYPE_FLOAT)
        mdl_error("First arg to G=? must be FIX or FLOAT");
    if (e2->type != MDL_TYPE_FIX && e2->type != MDL_TYPE_FLOAT)
        mdl_error("Second arg to G=? must be FIX or FLOAT");
    return mdl_boolean_value(!MDL_COMPARE_NUMERIC(e1, e2, <));
}

mdl_value_t *mdl_builtin_eval_lessequalp()
/* SUBR L=? */
{
    ARGSETUP();
    mdl_value_t *e1;
    mdl_value_t *e2;
    GETNEXTREQARG(e1);
    GETNEXTREQARG(e2);
    NOMOREARGS();

    if (e1->type != MDL_TYPE_FIX && e1->type != MDL_TYPE_FLOAT)
        mdl_error("First arg to L=? must be FIX or FLOAT");
    if (e2->type != MDL_TYPE_FIX && e2->type != MDL_TYPE_FLOAT)
        mdl_error("Second arg to L=? must be FIX or FLOAT");
    return mdl_boolean_value(!MDL_COMPARE_NUMERIC(e1, e2, >));
}

// Equality and membership (8.2.2)
mdl_value_t *mdl_builtin_eval_double_equalp()
/* SUBR ==? */
{
    ARGSETUP();
    mdl_value_t *e1;
    mdl_value_t *e2;
    GETNEXTREQARG(e1);
    GETNEXTREQARG(e2);
    NOMOREARGS();

    return mdl_boolean_value(mdl_value_double_equal(e1, e2));
}

mdl_value_t *mdl_builtin_eval_double_nequalp()
/* SUBR N==? */
{
    ARGSETUP();
    mdl_value_t *e1;
    mdl_value_t *e2;
    GETNEXTREQARG(e1);
    GETNEXTREQARG(e2);
    NOMOREARGS();

    return mdl_boolean_value(!mdl_value_double_equal(e1, e2));
}

mdl_value_t *mdl_builtin_eval_equalp()
/* SUBR =? */
{
    ARGSETUP();
    mdl_value_t *e1;
    mdl_value_t *e2;
    GETNEXTREQARG(e1);
    GETNEXTREQARG(e2);
    NOMOREARGS();

    return mdl_boolean_value(mdl_value_equal(e1, e2));
}

mdl_value_t *mdl_builtin_eval_nequalp()
/* SUBR N=? */
{
    ARGSETUP();
    mdl_value_t *e1;
    mdl_value_t *e2;
    GETNEXTREQARG(e1);
    GETNEXTREQARG(e2);
    NOMOREARGS();

    return mdl_boolean_value(!mdl_value_equal(e1, e2));
}

void *mdl_memmem(void *hp, int hl, void *np, int nl)
{
    char *sp = (char *)hp; // scan pointer
    char *se = (char *)hp + hl - nl + 1; // scan end
    char *ne = (char *)np + nl; // needle end
    char *shp, *snp; // inner scanners
    while (sp < se)
    {
        if (*sp == *(char *)np)
        {
            shp = sp;
            snp = (char *)np;
            while (snp < ne)
            {
                if (*snp != *shp) break;
                snp++;
                shp++;
            }
            if (snp == ne) return (void *)sp;
        }
        sp++;
    }
    return NULL;
}

mdl_value_t *mdl_builtin_eval_member()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *obj;
    mdl_value_t *structured;
    mdl_struct_walker_t w;
    mdl_value_t *elem;

    GETNEXTARG(obj);
    GETNEXTREQARG(structured);
    NOMOREARGS();
    if (mdl_primtype_nonstructured(structured->pt)) mdl_error("Second arg to MEMBER must be structured");
    
    if (structured->pt == PRIMTYPE_STRING && obj->pt == PRIMTYPE_STRING)
    {
        void *substr;
        substr = mdl_memmem(structured->v.s.p, structured->v.s.l,
                            obj->v.s.p, obj->v.s.l);
        if (substr)
        {
            return mdl_make_string(structured->v.s.l - ((char *)substr - structured->v.s.p), (char *)substr);
        }
        return &mdl_value_false;
    }
    mdl_init_struct_walker(&w, structured);
    elem = w.next(&w);
    while (elem)
    {
        if (mdl_value_equal(elem, obj))
        {
            return w.rest(&w);
        }
            
        elem = w.next(&w);
    }
    return &mdl_value_false;
}
// ARGSTOP

mdl_value_t *mdl_builtin_eval_memq()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *obj = LITEM(args, 0);
    mdl_value_t *structured = LITEM(args, 1);
    mdl_struct_walker_t w;
    mdl_value_t *elem;

    if (!structured) mdl_error("Not enough arguments to MEMQ");
    if (LHASITEM(args, 2))
        mdl_error("Too many args to MEMQ");
    if (mdl_primtype_nonstructured(structured->pt)) mdl_error("Second arg to MEMQ must be structured");
    
    mdl_init_struct_walker(&w, structured);
    elem = w.next(&w);
    while (elem)
    {
        if (mdl_value_double_equal(elem, obj))
        {
            return w.rest(&w);
        }
            
        elem = w.next(&w);
    }
    return &mdl_value_false;
}
// STRCOMP
mdl_value_t *mdl_builtin_eval_strcomp()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *e1 = LITEM(args, 0);
    mdl_value_t *e2 = LITEM(args, 1);
    if (!e2)
        mdl_error("Not enough args to STRCOMP");
    if (LHASITEM(args, 2))
        mdl_error("Too many args to STRCOMP");
    if (e1->type == MDL_TYPE_ATOM && e2->type == MDL_TYPE_ATOM)
    {
        int val = strcmp(e1->v.a->pname, e2->v.a->pname);
        
        if (val < 0) return mdl_new_fix(-1);
        else if (val > 0) return mdl_new_fix(1);
        else return mdl_new_fix(0);
    }
    else if (e1->type == MDL_TYPE_STRING && e2->type == MDL_TYPE_STRING)
    {
        int minlen = (e1->v.s.l < e2->v.s.l)?e1->v.s.l:e2->v.s.l;
        int val = memcmp(e1->v.s.p, e2->v.s.p, minlen);

        if (val < 0) return mdl_new_fix(-1);
        else if (val > 0) return mdl_new_fix(1);
        else if (e1->v.s.l < e2->v.s.l) return mdl_new_fix(-1);
        else if (e1->v.s.l > e2->v.s.l) return mdl_new_fix(1);
        else return mdl_new_fix(0);
    }
    else
    {
        mdl_error("Args to STRCOMP must be both ATOM or both STRING");
    }
}

// ARITHMETIC
mdl_value_t *mdl_builtin_eval_multiply()
/* SUBR * */
{
    MDL_INT accum = 1;
    MDL_FLOAT faccum;
    bool floating = false;

    mdl_value_t *args = cur_frame->args;
    mdl_value_t *argp = LREST(args, 0);
    while (argp)
    {
        mdl_value_t *arg = argp->v.p.car;
        if (arg->type == MDL_TYPE_FLOAT)
        {
            if (!floating) 
            {
                faccum = accum;
                floating = true;
            }
            faccum *= arg->v.fl;
        }
        else 
        {
            if (arg->type != MDL_TYPE_FIX)
                mdl_error("Arguments to * must be FIX OR FLOAT ");
            
            if (floating)
                faccum *= arg->v.w;
            else
                accum *= arg->v.w;
        }
        argp = argp->v.p.cdr;
    }
    if (floating)
        return mdl_new_float(faccum);
    else
        return mdl_new_fix(accum);
}

mdl_value_t *mdl_builtin_eval_add()
/* SUBR + */
{
    MDL_INT accum = 0;
    MDL_FLOAT faccum;
    bool floating = false;

    mdl_value_t *args = cur_frame->args;
    mdl_value_t *argp = LREST(args, 0);
    while (argp)
    {
        mdl_value_t *arg = argp->v.p.car;
        if (arg->type == MDL_TYPE_FLOAT)
        {
            if (!floating) 
            {
                faccum = accum;
                floating = true;
            }
            faccum += arg->v.fl;
        }
        else 
        {
            if (arg->type != MDL_TYPE_FIX)
                return mdl_call_error_ext("ARG-WRONG-TYPE", "+", NULL);
//                mdl_error("Arguments to + must be FIX OR FLOAT ");
            
            if (floating)
                faccum += arg->v.w;
            else
                accum += arg->v.w;
        }
        argp = argp->v.p.cdr;
    }
    if (floating)
        return mdl_new_float(faccum);
    else
        return mdl_new_fix(accum);
}

mdl_value_t *mdl_builtin_eval_subtract()
/* SUBR - */
{
    MDL_INT accum = 0;
    MDL_FLOAT faccum;
    bool floating = false;
    bool firstarg = true;;

    mdl_value_t *args = cur_frame->args;
    mdl_value_t *argp = LREST(args, 0);
    while (argp)
    {
        mdl_value_t *arg = argp->v.p.car;
        if (arg->type == MDL_TYPE_FLOAT)
        {
            if (!floating) 
            {
                faccum = accum;
                floating = true;
            }
            if (firstarg)
                faccum = arg->v.fl;
            else
                faccum = faccum - arg->v.fl;
        }
        else 
        {
            if (arg->type != MDL_TYPE_FIX)
                mdl_error("Arguments to - must be FIX OR FLOAT ");
            
            if (firstarg)
            {
                accum = arg->v.w;
            }
            else
            {
                if (floating)
                    faccum = faccum - arg->v.w;
                else
                    accum = accum - arg->v.w;
            }
        }
        argp = argp->v.p.cdr;
        if (firstarg && !argp)
        {
            if (floating)
                faccum = -faccum;
            else
                accum = -accum;
        }
        firstarg = false;
    }
    if (floating)
        return mdl_new_float(faccum);
    else
        return mdl_new_fix(accum);
}

mdl_value_t *mdl_builtin_eval_divide()
/* SUBR / */
{
    MDL_INT accum = 0;
    MDL_FLOAT faccum;
    bool floating = false;
    bool firstarg = true;;

    mdl_value_t *args = cur_frame->args;
    mdl_value_t *argp = LREST(args, 0);
    while (argp)
    {
        mdl_value_t *arg = argp->v.p.car;
        if (arg->type == MDL_TYPE_FLOAT)
        {
            if (!floating) 
            {
                faccum = accum;
                floating = true;
            }
            if (firstarg)
                faccum = arg->v.fl;
            else
            {
                if (arg->v.fl == 0.0) mdl_error("DIVIDE by 0");
                faccum = faccum / arg->v.fl;
            }
        }
        else 
        {
            if (arg->type != MDL_TYPE_FIX)
                mdl_error("Arguments to / must be FIX OR FLOAT ");
            
            if (firstarg)
            {
                accum = arg->v.w;
            }
            else
            {
                if (arg->v.w == 0.0) mdl_error("DIVIDE by 0");
                if (floating)
                    faccum = faccum / arg->v.w;
                else
                    accum = accum / arg->v.w;
            }
        }
        argp = argp->v.p.cdr;
        if (firstarg && !argp)
        {
            // unary divide
            if (floating)
                faccum = 1/faccum;
            else
                accum = 1/accum;
        }
        firstarg = false;
    }
    if (floating)
        return mdl_new_float(faccum);
    else
        return mdl_new_fix(accum);
}

mdl_value_t *mdl_builtin_eval_min()
/* SUBR */
{
    MDL_INT accum = MDL_INT_MAX;
    MDL_FLOAT faccum = MDL_FLOAT_MAX;;
    bool floating = false;
    bool firstarg = true;

    mdl_value_t *args = cur_frame->args;
    mdl_value_t *argp = LREST(args, 0);
    while (argp)
    {
        mdl_value_t *arg = argp->v.p.car;
        if (arg->type == MDL_TYPE_FLOAT)
        {
            if (!floating)
            {
                if (!firstarg)
                    faccum = accum;
                floating = true;
            }
            if (faccum > arg->v.fl) faccum = arg->v.fl;
        }
        else 
        {
            if (arg->type != MDL_TYPE_FIX)
                mdl_error("Arguments to MIN must be FIX OR FLOAT ");
            
            if (floating)
            {
                if (faccum > arg->v.w) faccum = arg->v.w;
            }
            else
            {
                if (accum > arg->v.w) accum = arg->v.w;
            }
        }
        argp = argp->v.p.cdr;
        firstarg = false;
    }
    if (floating || firstarg)
        return mdl_new_float(faccum);
    else
        return mdl_new_fix(accum);
}

mdl_value_t *mdl_builtin_eval_max()
/* SUBR */
{
    MDL_INT accum = MDL_INT_MIN;
    MDL_FLOAT faccum = MDL_FLOAT_MIN;
    bool floating = false;
    bool firstarg = true;

    mdl_value_t *args = cur_frame->args;
    mdl_value_t *argp = LREST(args, 0);
    while (argp)
    {
        mdl_value_t *arg = argp->v.p.car;
        if (arg->type == MDL_TYPE_FLOAT)
        {
            if (!floating)
            {
                if (!firstarg)
                    faccum = accum;
                floating = true;
            }
            if (faccum < arg->v.fl) faccum = arg->v.fl;
        }
        else 
        {
            if (arg->type != MDL_TYPE_FIX)
                mdl_error("Arguments to MAX must be FIX OR FLOAT ");
            
            if (floating)
            {
                if (faccum < arg->v.w) faccum = arg->v.w;
            }
            else
            {
                if (accum < arg->v.w) accum = arg->v.w;
            }
        }
        argp = argp->v.p.cdr;
        firstarg = false;
    }
    if (floating || firstarg)
        return mdl_new_float(faccum);
    else
        return mdl_new_fix(accum);
}

mdl_value_t *mdl_builtin_eval_mod()
/* SUBR */
{
    MDL_INT numv,modv,result;

    mdl_value_t *args = cur_frame->args;
    mdl_value_t *num = LITEM(args, 0);
    mdl_value_t *modulus = LITEM(args, 1);

    if (LHASITEM(args, 2)) mdl_error("Too many arguments to MOD");
    if (num->type != MDL_TYPE_FIX || modulus->type != MDL_TYPE_FIX)
    {
       mdl_error("MOD arguments must be of type FIX");
    }
    numv = num->v.w;
    modv = modulus->v.w;

    if (modv == 0)
        mdl_error("MOD with 0");
    result = numv % modv;
    if ((result < 0 && modv > 0) || 
        (result > 0 && modv < 0))
    {
        result += modv;
    }
    return mdl_new_fix(result);
}

mdl_value_t *mdl_builtin_eval_random()
/* SUBR */
{
    mdl_value_t *seed1, *seed2;
    MDL_INT rvalue;
    unsigned short rseed[3];
    
    ARGSETUP();
    GETNEXTARG(seed1);
    GETNEXTARG(seed2);
    NOMOREARGS();
    if (seed1 && seed1->type != MDL_TYPE_FIX)
        mdl_error("RANDOM seeds must be type FIX");
    if (seed2 && seed2->type != MDL_TYPE_FIX)
        mdl_error("RANDOM seeds must be type FIX");
    if (seed1 && seed2)
    {
#ifdef MDL32
        uint64_t tot_seed = ((uint64_t)seed1->v.w << 16) ^ ((uint64_t)seed2->v.w);
        rseed[0] = tot_seed >> 32;
        rseed[1] = tot_seed >> 16;
        rseed[2] = tot_seed;
#else
        // treat the seed as two 36-bit numbers; XOR together for 48 bits
        uint64_t tot_seed = ((MDL_UINT)seed1->v.w << 12) ^ ((MDL_UINT)seed2->v.w);
        rseed[0] = tot_seed >> 32;
        rseed[1] = tot_seed >> 16;
        rseed[2] = tot_seed;
#endif
        seed48(rseed);
    }
    else if (seed1)
    {
#ifdef MDL32
        rseed[0] = seed1->v.w >> 16;
        rseed[1] = seed1->v.w;
        rseed[2] = 1;
#else
        // 36-bit seed
        rseed[0] = seed1->v.w >> 20;
        rseed[1] = seed1->v.w >> 4;
        rseed[2] = seed1->v.w & 0xF;
#endif
        seed48(rseed);
    }
#ifdef MDL32
    rvalue = mrand48();
#else
    rvalue = (MDL_INT)(((MDL_UINT)mrand48() << 32) ^ ((MDL_UINT)mrand48()));
#endif
    return mdl_new_fix(rvalue);

}

mdl_value_t *mdl_builtin_eval_float()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *arg = args->v.p.cdr->v.p.car;
    if (!arg) mdl_error("Not enough args to FLOAT");
    if (args->v.p.cdr->v.p.cdr) mdl_error("Too many args to FLOAT");
    if (arg->type != MDL_TYPE_FIX)
        mdl_error("Arg to FLOAT must be FIX");
    return mdl_new_float((MDL_FLOAT)arg->v.w);
}

mdl_value_t *mdl_builtin_eval_fix()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *arg = args->v.p.cdr->v.p.car;
    if (!arg) mdl_error("Not enough args to FLOAT");
    if (args->v.p.cdr->v.p.cdr) mdl_error("Too many args to FLOAT");
    if (arg->type != MDL_TYPE_FLOAT)
        mdl_error("Arg to FIX must be FLOAT");
    return mdl_new_fix((MDL_INT)arg->v.fl);
}
mdl_value_t *mdl_builtin_eval_abs()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *num;
    GETNEXTREQARG(num);
    NOMOREARGS();
    if (num->type == MDL_TYPE_FLOAT)
    {
#ifdef MDL32
        return mdl_new_float(fabsf(num->v.fl));
#else
        return mdl_new_float(fabs(num->v.w));
#endif
    }
    else if (num->type == MDL_TYPE_FIX)
    {
        return mdl_new_fix((num->v.w<0)?(-num->v.w):num->v.w);
    }
    return mdl_call_error("FIRST-ARG-WRONG-TYPE", cur_frame->subr, NULL);
}


// Properties
mdl_value_t *mdl_builtin_eval_getprop()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *item = LITEM(args, 0);
    mdl_value_t *indicator = LITEM(args, 1);
    mdl_value_t *exp = LITEM(args, 2);
    if (indicator == NULL)
    {
        mdl_error("Not enough ARGS in GETPROP");
    }
    if (LHASITEM(args, 3))
    {
        mdl_error("Too many ARGS in GETPROP");
    }
    mdl_value_t *result =  mdl_internal_eval_getprop(item, indicator);
    if (result == NULL)
    {
        if (exp) {
            mdl_push_interp_eval(RETURN_COPYDOWN, exp);
        }
        else result = &mdl_value_false;
    }
    return result;
}

mdl_value_t *mdl_builtin_eval_putprop()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *item = LITEM(args, 0);
    mdl_value_t *indicator = LITEM(args, 1);
    mdl_value_t *value = LITEM(args, 2);
    if (indicator == NULL)
    {
        mdl_error("Not enough ARGS in PUTPROP");
    }
    if (LHASITEM(args, 3))
    {
        mdl_error("Too many ARGS in PUTPROP");
    }
    mdl_value_t *result =  mdl_internal_eval_putprop(item, indicator, value);
    return result;
}

// Object lists
mdl_value_t *mdl_builtin_eval_moblist()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *oblname = LITEM(args, 0);
    mdl_value_t *fix = LITEM(args, 1);
    if (!oblname)
        mdl_error("Not enough arguments to MOBLIST");
    if (LHASITEM(args, 2))
        mdl_error("Too many arguments to MOBLIST");
    if (oblname->type != MDL_TYPE_ATOM)
        mdl_error("First argument to MOBLIST must be atom");
    if (fix && fix->type != MDL_TYPE_FIX)
        mdl_error("First argument to MOBLIST must be atom");
    MDL_INT buckets = MDL_OBLIST_HASHBUCKET_DEFAULT;
    if (fix) buckets = fix->v.w;
    if (buckets <= 0) 
        mdl_error("Number of buckets for MOBLIST must be > 0");
    return mdl_create_oblist(oblname, buckets);
}
mdl_value_t *mdl_builtin_eval_oblistp()
/* SUBR OBLIST? */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *a = LITEM(args, 0);
    if (!a)
        mdl_error("Not enough arguments to OBLIST?");
    if (LHASITEM(args, 1))
        mdl_error("Too many arguments to OBLIST?");
    if (a->type != MDL_TYPE_ATOM)
        return mdl_call_error("FIRST-ARG-WRONG-TYPE", cur_frame->subr, a, NULL);
    if (!a->v.a->oblist) return &mdl_value_false;
    return a->v.a->oblist;
}

mdl_value_t *mdl_builtin_eval_lookup()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *str = LITEM(args, 0);
    mdl_value_t *oblist = LITEM(args, 1);
    if (!oblist)
        mdl_error("Not enough arguments to LOOKUP");
    if (LHASITEM(args, 2))
        mdl_error("Too many arguments to LOOKUP");
    if (str->type != MDL_TYPE_STRING)
        mdl_error("First argument to LOOKUP must be string");
    if (oblist->type != MDL_TYPE_OBLIST)
        mdl_error("Second argument to LOOKUP must be oblist");
    mdl_value_t *result = mdl_get_atom_from_oblist(str->v.s.p, oblist);
    if (result) return result;
    return &mdl_value_false;
}

mdl_value_t *mdl_builtin_eval_atom()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *str = LITEM(args, 0);
    if (!str)
        mdl_error("Not enough arguments to ATOM");
    if (LHASITEM(args, 1))
        mdl_error("Too many arguments to ATOM");
    if (str->type != MDL_TYPE_STRING)
        mdl_error("First argument to ATOM must be string");
    mdl_value_t *result = mdl_create_atom(str->v.s.p);
    return result;
}

mdl_value_t *mdl_builtin_eval_remove()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *str = LITEM(args, 0);
    mdl_value_t *oblist = LITEM(args, 1);
    mdl_value_t *result;

    if (str->type == MDL_TYPE_ATOM)
    {
        if (oblist)
            mdl_error("Too many arguments to REMOVE (atom)");
        result = mdl_remove_atom_from_oblist(str->v.a->pname, str->v.a->oblist);
    }
    else
    {
        if (!oblist)
            mdl_error("Not enough arguments to REMOVE");
        if (LHASITEM(args, 2))
            mdl_error("Too many arguments to REMOVE");
        if (str->type != MDL_TYPE_STRING)
            mdl_error("First argument to REMOVE must be string if oblist specified");
        if (oblist->type != MDL_TYPE_OBLIST)
            mdl_error("Second argument to REMOVE must be oblist");
        result = mdl_remove_atom_from_oblist(str->v.s.p, oblist);
    }
    if (result) return result;
    return &mdl_value_false;
}

mdl_value_t *mdl_builtin_eval_insert()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *str = LITEM(args, 0);
    mdl_value_t *oblist = LITEM(args, 1);
    mdl_value_t *result = NULL;

    if (!oblist)
        mdl_error("Not enough arguments to INSERT");
    if (oblist->type != MDL_TYPE_OBLIST)
        mdl_error("Second argument to INSERT must be oblist");
    if (LHASITEM(args, 2))
        mdl_error("Too many arguments to INSERT");
    if (str->type == MDL_TYPE_ATOM)
    {
        if (str->v.a->oblist)
            return mdl_call_error_ext("ATOM-ALREADY-THERE", "Cannot INSERT, atom already on plist", str, mdl_internal_eval_getprop(oblist, mdl_value_oblist), NULL);
        if (mdl_get_atom_from_oblist(str->v.a->pname, oblist))
            return mdl_call_error_ext("ATOM-ALREADY-THERE", "Cannot INSERT, atom with same pname exists on oblist", str, mdl_internal_eval_getprop(oblist, mdl_value_oblist), NULL);
        mdl_put_atom_in_oblist(str->v.a->pname, oblist, str);
        str->v.a->oblist = oblist;
        result = str;
    }
    else if (str->type == MDL_TYPE_STRING)
    {
        result = mdl_create_atom_on_oblist(str->v.s.p, oblist);
        if (result == NULL)
            mdl_error("Cannot INSERT, atom with given pname exists on oblist");
    }
    else
        mdl_error("First argument to INSERT must be string or atom");
    if (result) return result;
    return &mdl_value_false;
}


mdl_value_t *mdl_builtin_eval_pname()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *a = LITEM(args,0);

    if (!a)
        mdl_error("Not enough arguments to PNAME");
    if (a->type != MDL_TYPE_ATOM)
        return mdl_call_error_ext("FIRST-ARG-WRONG-TYPE", "First argument to PNAME must be ATOM", NULL);
    if (LHASITEM(args, 1))
        mdl_error("Too many arguments to PNAME");
    return mdl_new_string(a->v.a->pname);
}

mdl_value_t *mdl_builtin_eval_spname()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *a = LITEM(args,0);

    if (!a)
        mdl_error("Not enough arguments to SPNAME");
    if (a->type != MDL_TYPE_ATOM)
        mdl_call_error("FIRST-ARG-WRONG-TYPE", cur_frame->subr, a, NULL);
    if (LHASITEM(args, 1))
        mdl_error("Too many arguments to SPNAME");
    return mdl_make_string(a->v.a->pname);
}

mdl_value_t *mdl_builtin_eval_root()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    if (LHASITEM(args, 0))
        mdl_error("Too many arguments to ROOT");
    return mdl_value_root_oblist;
}

mdl_value_t *mdl_builtin_eval_block()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *lookup = LITEM(args, 0);
    if (lookup == NULL)
        mdl_error("Too few arguments to BLOCK");
    if (LHASITEM(args, 1))
        mdl_error("Too many arguments to BLOCK");
    return mdl_push_oblist_lval(lookup);
}

mdl_value_t *mdl_builtin_eval_endblock()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    if (LHASITEM(args, 0))
        mdl_error("Too many arguments to ENDBLOCK");
    return mdl_pop_oblist_lval();
}

// Boolean operators (8.2.3)
mdl_value_t *mdl_builtin_eval_not()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *arg;

    GETNEXTREQARG(arg);
    NOMOREARGS();

    return mdl_boolean_value(!mdl_is_true(arg));
}

mdl_value_t* mdl_and_pt2() {
    IARGSETUP();
    IGETNEXTARG(rest);
    if (mdl_interp_stack->retval) {
        if (!mdl_is_true(mdl_interp_stack->retval)) return mdl_interp_stack->retval;
    }
    if (!rest) return mdl_interp_stack->retval;
    IARGRESET(mdl_interp_stack);
    ISETNEXTARG(rest->v.p.cdr);
    mdl_interp_stack->started = false;
    mdl_push_interp_eval(RETURN_COPYFORWARD, rest->v.p.car);
    return NULL;
}

mdl_value_t *mdl_builtin_eval_and()
/* FSUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *rest = LREST(args, 0);

    if (!rest) return mdl_value_T;
    mdl_push_interp_and_pt2(RETURN_COPYDOWN, rest);
    return NULL;
}

mdl_value_t *mdl_builtin_eval_andp()
/* SUBR AND? */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *rest = LREST(args, 0);
    mdl_value_t *result = mdl_value_T;

    while (rest)
    {
        result = rest->v.p.car;
        if (!mdl_is_true(result)) break;
        rest = rest->v.p.cdr;
    }
    return result;
}

mdl_value_t* mdl_or_pt2() {
    IARGSETUP();
    IGETNEXTARG(rest);
    if (mdl_interp_stack->retval) {
        if (mdl_is_true(mdl_interp_stack->retval)) return mdl_interp_stack->retval;
    }
    if (!rest) return mdl_interp_stack->retval;
    IARGRESET(mdl_interp_stack);
    ISETNEXTARG(rest->v.p.cdr);
    mdl_interp_stack->started = false;
    mdl_push_interp_eval(RETURN_COPYFORWARD, rest->v.p.car);
    return NULL;
}

mdl_value_t *mdl_builtin_eval_or()
/* FSUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *rest = LREST(args, 0);

    if (!rest) return &mdl_value_false;
    mdl_push_interp_or_pt2(RETURN_COPYDOWN, rest);
    return NULL;
}

mdl_value_t *mdl_builtin_eval_orp()
/* SUBR OR? */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *rest = LREST(args, 0);
    mdl_value_t *result = &mdl_value_false;

    while (rest)
    {
        result = rest->v.p.car;
        if (mdl_is_true(result)) break;
        rest = rest->v.p.cdr;
    }
    return result;
}

// Object properties (8.2.4)

mdl_value_t *mdl_builtin_eval_typep()
/* SUBR TYPE? */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *arg = LITEM(args, 0);
    mdl_value_t *rest = LREST(args, 1);
    
    mdl_value_t *result = &mdl_value_false;

    if (arg == NULL)
        mdl_error("Not enough arguments to TYPE?");
    atom_t *mtype = mdl_get_type_name(arg->type);
    while (rest)
    {
        if (mdl_value_equal_atom(rest->v.p.car, mtype))
        {
            result = rest->v.p.car;
            break;
        }
        rest = rest->v.p.cdr;
    }
    return result;
}

mdl_value_t *mdl_builtin_eval_applicablep()
/* SUBR APPLICABLE? */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *arg = LITEM(args, 0);
    if (!arg)
        mdl_error("Not enough args to APPLICABLE?");
    if (LHASITEM(args, 1))
        mdl_error("Too many args to APPLICABLE?");
    return mdl_boolean_value(mdl_type_is_applicable(mdl_apply_type(arg->type)));
}

mdl_value_t *mdl_builtin_eval_monadp()
/* SUBR MONAD? */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *arg = LITEM(args, 0);
    if (!arg)
        mdl_error("Not enough args to MONAD?");
    if (LHASITEM(args, 1))
        mdl_error("Too many args to MONAD?");
    return mdl_boolean_value(mdl_primtype_nonstructured(arg->pt) || mdl_internal_struct_is_empty(arg));
}

mdl_value_t *mdl_builtin_eval_structuredp()
/* SUBR STRUCTURED? */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *arg = LITEM(args, 0);
    if (!arg)
        mdl_error("Not enough args to STRUCTURED?");
    if (LHASITEM(args, 1))
        mdl_error("Too many args to STRUCTURED?");
    return mdl_boolean_value(!mdl_primtype_nonstructured(arg->pt));
}

mdl_value_t *mdl_builtin_eval_emptyp()
/* SUBR EMPTY? */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *arg = LITEM(args, 0);
    if (!arg) mdl_error("Not enough arguments to EMPTY?");
    if (LHASITEM(args, 1))
        mdl_error("Too many args to EMPTY?");
    
    if (mdl_primtype_nonstructured(arg->pt)) return mdl_call_error_ext("FIRST-ARG-WRONG_TYPE","First arg to EMPTY? must be structured",NULL);

    return mdl_boolean_value(mdl_internal_struct_is_empty(arg));
}

mdl_value_t *mdl_builtin_eval_lengthp()
/* SUBR LENGTH? */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *arg = LITEM(args, 0);
    mdl_value_t *max = LITEM(args, 1);
    mdl_value_t *cursor;
    int count, maxv;

    if (!max) mdl_error("Not enough arguments to LENGTH?");
    if (LHASITEM(args, 2))
        mdl_error("Too many args to LENGTH?");
    if (mdl_primtype_nonstructured(arg->pt)) mdl_error("First arg to LENGTH? must be structured");

    if (max->type != MDL_TYPE_FIX) 
        mdl_error("Second arg to LENGTH? must be FIX");
    maxv = max->v.w;
    
    if (arg->pt == PRIMTYPE_LIST)
    {
        count = 0;
        cursor = arg->v.p.cdr;
        while (count < maxv && cursor)
        {
            count++;
            cursor = cursor->v.p.cdr;
        }
        if (cursor) return &mdl_value_false;
    }
    else
    {
        count = mdl_internal_struct_length(arg);
        if (count > maxv) return &mdl_value_false;
    }
    return mdl_new_fix(count);
}
// INPUT/OUTPUT
// arguments for "modep" and "funcp" may be skipped by passing NULL (for FLOAD)
// returns any arguments following the channel arguments
mdl_value_t *mdl_get_check_channel_args(mdl_value_t *args, mdl_value_t **modep, mdl_value_t **name1p, mdl_value_t **name2p, mdl_value_t **devicep, mdl_value_t **dirp, mdl_value_t **funcp)
{
    mdl_value_t *mode = NULL;
    mdl_value_t *name1 = NULL;
    mdl_value_t *name2 = NULL;
    mdl_value_t *device = NULL;
    mdl_value_t *dir = NULL;
    mdl_value_t *func = NULL;

    ARGSETUP();
    if (modep) GETNEXTARG(mode);
    GETNEXTARG(name1);
    GETNEXTARG(name2);
    GETNEXTARG(device);
    GETNEXTARG(dir);
    if (funcp) GETNEXTARG(func);

    if (mode && mode->type != MDL_TYPE_STRING)
        mdl_error("MODE arg to OPEN/CHANNEL must be string");
    if (name1 && name1->type != MDL_TYPE_STRING)
        mdl_error("NAME1 arg to OPEN/CHANNEL must be string");
    if (name2 && name2->type != MDL_TYPE_STRING)
        mdl_error("NAME2 arg to OPEN/CHANNEL must be string");
    if (device && device->type != MDL_TYPE_STRING)
        mdl_error("DEVICE arg to OPEN/CHANNEL must be string");
    if (dir && dir->type != MDL_TYPE_STRING)
        mdl_error("DIR arg to OPEN/CHANNEL must be string");
    if (func && !mdl_string_equal_cstr(&device->v.s, "INT"))
        mdl_error("FUNC arg to OPEN/CHANNEL allowed only for INT device");
    if (func && func->type != MDL_TYPE_FUNCTION)
        mdl_error("FUNC arg to OPEN/CHANNEL must be FUNCTION");
    if (modep) *modep = mode;
    *name1p = name1;
    *name2p = name2;
    *devicep = device;
    *dirp = dir;
    if (funcp )*funcp = func;
    return REMAINING_ARGS();
}

mdl_value_t *mdl_builtin_eval_channel()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *mode;
    mdl_value_t *name1;
    mdl_value_t *name2;
    mdl_value_t *device;
    mdl_value_t *dir;
    mdl_value_t *func;
    mdl_value_t *chan;

    if (mdl_get_check_channel_args(args, &mode, &name1, &name2, &device, &dir, &func))
        mdl_error("Too many args to OPEN or CHANNEL");
    chan = mdl_internal_create_channel();
    if (!mode)
    {
        mode = mdl_new_string("READ");
    }
    mdl_decode_file_args(&name1, &name2, &device, &dir);
    chan = mdl_internal_create_channel();
    *VITEM(chan,CHANNEL_SLOT_MODE) = *mode;
    *VITEM(chan,CHANNEL_SLOT_FNARG1) = *name1;
    *VITEM(chan,CHANNEL_SLOT_FNARG2) = *name2;
    *VITEM(chan,CHANNEL_SLOT_DEVNARG) = *device;
    *VITEM(chan,CHANNEL_SLOT_DIRNARG) = *dir;
    if (mdl_string_equal_cstr(&mode->v.s, "READ") || 
        mdl_string_equal_cstr(&mode->v.s, "READB"))
    {
        mdl_set_chan_eof_object(chan, NULL);
    }
    return chan;
}

mdl_value_t *mdl_builtin_eval_open()
/* SUBR */
{
    mdl_value_t *chan;
    chan = mdl_builtin_eval_channel();
    return mdl_internal_open_channel(chan);
                               
}

mdl_value_t *mdl_builtin_eval_file_existsp()
/* SUBR FILE-EXISTS?*/
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *argt[4] = {NULL, NULL, NULL, NULL};
    mdl_value_t *cursor = args->v.p.cdr;
    int nargs = 0;
    int err;
    char *pathname;
    mdl_value_t *errfalse;
    struct stat stbuf;

    while (cursor && nargs < 4)
    {
        argt[nargs] = cursor->v.p.car;
        if (argt[nargs]->type != MDL_TYPE_STRING)
            mdl_error("All args to FILE-EXISTS must be string");
        cursor = cursor->v.p.cdr;
    }
    if (cursor)
        mdl_error("Too many args to FILE-EXISTS");
    mdl_decode_file_args(&argt[0], &argt[1], &argt[2], &argt[3]);
    pathname = mdl_build_pathname(argt[0], argt[1], argt[2], argt[3]);
    err = stat(pathname, &stbuf);

    if (!err) return mdl_value_T;
    errfalse = mdl_cons_internal(mdl_new_fix(errno), NULL);
    errfalse = mdl_cons_internal(mdl_new_string(strerror(errno)), errfalse);
    errfalse = mdl_make_list(errfalse, MDL_TYPE_FALSE);
    return errfalse;
}

mdl_value_t *mdl_builtin_eval_close()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *chan = LITEM(args, 0);

    if (!chan) mdl_error("Not enough args to CLOSE");
    if (LITEM(args, 1)) mdl_error("Too many args to CLOSE");
    if (chan->type != MDL_TYPE_CHANNEL)
        mdl_error("Argument to close must be of type CHANNEL");
    mdl_internal_close_channel(chan);
    return chan;
}

mdl_value_t *mdl_builtin_eval_reset()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *chan = LITEM(args, 0);

    if (!chan) mdl_error("Not enough args to RESET");
    if (LITEM(args, 1)) mdl_error("Too many args to RESET");
    if (chan->type != MDL_TYPE_CHANNEL)
        mdl_error("Argument to RESET must be of type CHANNEL");
    return mdl_internal_reset_channel(chan);
}

mdl_value_t *mdl_builtin_eval_access()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *chan = LITEM(args, 0);
    mdl_value_t *seek_to = LITEM(args, 1);
    mdl_value_t *mode;
    int chnum;

    if (!seek_to) mdl_error("Not enough args to ACCESS");
    if (LITEM(args, 2)) mdl_error("Too many args to CLOSE");
    if (chan->type != MDL_TYPE_CHANNEL)
        mdl_error("First argument to ACCESS must be of type CHANNEL");
    if (seek_to->type != MDL_TYPE_FIX)
        mdl_error("Second argument to ACCESS must be of type FIX");

    mode = VITEM(chan,CHANNEL_SLOT_MODE);
    chnum = VITEM(chan,CHANNEL_SLOT_CHNUM)->v.w;
    if (chnum == 0)
        mdl_error("Can't ACCESS closed or internal channels");
    if (mdl_string_equal_cstr(&mode->v.s, "PRINT"))
        mdl_error("Can't ACCESS on PRINT channels");
    
    // FIXME -- do the seek right for binary streams too
    // FIXME -- any other buffers
    if (mdl_string_equal_cstr(&mode->v.s, "READ") || 
        mdl_string_equal_cstr(&mode->v.s, "READB"))
        mdl_clear_chan_flags(chan,ICHANNEL_HAS_LOOKAHEAD | ICHANNEL_AT_EOF);
    fseek(mdl_get_channum_file(chnum), seek_to->v.w, SEEK_SET);
    return chan;
}
// Conversion I/O
void mdl_setup_frame_for_read(mdl_value_t **chanp, mdl_value_t *look_up, mdl_value_t *read_table)
{
    mdl_value_t *mdl_value_atom_inchan  = mdl_get_atom("INCHAN!-", true, NULL);
    if (!*chanp)
    {
        *chanp = mdl_local_symbol_lookup_pname("INCHAN!-", cur_frame);
        if (!*chanp) 
            mdl_error("No channel for READ");
    }
    mdl_bind_local_symbol(mdl_value_atom_inchan->v.a, *chanp, cur_frame, false);
    if ((*chanp)->type != MDL_TYPE_CHANNEL)
    {
        mdl_error("Error: Attempt to read from non-channel");
    }
    // FIXME -- when I need look_up and read_table, they should probably be
    // bound regardless of whether they are specified (affects SET)
    // Very low priority...
    if (look_up)
    {
        mdl_bind_local_symbol(atom_oblist, look_up, cur_frame, false);
    }

    if (read_table)
    {
        mdl_value_t *mdl_value_atom_read_table = mdl_get_atom("READ-TABLE!-", true, NULL);
        
        mdl_bind_local_symbol(mdl_value_atom_read_table->v.a, read_table, cur_frame, false);
    }
}

mdl_value_t *mdl_builtin_eval_read()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *argstup = mdl_make_tuple(LREST(args, 0));
    mdl_value_t *chan = TPITEM(argstup, 0); // rebinds .INCHAN
    mdl_value_t *eof_routine = TPITEM(argstup, 1); // Sets EOF routine in channel
    mdl_value_t *look_up = TPITEM(argstup, 2); // Rebinds .OBLIST 
    mdl_value_t *read_table = TPITEM(argstup, 3); // Rebinds .READ-TABLE
    mdl_value_t *result;
    
    if (TPHASITEM(argstup, 4))
        mdl_error("Too many args to READ");

    mdl_setup_frame_for_read(&chan, look_up, read_table);

    if (!mdl_chan_mode_is_input(chan))
        mdl_error("Channel for READ must be input channel");
        
    mdl_set_chan_eof_object(chan, eof_routine);

    result = mdl_read_object(chan);
    return result;
}

mdl_value_t *mdl_builtin_eval_readchr()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *chan = LITEM(args, 0); // rebinds .INCHAN
    mdl_value_t *eof_routine = LITEM(args, 1); // Sets EOF routine in channel
    mdl_value_t *result;

    mdl_setup_frame_for_read(&chan, NULL, NULL);
    if (!mdl_chan_mode_is_input(chan))
        mdl_error("Channel for READCHR must be input channel");

    if (LITEM(args, 2)) mdl_error("Too many args to READCHR");

    mdl_set_chan_eof_object(chan, eof_routine);
    result = mdl_read_character(chan);
    return result;
}

mdl_value_t *mdl_builtin_eval_nextchr()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *chan = LITEM(args, 0); // rebinds .INCHAN
    mdl_value_t *eof_routine = LITEM(args, 1); // Sets EOF routine in channel
    mdl_value_t *result;
    
    if (!mdl_chan_mode_is_input(chan))
        mdl_error("Channel for NEXTCHR must be input channel");

    if (LITEM(args, 2)) mdl_error("Too many args to nextchr");
    mdl_setup_frame_for_read(&chan, NULL, NULL);
    mdl_set_chan_eof_object(chan, eof_routine);
    result = mdl_next_character(chan);
    return result;
}
// Conversion output
void mdl_setup_frame_for_print(mdl_value_t **chanp)
{
    mdl_value_t *mdl_value_atom_outchan  = mdl_get_atom("OUTCHAN!-", true, NULL);
    if (!*chanp)
    {
        *chanp = mdl_local_symbol_lookup(mdl_value_atom_outchan->v.a, cur_frame);
        if (!*chanp) 
            mdl_error("No channel for PRINT");
    }
    mdl_bind_local_symbol(mdl_value_atom_outchan->v.a, *chanp, cur_frame, false);
    if ((*chanp)->type != MDL_TYPE_CHANNEL)
    {
        mdl_error("Error: Attempt to write to non-channel");
    }
}

mdl_value_t *mdl_builtin_eval_print()
/* SUBR */
{
    mdl_value_t *obj, *chan;
    bool binary;

    ARGSETUP();
    GETNEXTARG(obj);
    GETNEXTARG(chan);
    if (!obj)
        mdl_error("Too few arguments to PRINT");
    NOMOREARGS();
    mdl_setup_frame_for_print(&chan);

    binary = mdl_chan_mode_is_print_binary(chan);
    if (!binary && !mdl_chan_mode_is_output(chan))
        mdl_error("Channel for PRINT must be output channel");

    mdl_print_newline_to_chan(chan, binary?MDL_PF_BINARY:MDL_PF_NONE, NULL);
    mdl_print_value_to_chan(chan, obj, false, false, NULL);
    mdl_print_char_to_chan(chan, ' ', binary?MDL_PF_BINARY:MDL_PF_NONE, NULL);
    return obj;
}

mdl_value_t *mdl_builtin_eval_prin1()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *obj, *chan;

    GETNEXTARG(obj);
    GETNEXTARG(chan);
    NOMOREARGS();

    mdl_setup_frame_for_print(&chan);

    if (!mdl_chan_mode_is_output(chan))
        mdl_error("Channel for PRIN1 must be output channel");

    mdl_print_value_to_chan(chan, obj, false, false, NULL);
    fflush(stdout);
    return obj;
}

mdl_value_t *mdl_builtin_eval_princ()
/* SUBR */
{
    mdl_value_t *obj, *chan;

    ARGSETUP();
    GETNEXTARG(obj);
    GETNEXTARG(chan);
    NOMOREARGS();
    if (!obj)
        mdl_error("Too few arguments to PRINC");
    mdl_setup_frame_for_print(&chan);

    if (!mdl_chan_mode_is_output(chan))
        mdl_error("Channel for PRINC must be output channel");

    mdl_print_value_to_chan(chan, obj, true, false, NULL);
    return obj;
}

mdl_value_t *mdl_builtin_eval_terpri()
/* SUBR */
{
    mdl_value_t *chan;
    bool binary;

    ARGSETUP();
    GETNEXTARG(chan);
    NOMOREARGS();
    mdl_setup_frame_for_print(&chan);

    binary = mdl_chan_mode_is_print_binary(chan);
    if (!binary && !mdl_chan_mode_is_output(chan))
        mdl_error("Channel for TERPRI/CRLF must be output channel");

    mdl_print_newline_to_chan(chan, binary?MDL_PF_BINARY:MDL_PF_NONE, NULL);
    return &mdl_value_false;
}

mdl_value_t *mdl_builtin_eval_flatsize()
/* SUBR */
{
    mdl_value_t *obj = NULL;
    mdl_value_t *radix = NULL;
    mdl_frame_t *frame = mdl_new_frame();
    mdl_frame_t *prev_frame = cur_frame;
    mdl_value_t *chan;
    mdl_value_t *max;
    mdl_value_t *result;
    int jumpval;
    mdl_value_t *mdl_value_atom_outchan;

    ARGSETUP();
    GETNEXTARG(obj);
    GETNEXTARG(max);
    GETNEXTARG(radix);
    if (!max) mdl_error("Not enough args to FLATSIZE");
    NOMOREARGS();
    int radixint = 10;
    if (radix)
    {
        if (radix->type == MDL_TYPE_FIX)
            radixint = radix->v.w;
        else
            mdl_error("Radix must be a FIX");
    }

    frame->subr = cur_frame->subr;
    frame->prev_frame = prev_frame;
    mdl_push_frame(frame);

    chan = mdl_create_internal_output_channel(0, max->v.w, mdl_make_frame_value(frame));
    mdl_value_atom_outchan = mdl_get_atom("OUTCHAN!-", true, NULL);
    mdl_bind_local_symbol(mdl_value_atom_outchan->v.a, chan, frame, false);

    frame->interp_frame2 = mdl_interp_stack;
    if ((jumpval = mdl_setjmp(frame->interp_frame2)) != 0)
    {
        if (jumpval == LONGJMP_FLATSIZE_EXCEEDED)
        {
            return &mdl_value_false;
        }
        else
        {
            // Pass it up the chain
            mdl_longjmp_to(prev_frame, jumpval);
        }
    }

    mdl_print_value_to_chan(chan, obj, false, false, NULL);
    result = mdl_new_fix(mdl_get_internal_output_channel_length(chan));
    mdl_pop_frame(prev_frame);
    return result;
}

mdl_value_t *mdl_builtin_eval_crlf()
{
    mdl_builtin_eval_terpri();
    return mdl_value_T;
}

// Image (Binary) input
mdl_value_t *mdl_builtin_eval_readb()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *buffer = LITEM(args, 0);
    mdl_value_t *chan = LITEM(args, 1); // rebinds .INCHAN
    mdl_value_t *eof_routine = LITEM(args, 2); // Sets EOF routine in channel
    mdl_value_t *result;

    mdl_setup_frame_for_read(&chan, NULL, NULL);
    if (buffer == NULL) mdl_error("Not enough arguments to READB");
    if (LITEM(args, 3)) mdl_error("Too many args to READB");
    
    if (!mdl_chan_mode_is_read_binary(chan))
        mdl_error("Channel for READB must be binary input channel");

    mdl_set_chan_eof_object(chan, eof_routine);
    result = mdl_read_binary(chan, buffer);
    return result;
}

mdl_value_t *mdl_builtin_eval_readstring()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *buffer = LITEM(args, 0);
    mdl_value_t *chan = LITEM(args, 1); // rebinds .INCHAN
    mdl_value_t *stop = LITEM(args, 2); 
    mdl_value_t *eof_routine = LITEM(args, 3); // Sets EOF routine in channel
    mdl_value_t *result;

    mdl_setup_frame_for_read(&chan, NULL, NULL);
    if (buffer == NULL) mdl_error("Not enough arguments to READSTRING");
    if (LITEM(args, 4)) mdl_error("Too many args to READSTRING");
    
    if (!mdl_chan_mode_is_input(chan))
        mdl_error("Channel for READSTRING must be input channel");

    mdl_set_chan_eof_object(chan, eof_routine);
    result = mdl_read_string(chan, buffer, stop);
    return result;
}

// Imaged (Binary) output
mdl_value_t *mdl_builtin_eval_printb()
/* SUBR */
{
    mdl_value_t *buffer;
    mdl_value_t *chan;

    ARGSETUP();
    GETNEXTARG(buffer);
    GETNEXTARG(chan);
    NOMOREARGS();
    if (!chan)
        mdl_error("Too few arguments to PRINTB");

    if (!mdl_chan_mode_is_print_binary(chan))
        mdl_error("Channel for PRINTB must be binary output channel");

    if (buffer->type != MDL_TYPE_UVECTOR)
        mdl_error("Buffer for PRINTB must be UVECTOR");
    if (mdl_type_primtype(UVTYPE(buffer)) != MDL_TYPE_WORD)
        mdl_error("Buffer for PRINTB must be UVECTOR containing WORDs");
    mdl_print_binary(chan, buffer);
    return buffer;
}

mdl_value_t *mdl_builtin_eval_printstring()
/* SUBR */
{
    mdl_value_t *buffer;
    mdl_value_t *count;
    mdl_value_t *chan;
    int len;

    ARGSETUP();
    GETNEXTARG(buffer);
    GETNEXTARG(chan);
    GETNEXTARG(count);
    NOMOREARGS();
    if (!buffer)
        mdl_error("Too few arguments to PRINTSTRING");

    mdl_setup_frame_for_print(&chan);

    if (chan->type != MDL_TYPE_CHANNEL)
        mdl_error("Channel wrong type in PRINTSTRING");
        

    if (!mdl_chan_mode_is_output(chan))
        mdl_error("Channel for PRINTSTRING must be output channel");

    if (buffer->type != MDL_TYPE_STRING)
        mdl_error("Buffer for PRINTSTRING must be STRING");

    if (count && count->v.w < 0)
        mdl_error("Count for PRINTSTRING must be >= 0");
    len = buffer->v.s.l;
    if (count && count->v.w < len) len = count->v.w;
    mdl_print_string_to_chan(chan, buffer->v.s.p, len, 0, false, false);
    return buffer;
}

mdl_value_t *mdl_builtin_eval_image()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *ch;
    mdl_value_t *chan;

    GETNEXTREQARG(ch);
    GETNEXTARG(chan);
    NOMOREARGS();

    if (ch->type != MDL_TYPE_FIX)
        return mdl_call_error("FIRST-ARG-WRONG-TYPE", NULL);
    mdl_setup_frame_for_print(&chan);
    mdl_print_char_to_chan(chan, (int)ch->v.w, MDL_PF_NOADVANCE, NULL);
    return ch;
}
// Other IO (LOAD/FLOAD)
mdl_value_t *mdl_builtin_eval_load()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *chan = LITEM(args, 0); // rebinds .INCHAN
    mdl_value_t *look_up = LITEM(args, 1); // Rebinds .OBLIST 
    
    if (LHASITEM(args, 2))
        mdl_error("Too many args to LOAD");

    mdl_setup_frame_for_read(&chan, look_up, NULL);

    if (!mdl_chan_mode_is_input(chan))
        mdl_error("Channel for LOAD must be input channel");
        
    mdl_set_chan_eof_object(chan, NULL);

    mdl_load_file_from_chan(chan);
    mdl_internal_close_channel(chan);
    return mdl_new_string(4, "DONE");
}

mdl_value_t *mdl_builtin_eval_fload()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *name1;
    mdl_value_t *name2;
    mdl_value_t *device;
    mdl_value_t *dir;
    mdl_value_t *look_up;
    mdl_value_t *chan;
    mdl_frame_t *prev_frame;
    mdl_value_t *close_form;
    int jumpval;
    
    look_up = mdl_get_check_channel_args(args, NULL, &name1, &name2, &device, &dir, NULL);
    if (look_up)
    {
        if (look_up->v.p.cdr)
            mdl_error("Too many args to LOAD");
        else
            look_up = look_up->v.p.car;
    }

    chan = mdl_internal_create_channel();
    mdl_decode_file_args(&name1, &name2, &device, &dir);

    *VITEM(chan,CHANNEL_SLOT_MODE) = *mdl_new_string(4, "READ");
    *VITEM(chan,CHANNEL_SLOT_FNARG1) = *name1;
    *VITEM(chan,CHANNEL_SLOT_FNARG2) = *name2;
    *VITEM(chan,CHANNEL_SLOT_DEVNARG) = *device;
    *VITEM(chan,CHANNEL_SLOT_DIRNARG) = *dir;
    mdl_set_chan_eof_object(chan, NULL);
    if (!mdl_is_true(mdl_internal_open_channel(chan)))
        return mdl_call_error("FILE-SYSTEM-ERROR", cur_frame->subr, NULL); // FIXME by passing FALSE to ERROR

    // frame for fake UNWIND
    prev_frame = cur_frame;
    mdl_push_frame(mdl_new_frame());
    cur_frame->subr = prev_frame->subr;
    cur_frame->prev_frame = prev_frame;
    mdl_setup_frame_for_read(&chan, look_up, NULL);
    cur_frame->args = mdl_new_empty_tuple(2, MDL_TYPE_TUPLE);
    cur_frame->frame_flags = MDL_FRAME_FLAGS_UNWIND;

    close_form = mdl_cons_internal(chan, NULL);
    close_form = mdl_cons_internal(mdl_get_atom_from_oblist("CLOSE", mdl_value_root_oblist), close_form);
    close_form = mdl_make_list(close_form, MDL_TYPE_FORM);
    *TPREST(cur_frame->args,1) = *close_form;
    cur_frame->interp_frame2 = mdl_interp_stack;
    if ((jumpval = mdl_setjmp(cur_frame->interp_frame2) != 0))
    {
        // Pass it up the chain
        mdl_longjmp_to(prev_frame, jumpval);
    }
    
    mdl_load_file_from_chan(chan);
    mdl_internal_close_channel(chan);
    mdl_pop_frame(prev_frame);
    return mdl_new_string(4, "DONE");
}

mdl_value_t *mdl_builtin_eval_sname()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *sname;

    GETNEXTARG(sname);
    NOMOREARGS();

    if (!sname)
    {
        sname = mdl_global_symbol_lookup(mdl_get_atom("SNM", true, NULL)->v.a);
        if (!sname)
        {
            char *cwdp = mdl_getcwd();
            if (!cwdp)
                mdl_error("Unable to determine a working directory");
            sname = mdl_new_string(cwdp);
        }
        return sname;
    }
    else
    {
        return mdl_set_gval(mdl_get_atom("SNM", true, NULL)->v.a, sname);
    }
}

// 11.6 SAVE/RESTORE (these are partially implemented)
mdl_value_t *mdl_builtin_eval_save()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *name1;
    mdl_value_t *name2;
    mdl_value_t *dev;
    mdl_value_t *dir;
    char *pathname;
    mdl_value_t *nm1;
    mdl_value_t *nm2;
    mdl_value_t *gc;
    FILE *f;

    GETNEXTARG(name1);
    GETNEXTARG(name2);
    GETNEXTARG(dev);
    GETNEXTARG(dir);
    GETNEXTARG(gc);
    NOMOREARGS();
    
    // NM1, NM2 -- root or current?
    nm1 = mdl_get_atom("NM1", true, NULL);
    nm2 = mdl_get_atom("NM2", true, NULL);
    mdl_bind_local_symbol(nm1->v.a, mdl_new_string(6, "MUDDLE"), cur_frame, false);
    mdl_bind_local_symbol(nm2->v.a, mdl_new_string(4, "SAVE"), cur_frame, false);

    mdl_decode_file_args(&name1, &name2, &dev, &dir);
    pathname = mdl_build_pathname(name1, name2, dev, dir);
//    fprintf(stderr, "Saving to %s\n", pathname);
    f = fopen(pathname, "wb");
    if (f)
    {
        mdl_write_image(f);
        fclose(f);
        return mdl_new_string(5, "SAVED");
    }
    return &mdl_value_false;
}

mdl_value_t *mdl_builtin_eval_restore()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *name1;
    mdl_value_t *name2;
    mdl_value_t *dev;
    mdl_value_t *dir;
    char *pathname;
    mdl_value_t *nm1;
    mdl_value_t *nm2;
    mdl_value_t *gc;
    FILE *f;

    GETNEXTARG(name1);
    GETNEXTARG(name2);
    GETNEXTARG(dev);
    GETNEXTARG(dir);
    GETNEXTARG(gc);
    NOMOREARGS();
    
    // NM1, NM2 -- root or current?
    nm1 = mdl_get_atom("NM1", true, NULL);
    nm2 = mdl_get_atom("NM2", true, NULL);
    mdl_bind_local_symbol(nm1->v.a, mdl_new_string(6, "MUDDLE"), cur_frame, false);
    mdl_bind_local_symbol(nm2->v.a, mdl_new_string(4, "SAVE"), cur_frame, false);

    mdl_decode_file_args(&name1, &name2, &dev, &dir);
    pathname = mdl_build_pathname(name1, name2, dev, dir);
    f = fopen(pathname, "rb");
    if (f)
    {
        bool ok = mdl_read_image(f);
        // danger: at this point the stack and interp stack have changed.
        fclose(f);
        if (ok) return mdl_new_string(8, "RESTORED");
    }
    return &mdl_value_false;
}

// 11.7.5 FILE-LENGTH
mdl_value_t *mdl_builtin_eval_file_length()
/* SUBR FILE-LENGTH */
{
    ARGSETUP();
    mdl_value_t *chan;
    int channum;
    FILE *f;
    fpos_t savepos;
    off_t endpos;

    GETNEXTREQARG(chan);
    NOMOREARGS();
    
    if (!mdl_chan_mode_is_input(chan))
        return mdl_call_error("WRONG-DIRECTION-CHANNEL", NULL);
    channum = mdl_get_chan_channum(chan);
    if (!channum)
        return mdl_call_error("CHANNEL-CLOSED", NULL); // or maybe internal.
    f = mdl_get_channum_file(channum);
    if (!f) 
        return mdl_call_error_ext("CHANNEL-CLOSED", "Channel closed but nonzero", NULL); // shouldn't happen
    if (fgetpos(f,&savepos) == -1) 
        return mdl_call_error("FILE-LENGTH-UNAVAILABLE", NULL); // not an original MDL error
    if (fseek(f, SEEK_END, 0) == -1)
        return mdl_call_error("FILE-LENGTH-UNAVAILABLE", NULL); // not an original MDL error
    endpos = ftello(f);
    fsetpos(f, &savepos);
    if (mdl_chan_mode_is_read_binary(chan))
        return mdl_new_fix((MDL_INT)(endpos / sizeof(MDL_INT)));
    else
        return mdl_new_fix((MDL_INT)endpos);
}

// 14.7.4 DECL?
mdl_value_t *mdl_builtin_eval_declp()
/* SUBR DECL? */
{
    ARGSETUP();
    mdl_value_t *val;
    mdl_value_t *decl;
    bool error;

    GETNEXTARG(val);
    GETNEXTREQARG(decl);
    return mdl_check_decl(val, decl, &error);
}

// 16.1-16.6 LISTEN, ERROR, ERRET, UNWIND, RETRY, etc
mdl_value_t *mdl_builtin_eval_listen()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    return mdl_internal_listen_error(args, false);
}

mdl_value_t *mdl_builtin_eval_error()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    return mdl_internal_listen_error(args, true);
}

mdl_value_t *mdl_builtin_eval_erret()
/* SUBR */
{
    mdl_value_t *result;
    mdl_value_t *frame;

    ARGSETUP();
    GETNEXTARG(result);
    GETNEXTARG(frame);
    NOMOREARGS();

    mdl_internal_erret(result, frame);
    return NULL;
}

mdl_value_t *mdl_builtin_eval_retry()
/* SUBR */
{
    mdl_value_t *frame;

    ARGSETUP();
    GETNEXTARG(frame);
    NOMOREARGS();

    if (!frame) frame = mdl_local_symbol_lookup_pname("L-ERR !-INTERRUPTS!-", cur_frame);
    if (!frame) mdl_error("No frame in RETRY!");
    mdl_longjmp_to(frame->v.f, LONGJMP_RETRY);
}

mdl_value_t *mdl_builtin_eval_unwind()
/* FSUBR */
{
    mdl_value_t *stdexpr;
    mdl_value_t *errexpr;

    ARGSETUP();
    GETNEXTARG(stdexpr);
    GETNEXTARG(errexpr);
    
    if (!errexpr)
        mdl_error("Not enough args to UNWIND");
    cur_frame->frame_flags |= MDL_FRAME_FLAGS_UNWIND;

    mdl_push_interp_eval(RETURN_COPYDOWN, stdexpr);
    return NULL;
 }

mdl_value_t *mdl_builtin_eval_funct()
/* SUBR */
{
    mdl_value_t *frame;

    ARGSETUP();
    GETNEXTARG(frame);
    NOMOREARGS();
    if (!frame) mdl_error("Not enough args to FUNCT");
    return frame->v.f->subr;
}

mdl_value_t *mdl_builtin_eval_args()
/* SUBR */
{
    mdl_value_t *frame;

    ARGSETUP();
    GETNEXTARG(frame);
    NOMOREARGS();
    if (!frame) mdl_error("Not enough args to ARGS");
    return frame->v.f->args;
}

mdl_value_t *mdl_builtin_eval_frame()
/* SUBR */
{
    mdl_value_t *frame;
    mdl_frame_t *f;

    ARGSETUP();
    GETNEXTARG(frame);
    NOMOREARGS();
    if (!frame) {
        frame = mdl_local_symbol_lookup_pname("L-ERR !-INTERRUPTS!-", cur_frame);
        return frame;
    }
    else {
        if (!frame) mdl_error("No frame in FRAME!");
        f = frame->v.f->prev_frame;
        while (f && !(f->frame_flags & MDL_FRAME_FLAGS_TRUEFRAME))
            f = f->prev_frame;
        if (!f) mdl_error("No previous frame found");
        return mdl_make_frame_value(f);
    }
}

// FFRAME did not exist in the original MDL.  It returns the frames
// of not just compiled subroutines, but also functions called by atom name
mdl_value_t *mdl_builtin_eval_fframe()
/* SUBR */
{
    mdl_value_t *frame;
    mdl_frame_t *f;

    ARGSETUP();
    GETNEXTARG(frame);
    NOMOREARGS();

    if (!frame) frame = mdl_local_symbol_lookup_pname("L-ERR !-INTERRUPTS!-", cur_frame);
    if (!frame) mdl_error("No frame in FFRAME!");
    f = frame->v.f->prev_frame;
    while (f && !(f->frame_flags & (MDL_FRAME_FLAGS_TRUEFRAME|MDL_FRAME_FLAGS_NAMED_FUNC)))
        f = f->prev_frame;
    if (!f) mdl_error("No previous frame found");
    return mdl_make_frame_value(f);
}

// TFFRAME returns the top FFRAME (excluding its own)
// (Also not in the original MDL)
mdl_value_t *mdl_builtin_eval_tfframe()
{
    mdl_frame_t *f;
    mdl_value_t *args = cur_frame->args;
    if (args->v.p.cdr) mdl_error("Too many args to TFFRAME");

    f = cur_frame->prev_frame;
    while (f && !(f->frame_flags & (MDL_FRAME_FLAGS_TRUEFRAME|MDL_FRAME_FLAGS_NAMED_FUNC)))
        f = f->prev_frame;
    if (!f) mdl_error("No previous frame found");
    return mdl_make_frame_value(f);
}


mdl_value_t *mdl_rep_state_machine() {
    IARGSETUP();
    IGETNEXTARG(state_val);
    mdl_interp_frame_t *my_iframe = mdl_interp_stack;
    switch (state_val->v.w) {
    case 0:
        mdl_push_interp_eval(RETURN_COPYFORWARD, my_iframe->retval);
        my_iframe->started = false;
        // directly modifying FIX may be an issue for optimiation
        state_val->v.w++;
        break;
    case 1:
    {
        mdl_value_t *mdl_value_atom_terpri = mdl_get_atom_from_oblist("TERPRI", mdl_value_root_oblist);
        mdl_value_t *mdl_value_atom_prin1 = mdl_get_atom_from_oblist("PRIN1", mdl_value_root_oblist);
        mdl_value_t *mdl_value_atom_lval = mdl_get_atom_from_oblist("LVAL", mdl_value_root_oblist);
        mdl_value_t *atom_last_out = mdl_get_atom("LAST-OUT!-", true, NULL);
        mdl_value_t *terpriform = mdl_make_list(mdl_cons_internal(mdl_value_atom_terpri, NULL), MDL_TYPE_FORM);
        mdl_value_t *printform = mdl_cons_internal(atom_last_out, NULL);
        printform = mdl_cons_internal(mdl_value_atom_lval, printform);
        printform = mdl_cons_internal(mdl_make_list(printform, MDL_TYPE_FORM), NULL);
        printform = mdl_cons_internal(mdl_value_atom_prin1, printform);
        printform = mdl_make_list(printform, MDL_TYPE_FORM);
        mdl_set_lval(atom_last_out->v.a, my_iframe->retval, cur_frame);
        mdl_push_interp_eval(RETURN_DISCARD, terpriform);
        mdl_push_interp_eval(RETURN_DISCARD, printform);
        state_val->v.w++;
        // DONE after here, unless REP loops 
        break;
    }
    }
    return mdl_boolean_value(false);
}

// REP -- the Read/evaluate/print SUBR
mdl_value_t *mdl_builtin_eval_rep()
/* SUBR */
{
#if 0
    mdl_value_t *args = cur_frame->args;
    // Can't do this because can be called from LISTEN without a new frame
    if (args->v.p.cdr)
        mdl_error("Too many args to REP");
#endif
#if 1
    mdl_value_t *readform;
    mdl_value_t *mdl_value_atom_read = mdl_get_atom_from_oblist("READ", mdl_value_root_oblist);
    readform = mdl_make_list(mdl_cons_internal(mdl_value_atom_read, NULL), MDL_TYPE_FORM);
    mdl_push_interp_rep_state_machine(RETURN_COPYDOWN, mdl_new_fix(0));
    mdl_push_interp_std_eval(RETURN_COPYFORWARD, readform);
    return mdl_boolean_value(false);
#else
    mdl_value_t *terpriform;
    mdl_value_t *printform;
    mdl_value_t *readresult;
    mdl_value_t *evalresult;
    atom_t *atom_last_out;
    mdl_value_t *mdl_value_atom_read;
    mdl_value_t *mdl_value_atom_terpri;
    mdl_value_t *mdl_value_atom_prin1;
    mdl_value_t *mdl_value_atom_lval;

    mdl_value_atom_read = mdl_get_atom_from_oblist("READ", mdl_value_root_oblist);
    mdl_value_atom_terpri = mdl_get_atom_from_oblist("TERPRI", mdl_value_root_oblist);
    mdl_value_atom_prin1 = mdl_get_atom_from_oblist("PRIN1", mdl_value_root_oblist);
    mdl_value_atom_lval = mdl_get_atom_from_oblist("LVAL", mdl_value_root_oblist);
    atom_last_out = mdl_get_atom("LAST-OUT!-", true, NULL)->v.a;
    readform = mdl_make_list(mdl_cons_internal(mdl_value_atom_read, NULL), MDL_TYPE_FORM);
    terpriform = mdl_make_list(mdl_cons_internal(mdl_value_atom_terpri, NULL), MDL_TYPE_FORM);
    printform = mdl_cons_internal(mdl_newatomval(atom_last_out), NULL);
    printform = mdl_cons_internal(mdl_value_atom_lval, printform);
    printform = mdl_cons_internal(mdl_make_list(printform, MDL_TYPE_FORM), NULL);
    printform = mdl_cons_internal(mdl_value_atom_prin1, printform);
    printform = mdl_make_list(printform, MDL_TYPE_FORM);

// ZORK's behavior implies this while loop is not here,
// though the documentation suggests it is
//    while (1)
    {
        readresult = mdl_std_eval(readform);
        evalresult = mdl_eval(readresult);
        mdl_set_lval(atom_last_out, evalresult, cur_frame);
        mdl_std_eval(printform);
        mdl_std_eval(terpriform);
    }
#endif
    return NULL;
}

// 18.2-18.4 BITS, GETBITS, PUTBITS
mdl_value_t *mdl_builtin_eval_bits()
/* SUBR */
{
    mdl_value_t *width;
    mdl_value_t *right_edge;
    int val;

    ARGSETUP();
    GETNEXTARG(width);
    GETNEXTARG(right_edge);
    NOMOREARGS();
    if (!width) mdl_error("Not enough args to BITS");
    if (width->type != MDL_TYPE_FIX)
        mdl_error("Width in BITS must be FIX");
    if (right_edge && right_edge->type != MDL_TYPE_FIX)
        mdl_error("Right edge in BITS must be FIX");
    val = (width->v.w & 0xFF);
    if (right_edge) val |= ((right_edge->v.w & 0xFF) << 8);
    return mdl_new_word(val, MDL_TYPE_BITS);
}

mdl_value_t *mdl_builtin_eval_getbits()
{
    mdl_value_t *from;
    mdl_value_t *bits;
    int right_edge;
    int width;
    MDL_UINT mask;

    ARGSETUP();
    GETNEXTARG(from);
    GETNEXTARG(bits);
    NOMOREARGS();
    if (!bits) mdl_error("Not enough args to GETBITS");
    if (from->pt != PRIMTYPE_WORD)
        mdl_error("First arg to GETBITS must have primtype WORD");
    if (bits->type != MDL_TYPE_BITS)
        mdl_error("Second arg to GETBITS must be type BITS");
    right_edge = (bits->v.w >> 8)&0xFF;
    width = bits->v.w & 0xFF;
    mask = ((((MDL_UINT)1)<<width)-1) << right_edge;
    return mdl_new_word((MDL_INT)(((MDL_UINT)from->v.w & mask)>>right_edge), MDL_TYPE_WORD);
}

mdl_value_t *mdl_builtin_eval_putbits()
/* SUBR */
{
// Oddly, unlike the other bit operations, PUTBITS does not return a WORD
// in all cases.  Rather, it returns a value with the same type as the
// TO argument.

    mdl_value_t *from, *to;
    mdl_value_t *bits;
    int right_edge;
    int width;
    MDL_UINT mask1, mask2, fromint;

    ARGSETUP();
    GETNEXTARG(to);
    GETNEXTARG(bits);
    GETNEXTARG(from);
    NOMOREARGS();
    if (!bits) mdl_error("Not enough args to PUTBITS");
    if (to->pt != PRIMTYPE_WORD)
        mdl_error("First arg to PUTBITS must have primtype WORD");
    if (from && from->pt != PRIMTYPE_WORD)
        mdl_error("Third arg to PUTBITS must have primtype WORD");
    if (bits->type != MDL_TYPE_BITS)
        mdl_error("Second arg to PUTBITS must be type BITS");
    fromint = 0;
    if (from) fromint = (MDL_UINT)from->v.w;
    right_edge = (bits->v.w >> 8)&0xFF;
    width = bits->v.w & 0xFF;
    mask1 = (((MDL_UINT)1)<<width)-1;
    mask2 = ~(mask1 << right_edge);
    return mdl_new_word((MDL_INT)(((MDL_UINT)to->v.w & mask2) | 
                                  ((fromint & mask1) << right_edge)), to->type);
}

// 18.5 Bitwise booleans -- ANDB, ORB, XORB, EQUVB
mdl_value_t *mdl_builtin_eval_andb()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *rest = LREST(args, 0);
    MDL_INT result = ~0;
    mdl_value_t *arg;
    
    while (rest)
    {
        arg = rest->v.p.car;
        if (arg->pt != PRIMTYPE_WORD)
            mdl_error("Args to ANDB must be of PRIMTYPE WORD");
        result &= arg->v.w;
        rest = rest->v.p.cdr;
    }
    return mdl_new_word(result, MDL_TYPE_WORD);
}

mdl_value_t *mdl_builtin_eval_orb()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *rest = LREST(args, 0);
    MDL_INT result = 0;
    mdl_value_t *arg;
    
    while (rest)
    {
        arg = rest->v.p.car;
        if (arg->pt != PRIMTYPE_WORD)
            mdl_error("Args to ORB must be of PRIMTYPE WORD");
        result |= arg->v.w;
        rest = rest->v.p.cdr;
    }
    return mdl_new_word(result, MDL_TYPE_WORD);
}

mdl_value_t *mdl_builtin_eval_xorb()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *rest = LREST(args, 0);
    MDL_INT result = 0;
    mdl_value_t *arg;
    
    while (rest)
    {
        arg = rest->v.p.car;
        if (arg->pt != PRIMTYPE_WORD)
            mdl_error("Args to XORB must be of PRIMTYPE WORD");
        result ^= arg->v.w;
        rest = rest->v.p.cdr;
    }
    return mdl_new_word(result, MDL_TYPE_WORD);
}

mdl_value_t *mdl_builtin_eval_eqvb()
/* SUBR */
{
    // FIXME
    // this may be wrong. Should <EQVB 0 0 0> be 
    // <EQVB <EQVB 0 0> 0> or should it be 0, for instance?
    // this implementation assumes the former
    mdl_value_t *args = cur_frame->args;
    mdl_value_t *rest = LREST(args, 0);
    MDL_INT result = ~0;
    mdl_value_t *arg;
    
    while (rest)
    {
        arg = rest->v.p.car;
        if (arg->pt != PRIMTYPE_WORD)
            mdl_error("Args to EQVB must be of PRIMTYPE WORD");
        result = ~(result ^ arg->v.w);
        rest = rest->v.p.cdr;
    }
    return mdl_new_word(result, MDL_TYPE_WORD);
}

mdl_value_t *mdl_builtin_eval_lsh()
/* SUBR */
/* Note -- LOGICAL shift, not LEFT shift.  Positive shift is left and
   negative shift is right
 */
{
    ARGSETUP();
    mdl_value_t *shiftme;
    mdl_value_t *shiftby;
    MDL_UINT result;

    GETNEXTARG(shiftme);
    GETNEXTREQARG(shiftby);
    NOMOREARGS();

    if (shiftme->pt != PRIMTYPE_WORD)
        mdl_error("First arg to LSH must be of PRIMTYPE WORD");
    if (shiftby->type != MDL_TYPE_FIX)
        mdl_error("Second arg to LSH must be FIX");
    if (shiftby->v.w < 0)
        result = ((MDL_UINT)shiftme->v.w) >> -shiftby->v.w;
    else
        result = ((MDL_UINT)shiftme->v.w) << shiftby->v.w;
    return mdl_new_word(result, MDL_TYPE_WORD);
}
// 22 GC
mdl_value_t *mdl_builtin_eval_gc()
/* SUBR */
{
    ARGSETUP();
    mdl_value_t *min,*exhaustive, *ms_freq;
    GETNEXTARG(min);
    GETNEXTARG(exhaustive);
    GETNEXTARG(ms_freq);
    NOMOREARGS();

    // only partially implemented.
    GC_gcollect();
    return mdl_value_T;
}

// 23.1 - 23.4 MDL as a system process
mdl_value_t *mdl_builtin_eval_time()
/* SUBR */
{
    MDL_FLOAT cputime;
    // TIME args are evaled but ignored
    struct rusage ru;
    
    getrusage(RUSAGE_SELF, &ru);
    timeradd(&ru.ru_utime, &ru.ru_stime, &ru.ru_utime); // a, b, result
    cputime = ru.ru_utime.tv_sec + (ru.ru_utime.tv_usec/(MDL_FLOAT)1000000);
    return mdl_new_float(cputime);
}

mdl_value_t *mdl_builtin_eval_logout()
{
    // Unix-specific -- if this process has init as its parent, it is
    // running "disowned", so can logout.  In practice this will probably
    // never happen

    if (getppid() == 1) exit(0);
    return &mdl_value_false;
}
mdl_value_t *mdl_builtin_eval_quit()
/* SUBR */
{
    mdl_value_t *args = cur_frame->args;
    if (args->v.p.cdr) mdl_error("Too many arguments to QUIT");
#ifdef GC_DEBUG
    GC_gcollect();
#endif
    exit(0);
}

// New routines for MDL as a system process (not in original MDL)
// GETTIMEOFDAY returns a uvector of FIX with time since the Unix epoch
// in seconds and microseconds
// (note that original MDL is older than the Unix epoch!)
// (also note that the 32 bit version won't be very happy soon -- if
//  I really care about it I need to implement an XWORD type)
mdl_value_t *mdl_builtin_eval_gettimeofday()
/* SUBR */
{
    mdl_value_t *result;
    struct timeval now;
    uvector_element_t *elem;
    mdl_value_t *args = cur_frame->args;
    
    if (args->v.p.cdr) mdl_error("Too many args to gettimeofday");
    gettimeofday(&now, NULL);
    result = mdl_new_empty_uvector(2, MDL_TYPE_UVECTOR);
    UVTYPE(result) = MDL_TYPE_FIX;
    elem = UVREST(result, 0);
    elem[0].w = now.tv_sec;
    elem[1].w = now.tv_usec;
    return result;
}

// GETTIMEDATE takes two arguments, both optional.  The first is 
// a time value (either a FIX, or a UVECTOR from GETTIME)
// to convert to broken-down time format.  The second
// is a boolean specifying whether the time should be GMT (TRUE)
// or local (FALSE).  Default is current time and FALSE.
// Passing a false for the first arg also means "current time",
// so <GETTIMEDATE <> T> returns breakdown of current GMT time.
// The broken-down time format is a UVECTOR of type FIX
// 1: Seconds
// 2: Minutes
// 3: Hours
// 4: Day of month (1-31)
// 5: Month (1-12)
// 6: Year (full year, not year-1900)
// 7: Fractional time in microseconds
// Programs should not depend on there being exactly 7 elements; there
// could be more (e.g. day of week, day of year)
mdl_value_t *mdl_builtin_eval_gettimedate()
{
    mdl_value_t *timeuv;
    mdl_value_t *isgmt;
    mdl_value_t *result;
    uvector_element_t *elems;
    struct timeval tv;
    struct tm broketime;

    ARGSETUP();
    GETNEXTARG(timeuv);
    GETNEXTARG(isgmt);
    NOMOREARGS();

    if (!timeuv || !mdl_is_true(timeuv))
    {
        gettimeofday(&tv, NULL);
    }
    else if (timeuv->type == MDL_TYPE_FIX)
    {
        tv.tv_sec = timeuv->v.w;
        tv.tv_usec = 0;
    }
    else if ((timeuv->type == MDL_TYPE_UVECTOR) && 
             (UVTYPE(timeuv) == MDL_TYPE_FIX) && 
             (UVLENGTH(timeuv) == 2)
        )
    {
        elems = UVREST(timeuv,0);
        tv.tv_sec = elems[0].w;
        tv.tv_usec = elems[1].w;
    }
    else
        mdl_error("Wrong type of time to GETTIMEDATE");

    if (!isgmt || !mdl_is_true(isgmt))
    {
        localtime_r(&tv.tv_sec, &broketime);
    }
    else
    {
        gmtime_r(&tv.tv_sec, &broketime);
    }
    
    result = mdl_new_empty_uvector(7, MDL_TYPE_UVECTOR);
    UVTYPE(result) = MDL_TYPE_FIX;
    elems = UVREST(result, 0);
    elems[0].w = broketime.tm_sec;
    elems[1].w = broketime.tm_min;
    elems[2].w = broketime.tm_hour;
    elems[3].w = broketime.tm_mday;
    elems[4].w = broketime.tm_mon + 1;
    elems[5].w = broketime.tm_year + 1900;
    elems[6].w = tv.tv_usec;
    return result;
}
// UNIMPLEMENTED subroutines
// 14.5 Declaration checking
mdl_value_t *mdl_builtin_eval_gdecl()
/* FSUBR */
{
    // Don't know what this is supposed to return
    return mdl_value_T;
}
// No compiler, so no MANIFEST
mdl_value_t *mdl_builtin_eval_manifest()
/* SUBR */
{
    return mdl_value_T;
}

// 22.2.1 FREEZE
// Just returns a copy ; the GC is non-moving anyway
mdl_value_t *mdl_builtin_eval_freeze()
{
    mdl_value_t *freezeme, *frozen;

    ARGSETUP();
    GETNEXTARG(freezeme);
    NOMOREARGS();
    
    if (!freezeme) mdl_error("Not enough args to FREEZE");
    frozen = mdl_internal_copy_structured(freezeme);
    if (!frozen) mdl_error("Object not of a FREEZEable type");
    return frozen;
}
// 22.6 BLOAT
mdl_value_t *mdl_builtin_eval_bloat()
/* SUBR */
{
    mdl_value_t *fre;

    ARGSETUP();
    GETNEXTARG(fre);
    
    if (fre && fre->type != MDL_TYPE_FIX)
        mdl_error("Args to BLOAT must be type FIX");
    
    // Values are bogus, but should indicate lots of free storage
    if (fre) return fre;
    else return mdl_new_fix(65536);
}

// 21.3 - 21.7 INTERRUPTS 
mdl_value_t *mdl_builtin_eval_off()
/* SUBR */
{
    return mdl_value_T;
}

mdl_value_t *mdl_builtin_eval_event()
/* SUBR */
{
    return mdl_value_T;
}

mdl_value_t *mdl_builtin_eval_handler()
/* SUBR */
{
    return mdl_value_T;
}

// 21.6 More interrupts
mdl_value_t *mdl_builtin_eval_on()
/* SUBR */
{
    return mdl_value_T;
}

mdl_value_t *mdl_builtin_eval_enable()
/* SUBR */
{
    return mdl_value_T;
}

mdl_value_t *mdl_builtin_eval_disable()
/* SUBR */
{
    return mdl_value_T;
}

mdl_value_t *mdl_builtin_eval_int_level()
/* SUBR INT-LEVEL */
{
    mdl_value_t *atom_intlevel;
    mdl_value_t *arg;
    mdl_value_t *result;
    mdl_value_t *args = cur_frame->args;
    
    arg = LITEM(args, 0);
    if (LHASITEM(args, 1))
        mdl_error("Too many args to INT-LEVEL");
    if (arg && arg->type != MDL_TYPE_FIX)
        mdl_error("INT-LEVEL argument must be FIX");

    atom_intlevel = mdl_get_atom("INT-LEVEL!-INTERRUPTS!-", true, NULL);
    result = mdl_global_symbol_lookup(atom_intlevel->v.a);
    if (!result) result = mdl_new_fix(0);
    if (arg) mdl_set_gval(atom_intlevel->v.a, arg);
    return result;
}

mdl_value_t *mdl_builtin_eval_sleep()
{
    /* sleeps, but pred arg is ignored as there are no interrupts*/
    mdl_value_t *fix;
    mdl_value_t *pred;
    ARGSETUP();
    GETNEXTREQARG(fix);
    GETNEXTARG(pred);
    NOMOREARGS();

    if (fix->type != MDL_TYPE_FIX)
        return mdl_call_error("FIRST-ARG-WRONG-TYPE", NULL);

    if (fix->v.w < 0)
        return mdl_call_error_ext("ARGUMENT-OUT-OF-RANGE", "SLEEP time negative", NULL);
    fflush(stdout); // let the user see while we rest
    sleep(fix->v.w);

    return mdl_value_T;
}

// GPL implementing functions
mdl_value_t *mdl_builtin_eval_warranty()
/* SUBR */
{
    extern const char no_warranty[];
    mdl_value_t *chan = NULL;
    ARGSETUP();
    NOMOREARGS();

    mdl_setup_frame_for_print(&chan);
    mdl_print_string_to_chan(chan, no_warranty, strlen(no_warranty), 0, false, false);
    mdl_print_newline_to_chan(chan, false, NULL);
    return &mdl_value_false;
}

mdl_value_t *mdl_builtin_eval_copying()
/* SUBR */
{
    extern const char copying[];
    mdl_value_t *chan = NULL;
    ARGSETUP();
    NOMOREARGS();

    mdl_setup_frame_for_print(&chan);
    mdl_print_string_to_chan(chan, copying, strlen(copying), 0, false, false);
    mdl_print_newline_to_chan(chan, false, NULL);
    return mdl_value_T;
}
